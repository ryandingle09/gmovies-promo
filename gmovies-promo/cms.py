import calendar
import datetime
import jinja2
import json
import logging
import os
import binascii
import re
import urllib
import webapp2
import StringIO
import sys

sys.path.insert(0, 'libs')

import xlwt

from decimal import Decimal
from operator import itemgetter
from uuid import uuid4 as uuid_gen

from google.appengine.api import users
from google.appengine.ext import deferred, ndb
from google.appengine.runtime import DeadlineExceededError

from models import (asia_manila_timezone, PromoCategory, PromoCode, Theater, TheaterOrganization, CliamCodeApiSettings)
from util import DEFAULT_ERROR_MESSAGE_ADMIN

JINJA_ENVIRONMENT = jinja2.Environment(
            loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
            extensions=['jinja2.ext.autoescape'], autoescape=True)

log = logging.getLogger(__name__)

class BadRequestError(Exception):
    def __init__(self, param, code):
        super(Exception, self).__init__()
        self.msg = param
        self.code = code

class Index(webapp2.RequestHandler):

    def get(self):
        try:
            cursor = ndb.Cursor(urlsafe=self.request.get('cursor'))
            action = self.request.get('action') if self.request.get('action') else None
            item_id = self.request.get('id') if self.request.get('id') else '0'
            item_id = int(item_id)

            if action is not None and str(action) == 'edit' or str(action) == 'view':
                data = CliamCodeApiSettings.get_by_id(item_id)

                self.response.out.write(json.dumps({
                    'id': data.key.id(),
                    'name': data.name,
                    'slug': data.slug.replace("-"," ").lower() if str(action) == 'edit' else data.slug,
                    'token': data.token,
                    'promo_name': data.promo_name,
                    'endpoint': data.endpoint if data.endpoint is not None else '',
                    'method': data.method if data.method is not None else '',
                    'payload': data.payload if data.payload is not None else '',
                    'headers': data.headers if data.headers is not None else '',
                    'prefix': data.prefix,
                    'created_at': str(data.created_at + datetime.timedelta(hours=8)),
                    'updated_at': str(data.updated_at + datetime.timedelta(hours=8)) if data.updated_at is not None else None,
                }))

            else:
                data = CliamCodeApiSettings.query().order(-CliamCodeApiSettings.created_at, CliamCodeApiSettings.key)

                query_set_page, next_cursor, more = data.fetch_page(50, start_cursor=cursor)
                template_values = {'query_set_page': query_set_page, 'next_cursor': next_cursor,'more': more}
                template = JINJA_ENVIRONMENT.get_template('templates/api_cms/settings.html')
                self.response.write(template.render(template_values))

        except BadRequestError, (instance):
            response_object = {'return': {'code': instance.code, 'message': instance.msg}}
            self.response.out.write(json.dumps(response_object))

    def post(self):
        try:
            status = ''
            token = binascii.hexlify(os.urandom(24))
            action = self.request.get('action') if self.request.get('action') else None
            item_id = self.request.get('id') if self.request.get('id') else '0'
            item_id = int(item_id)

            name = self.request.get('name') if self.request.get('name') else ''
            slug = self.request.get('slug') if self.request.get('slug') else ''
            promo_name = self.request.get('promo_name') if self.request.get('promo_name') else ''
            prefix = self.request.get('prefix') if self.request.get('prefix') else ''
            endpoint = self.request.get('endpoint') if self.request.get('endpoint') else ''
            method = self.request.get('method') if self.request.get('method') else ''
            payload = self.request.get('payload') if self.request.get('payload') else ''
            headers = self.request.get('headers') if self.request.get('headers') else ''

            
            exists = CliamCodeApiSettings.query()
            check_by_name = exists.filter(CliamCodeApiSettings.name==name)
            check_by_slug = exists.filter(CliamCodeApiSettings.slug==slug)
            
            if str(action) == 'insert':
                if str(name) == '' or str(slug) == '' or str(promo_name) == '' or str(prefix) == '' or str(endpoint) == '':
                    status = {'status': 'All fields are required.'}

                elif check_by_name.count() == 1:
                    status = {'status': 'Name already taken.'}

                elif check_by_slug.count() == 1:
                    status = {'status': 'Slug already taken.'}

                else:
                    insert = CliamCodeApiSettings()
                    insert.name = name
                    insert.slug = slug.replace(" ","-").lower()
                    insert.promo_name = promo_name
                    insert.prefix = prefix
                    insert.endpoint = endpoint
                    insert.method = method
                    insert.payload = payload
                    insert.headers = headers
                    insert.token = token
                    insert.put()

                    status = {'status': 'success'}

            elif str(action) == 'update':
                update = CliamCodeApiSettings.get_by_id(item_id)
                check = False

                if str(name) == '' or str(slug) == '' or str(promo_name) == '' or str(prefix) == '' or str(endpoint) == '':
                    status = {'status': 'All fields are required.'}
                    check = True

                if update.name != name:
                    if check_by_name.count() == 1:
                        status = {'status': 'Name already registered.'}
                        check = True

                if update.slug != slug:
                    if check_by_slug.count() == 1:
                        status = {'status': 'Slug already registered.'}
                        check = True
                
                if not check:
                    update.name = name
                    update.slug = slug.replace(" ","-").lower()
                    update.promo_name = promo_name
                    update.prefix = prefix
                    update.endpoint = endpoint
                    update.method = method
                    update.payload = payload
                    update.headers = headers
                    update.updated_at = datetime.datetime.now()
                    update.put()

                    status = {'status': 'success'}

            else: # delete/disable
                find = CliamCodeApiSettings.get_by_id(item_id)
                find.updated_at = datetime.datetime.now()
                find.status = 'deleted'
                find.put()

                status = {'status': 'success'}

            self.response.out.write(json.dumps(status))

        except BadRequestError, (instance):
            response_object = {'return': {'code': instance.code, 'message': instance.msg}}
            self.response.out.write(json.dumps(response_object))

app = webapp2.WSGIApplication([
    webapp2.Route('/admin/0/api-settings', Index)
],debug=True)