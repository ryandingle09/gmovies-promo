import datetime
import json
import logging
import webapp2
import sys

sys.path.insert(0, 'libs')

from google.appengine.api import taskqueue

from models import PromoCode
from util import convert_timezone


log = logging.getLogger(__name__)


class BadRequestError(Exception):
    def __init__(self, param, code):
        super(Exception, self).__init__()
        self.msg = param
        self.code = code


class RefreshPromoCodesHandler(webapp2.RequestHandler):
    def get(self):
        log.info("RefreshPromoCodesHandler, GET, refreshing claimcodes status...")

        taskqueue.add(url='/tasks/refresh-claimcodes/expire')
        taskqueue.add(url='/tasks/refresh-claimcodes/unexpire')

        return self.response.out.write(json.dumps({'status': 'ok'}))


class ExpirePromoCodesHandler(webapp2.RequestHandler):
    def post(self):
        log.info("ExpirePromoCodesHandler, POST, start tagging is_expired True for claimcodes reached/exceeded the expiration date...")

        try:
            today = convert_timezone(datetime.datetime.now(), 8, '+')
            claimcodes = PromoCode.query(PromoCode.is_redeemed==False, PromoCode.is_expired==False, PromoCode.is_invalid==False,
                    PromoCode.expired_date<=today, PromoCode.expired_date!=None).fetch()

            for claimcode in claimcodes:
                if claimcode.expired_date and claimcode.expired_date <= today:
                    log.info("ExpirePromoCodesHandler, POST, claimcode, %s, tagged is_expired equal to True..." % claimcode.promo_code)

                    claimcode.is_expired = True
                    claimcode.put()
        except Exception, e:
            log.warn("ERROR! ExpirePromoCodesHandler, POST...")
            log.error(e)

        return self.response.out.write(json.dumps({'status': 'ok'}))


class UnexpirePromoCodesHandler(webapp2.RequestHandler):
    def post(self):
        log.info("UnexpirePromoCodesHandler, POST, start tagging is_expired False for claimcodes not reached/exceeded the expiration date...")

        try:
            today = convert_timezone(datetime.datetime.now(), 8, '+')
            claimcodes = PromoCode.query(PromoCode.is_expired==True, PromoCode.expired_date>today).fetch()

            for claimcode in claimcodes:
                if claimcode.expired_date and claimcode.expired_date > today:
                    log.info("UnexpirePromoCodesHandler, POST, claimcode, %s, tagged is_expired equal to False..." % claimcode.promo_code)

                    claimcode.is_expired = False
                    claimcode.put()
        except Exception, e:
            log.warn("ERROR! UnexpirePromoCodesHandler, POST...")
            log.error(e)

        return self.response.out.write(json.dumps({'status': 'ok'}))


app = webapp2.WSGIApplication([webapp2.Route('/tasks/refresh-claimcodes', RefreshPromoCodesHandler),
                webapp2.Route('/tasks/refresh-claimcodes/expire', ExpirePromoCodesHandler),
                webapp2.Route('/tasks/refresh-claimcodes/unexpire', UnexpirePromoCodesHandler)],
        debug=True)