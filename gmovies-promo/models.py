import datetime

from decimal import Decimal
from google.appengine.ext import ndb
from google.appengine.api import users
from orgs import (AYALA_MALLS, FESTIVAL_MALLS, GREENHILLS_MALLS, ROBINSONS_MALLS,
        GATEWAY_MALLS, MEGAWORLD_MALLS, ROCKWELL_MALLS, SM_MALLS, SHANGRILA_MALLS)


DEFAULT_SEAT_COUNT = 2
DEFAULT_PRICE_PER_SEAT = '350.0'
DEFAULT_THEATERORG = str(AYALA_MALLS)

DATE_FORMAT = '%Y-%m-%d'
TIME_FORMAT = '%H:%M:%S'
DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S'
DATETIME_FORMAT_WITH_MICROSECONDS = '%Y-%m-%d %H:%M:%S.%f'

THEATERORGS = {
        str(AYALA_MALLS): 'Ayala Malls',
        str(FESTIVAL_MALLS): 'Festival Malls',
        str(GREENHILLS_MALLS): 'Greenhills',
        str(ROBINSONS_MALLS): 'Robinsons Malls',
        str(GATEWAY_MALLS): 'Gateway',
        str(MEGAWORLD_MALLS): 'Megaworld',
        str(ROCKWELL_MALLS): 'Rockwell',
        str(SM_MALLS): 'SM Malls',
        str(SHANGRILA_MALLS): 'Shangri-la'
}


# Custom Decimal Property
class DecimalProperty(ndb.IntegerProperty):
    # Decimal property ideal to store currency values, such as $20.34
    # See https://developers.google.com/appengine/docs/python/ndb/subclassprop
    def _validate(self, value):
        if not isinstance(value, (Decimal, str, unicode, int, long)):
            raise TypeError('Expected a Decimal, str, unicode, int or long an got instead %s' % repr(value))

    def _to_base_type(self, value):
        return int(Decimal(value) * 100)

    def _from_base_type(self, value):
        return Decimal(value)/Decimal(100)


class TheaterOrganization(ndb.Model):
    name = ndb.StringProperty(required=True)
    gmovies_theaterorg_id = ndb.StringProperty(required=True)
    url_parameter = ndb.StringProperty(required=True)
    allowed_users = ndb.StringProperty(repeated=True)
    is_active = ndb.BooleanProperty(default=True)


class Theater(ndb.Model):
    name = ndb.StringProperty(required=True)
    theater_code = ndb.StringProperty()
    gmovies_theater_id = ndb.StringProperty(required=True)
    url_parameter = ndb.StringProperty(required=True)
    allowed_users = ndb.StringProperty(repeated=True)
    is_active = ndb.BooleanProperty(default=True)


class Cinema(ndb.Model):
    name = ndb.StringProperty(required=True)
    gmovies_movie_id = ndb.StringProperty(required=True)
    is_active = ndb.BooleanProperty(default=True)


class Movie(ndb.Model):
    name = ndb.StringProperty(required=True)
    is_active = ndb.BooleanProperty(default=True)


class PromoCategory(ndb.Model):
    movies = ndb.KeyProperty(Movie, repeated=True)
    theaters = ndb.KeyProperty(Theater, repeated=True)
    cinemas = ndb.StringProperty(repeated=True)
    seats = ndb.StringProperty(repeated=True)
    name = ndb.StringProperty(required=True)
    correlation = ndb.StringProperty(repeated=True)
    prefix = ndb.StringProperty()
    max_seats = ndb.IntegerProperty(required=True) # maximum seats allowed.
    min_seats = ndb.IntegerProperty(default=1) # manimum seats allowed optional
    max_amount = DecimalProperty(required=True) # maximum amount allowed.
    seat_price = DecimalProperty(required=True) # estimated seat price, will be use to compute the max total amount allowed.
    screening_date = ndb.DateProperty()
    screening_time = ndb.TimeProperty()
    start_date = ndb.DateProperty()
    start_time = ndb.TimeProperty()
    expiration_date = ndb.DateProperty()
    expiration_time = ndb.TimeProperty()
    is_valid = ndb.BooleanProperty(default=True)
    globe_subsidize_amount = DecimalProperty()
    thirdparty_company_name = ndb.StringProperty()
    thirdparty_subsidize_amount = DecimalProperty()
    date_created = ndb.DateTimeProperty(auto_now_add=True)
    date_updated = ndb.DateTimeProperty(auto_now=True)
    created_by = ndb.StringProperty(required=True)
    updated_by = ndb.StringProperty(required=True)
    email_to = ndb.StringProperty(required=True)

    # Additional field for Generic Claim Codes. Added 11/20/2017
    is_generic = ndb.BooleanProperty(default=False)
    redeem_count = ndb.IntegerProperty(default=1)

    # Additional field for white and blacklisting mobile numbers. Added 09/06/2018
    whitelist = ndb.StringProperty(repeated=True)
    blacklist = ndb.StringProperty(repeated=True)

    # Additional field for MPass wallet selection. Added 11/08/2018
    mpass_wallet = ndb.StringProperty()

    def get_count_generated_claimcode(self):
        claim_codes = PromoCode.query(ancestor=self.key)

        return claim_codes.count()

    def get_count_available_claimcode(self):
        claim_codes = PromoCode.query(PromoCode.is_invalid==False, PromoCode.is_redeemed==False, PromoCode.is_expired==False, ancestor=self.key)

        return claim_codes.count()

    def get_count_redeemed_claimcode(self):
        claim_codes = PromoCode.query(PromoCode.is_invalid==False, PromoCode.is_redeemed==True, ancestor=self.key)

        return claim_codes.count()

    def get_count_expired_claimcode(self):
        claim_codes = PromoCode.query(PromoCode.is_invalid==False, PromoCode.is_expired==True, ancestor=self.key)

        return claim_codes.count()

    def get_count_invalid_claimcode(self):
        claim_codes = PromoCode.query(PromoCode.is_invalid==True, ancestor=self.key)

        return claim_codes.count()

    def get_total_amount_redeemed(self):
        total_amount_redeemed = 0.0
        claim_codes = PromoCode.query(PromoCode.is_invalid==False, PromoCode.is_redeemed==True, ancestor=self.key)

        for claim_code in claim_codes:
            if claim_code.booking_details and claim_code.booking_details.booked_seat_count and claim_code.booking_details.booked_price_per_seat:
                amount_redeemed = float(claim_code.booking_details.booked_price_per_seat) * claim_code.booking_details.booked_seat_count
                total_amount_redeemed += amount_redeemed

        return total_amount_redeemed

    def get_total_seats_redeemed(self):
        total_seats_redeemed = 0
        claim_codes = PromoCode.query(PromoCode.is_invalid==False, PromoCode.is_redeemed==True, ancestor=self.key)

        for claim_code in claim_codes:
            if claim_code.booking_details and claim_code.booking_details.booked_seat_count:
                total_seats_redeemed += claim_code.booking_details.booked_seat_count

        return total_seats_redeemed

    def to_entity(self):
        e = self.to_dict()
        e.pop('movies', None)
        e.pop('theaters', None)
        e.pop('cinemas', None)
        e.pop('seats', None)
        e.pop('correlation', None)
        e.pop('date_created', None)
        e.pop('date_updated', None)
        e.pop('created_by', None)
        e.pop('updated_by', None)

        e['name'] = e['name']
        e['seat_price'] = str(e['seat_price'])
        e['max_amount'] = str(e['max_amount'])
        e['screening_date'] = e['screening_date'].strftime(DATE_FORMAT) if e['screening_date'] else ''
        e['screening_time'] = e['screening_time'].strftime(TIME_FORMAT) if e['screening_time'] is not None else ''
        e['start_date'] = e['start_date'].strftime(DATE_FORMAT) if e['start_date'] else ''
        e['start_time'] = e['start_time'].strftime(TIME_FORMAT) if e['start_time'] is not None else ''
        e['expiration_date'] = e['expiration_date'].strftime(DATE_FORMAT) if e['expiration_date'] else ''
        e['expiration_time'] = e['expiration_time'].strftime(TIME_FORMAT) if e['expiration_time'] else ''
        e['whitelist'] = e['whitelist'] if e['whitelist'] else None
        e['blacklist'] = e['blacklist'] if e['blacklist'] else None
        e['mpass_wallet'] = e['mpass_wallet'] if e['mpass_wallet'] else None
        e['globe_subsidize_amount'] = str(e['globe_subsidize_amount']) if e['globe_subsidize_amount'] else ''
        e['thirdparty_company_name'] = str(e['thirdparty_company_name']) if e['thirdparty_company_name'] else ''
        e['thirdparty_subsidize_amount'] = str(e['thirdparty_subsidize_amount']) if e['thirdparty_subsidize_amount'] else ''

        return e


class BookingDetails(ndb.Model):
    theater = ndb.KeyProperty(Theater)
    theater_id = ndb.StringProperty()
    theaterorg_id = ndb.StringProperty()
    movie = ndb.StringProperty()
    movie_key = ndb.KeyProperty(Movie)
    movie_id = ndb.StringProperty()
    schedule = ndb.DateTimeProperty()
    cinema = ndb.StringProperty()
    booked_seats = ndb.StringProperty(repeated=True)
    booked_seat_count = ndb.IntegerProperty() # Count of reserved seats
    booked_price_per_seat = DecimalProperty() # Price per reserved seat


class PromoCode(ndb.Model):
    msisdn = ndb.StringProperty(required=True)
    email = ndb.StringProperty()
    pin = ndb.StringProperty(required=True)
    promo_code = ndb.StringProperty()
    created_on = ndb.DateTimeProperty(auto_now_add=True)
    issued_on = ndb.DateTimeProperty()
    redeemed_on = ndb.DateTimeProperty()
    device_id = ndb.StringProperty()

    # Generate by Batch. Added 08/11/2014
    batch_code = ndb.StringProperty()
    is_batch = ndb.BooleanProperty(default=False)

    #additional fields for min seats. added on 2018/06/19
    min_seats = ndb.IntegerProperty() # Manimum seats allowed

    # Allowed Seats and Total Amount Limit. Added 10/07/2014
    seat_count = ndb.IntegerProperty() # Maximum seats allowed
    price_per_seat = DecimalProperty() # Estimated price per seat

    # Additional field/s for promo codes expiration. Added 10/20/2014
    is_expired = ndb.BooleanProperty(default=False)
    expired_date = ndb.DateTimeProperty()

    #additional fields for screening date and time. added on 2018/06/19
    screening_date = ndb.DateProperty()
    screening_time = ndb.TimeProperty()


    # Additional field/s for bank deposit.
    # And admin panel for Rockwell promo codes.
    # Added 10/31/2014
    is_bank_deposit = ndb.BooleanProperty(default=False)
    purchase_amount = DecimalProperty()

    # deprecated, move to BookingDetails Model. 07/31/2015
    theaterorg_id = ndb.StringProperty()
    booked_seat_count = ndb.IntegerProperty() # Count of reserved seats
    booked_price_per_seat = DecimalProperty() # Price per reserved seat

    # Tag promo name for filtering purposes. Added 11/17/2014
    promo_name = ndb.StringProperty()

    # Additional field/s for booking details. Addded 7/31/2015
    booking_details = ndb.StructuredProperty(BookingDetails)
    is_redeemed = ndb.BooleanProperty(default=False)
    is_invalid = ndb.BooleanProperty(default=False)

    # Additional field for Generic Claim Codes. Added 11/20/2017
    is_generic = ndb.BooleanProperty(default=False)
    redeem_count = ndb.IntegerProperty(default=1) # Max number of redemptions allowed
    redemption_count = ndb.IntegerProperty(default=0) # Actual number of redemptions for the claim code

    # Additional field for white and blacklisting mobile numbers. Added 09/11/2018
    whitelist = ndb.StringProperty(repeated=True)
    blacklist = ndb.StringProperty(repeated=True)

    def get_total_amount(self):
        total_amount = None

        if self.price_per_seat and self.seat_count:
            total_amount = self.price_per_seat * self.seat_count

        return total_amount

    def get_booked_total_amount(self):
        booked_total_amount = None

        if self.booking_details and self.booking_details.booked_price_per_seat and self.booking_details.booked_seat_count:
            booked_total_amount = self.booking_details.booked_price_per_seat * self.booking_details.booked_seat_count
        elif self.booked_price_per_seat and self.booked_seat_count:
            booked_total_amount = self.booked_price_per_seat * self.booked_seat_count

        return booked_total_amount

    def get_status(self):
        if self.redeemed_on:
            return 'Redeemed'
        elif self.is_expired:
            return 'Expired'
        elif self.expired_date and asia_manila_timezone(datetime.datetime.now()) > self.expired_date:
            return 'Expired'
        elif self.is_invalid:
            return 'Invalid'

        return 'Available'

    def to_entity(self):
        e = {}
        e['claim_code'] = self.promo_code
        e['promo_name'] = self.key.parent().get().name if self.key.parent() and self.key.parent().get() else ''
        e['max_seats'] = self.seat_count
        e['min_seats'] = self.min_seats
        e['seat_price'] = str(self.price_per_seat)
        e['screening_date'] = self.screening_date.strftime(DATE_FORMAT) if self.screening_date else ''
        e['screening_time'] = self.screening_time.strftime(TIME_FORMAT) if self.screening_time else ''
        e['expiration_date'] = self.expired_date.date().strftime(DATE_FORMAT) if self.expired_date else ''
        e['expiration_time'] = self.expired_date.time().strftime(TIME_FORMAT) if self.expired_date else ''
        e['whitelist'] = self.whitelist if self.whitelist else None
        e['blacklist'] = self.blacklist if self.blacklist else None
        e['email_to'] = self.key.parent().get().email_to if self.key.parent() and self.key.parent().get() else ''
        e['is_valid'] = True if not self.is_invalid else False

        return e

class GenericClaimCodeTransactions(ndb.Model):
    promo_code = ndb.StringProperty()
    booking_details = ndb.StructuredProperty(BookingDetails)
    redeemed_on = ndb.DateTimeProperty()
    is_invalid = ndb.BooleanProperty(default=False)
    platform = ndb.StringProperty()

class Mobnum(ndb.Model):
    mobnum_class = ndb.StringProperty()

class Promos(ndb.Model):
    promo_code = ndb.StringProperty()
    created_on = ndb.DateTimeProperty(auto_now_add=True)
    issued_on = ndb.DateTimeProperty()
    device_id = ndb.StringProperty()
    seat_count = ndb.IntegerProperty() # Maximum seats allowed
    min_seats = ndb.IntegerProperty(default=1) #Minimum seats allowed
    price_per_seat = DecimalProperty() # Estimated price per seat

    # Generate by Batch. Added 08/11/2014
    batch_code = ndb.StringProperty()
    is_batch = ndb.BooleanProperty(default=True)

    # Additional field/s for promo codes expiration. Added 10/20/2014
    is_expired = ndb.BooleanProperty(default=False)
    expired_date = ndb.DateTimeProperty()

    # Additional field/s for promo codes screening date/dime. Added 06/20/2018
    screening_date = ndb.DateProperty()
    screening_time = ndb.TimeProperty()

    # Tag promo name for filtering purposes. Added 11/17/2014
    promo_name = ndb.StringProperty()

    # Additional field/s for booking details. Addded 7/31/2015
    is_redeemed = ndb.BooleanProperty(default=False)
    redeemed_on = ndb.DateTimeProperty()

    # Tag GMovies promo/blocked screening ID.
    promo_id = ndb.StringProperty(repeated=True)

    def get_total_amount(self):
        total_amount = None

        if self.seat_count:
            total_amount = Decimal('0.00') * self.seat_count

            if self.price_per_seat:
                total_amount = self.price_per_seat * self.seat_count

        return total_amount

    def get_status(self):
        if self.redeemed_on:
            return 'Redeemed'
        elif self.is_expired:
            return 'Expired'
        elif self.expired_date and asia_manila_timezone(datetime.datetime.now()) > self.expired_date:
            return 'Expired'

        return 'Available'


def asia_manila_timezone(datetime_utc):
    return datetime_utc + datetime.timedelta(hours=8)

class CliamCodeApiSettings(ndb.Model):
    name = ndb.StringProperty()
    slug = ndb.StringProperty()
    promo_name = ndb.StringProperty()
    prefix = ndb.StringProperty()
    endpoint = ndb.StringProperty()
    method = ndb.StringProperty()
    payload = ndb.StringProperty()
    headers = ndb.StringProperty()
    token = ndb.StringProperty()
    created_at = ndb.DateTimeProperty(auto_now_add=True)
    updated_at = ndb.DateTimeProperty()
    status = ndb.StringProperty(default="active")
    
class MpassAccounts(ndb.Model):
    name = ndb.StringProperty()
    username = ndb.StringProperty()
    password = ndb.StringProperty()
    created_at = ndb.DateTimeProperty(auto_now_add=True)
    created_by = ndb.StringProperty(default=str(users.get_current_user()))
    updated_at = ndb.DateTimeProperty(auto_now=True)
    updated_by = ndb.StringProperty(default=str(users.get_current_user()))
    deleted_at = ndb.DateTimeProperty()
    deleted_by = ndb.StringProperty()
    status = ndb.IntegerProperty(default=1)
