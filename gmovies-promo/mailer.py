#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Python25
# import simplejson as json
# from google.appengine.ext import webapp
# from google.appengine.ext.webapp import util

# Python27
import json
import webapp2

# import base64
import datetime
import random
import re
import string
import urllib

# from Crypto.Cipher import AES
from google.appengine.api import mail
from google.appengine.api import urlfetch

from models import *


class BadRequestError(Exception):
    def __init__(self, param, code):
        super(Exception, self).__init__()
        self.msg = param
        self.code = code


class MailerHandler(webapp2.RequestHandler):
    def get(self):
        email = self.request.get("email")
        try:
            # check if the promo code is actually valid
            # q = PromoCode.all() # This line of code is using google.appengine.ext.db library
            # q.filter("email = ", email)
            q = PromoCode.query(PromoCode.email == email)
            Record = q.get()

            #if Record is None:
            #   raise BadRequestError("Invalid email, try another.", 401)

            #mail.send_mail(sender="GMOVIES by Globe Telecom<gmovies.support@globelabsbeta.com>", to=email ,subject="GMOVIES Promo Code Registration ", body="Your promo code is: " + promo_code)
            message = mail.EmailMessage(sender="GMOVIES SUPPORT<gmovies.support@globelabsbeta.com>", subject="GMOVIES Promo Code Redemption Now Available")
            message.to = email
            message.body = """
Dear Valued Subscriber,

You can now start using your promo code via the GMOVIES App to redeem your FREE movie tickets. Ticket redemption was temporarily suspended on Dec. 24, 2012 for improvements on the GMOVIES ticket delivery system. 

Please make sure to update to version 1.2 of the GMOVIES App on iOS and please be guided by the FAQs below.

Sincerely yours,

The GMOVIES Team

iPHONE 5 FREE MOVIE Ticket FAQs
----------------------

1. Are there restrictions to this promo?
    a. Location. Our agreement with Ayala Cinemas limits acceptance of the e-tickets generated from GMOVIES initially with Greenbelt 3 and Glorietta 4 but with more cinemas coming on-line very soon.
    b. Ticket cost. Our agreement with Ayala limits the cost of each ticket to P 220 pesos. So special screenings or 3D screenings may not be eligible, depending on the ticket price. If you try to redeem the promo code with tickets costing more that P 220, the app will return an error message. You may select a different movie.
    c. Period. Redemption will expire within 60 days from the time you received the promo code. So if you gave your PIN, email and mobtel on December 20, you can redeem the ticket anytime until February.
    d. Obtaining a code. Obtaining a code via the PIN will expire by January 30. So if you have not done so, go the mobile site and get your promo code.

2. I have not claimed my tickets yet as the app does not accept promo codes at the moment. When can I claim my free movie tickets?
    a. Yes. Promo code issuance will resume on January 10, 2013 and be available until the 60 day limit of expiry. In order to use your promo code on this date, you must first download and install a later version (v 1.2 or higher) of the GMOVIES app from the iTunes app store.

3. Am I eligible? Are these numbers, 0917XXXXXXX eligible. I bought multiple iPhone5's so are all these eligible?
    a. Eligible subscribers must have been given a PIN via SMS. The free ticket offer is on a first come, first served policy and your purchase might not have made the cut-off.

4. What exactly is an e-ticket when obtained through GMOVIES?
    a. When you obtain a ticket via GMOVIES, you will receive an image inside the GMOVIES app that contains the details about the venue, time and seat assignment for the ticket you have purchased or redeemed. In addition, the ticket will also contain a machine readable bar code that the cinema operator will use to validate the ticket. If the bar code is not readable, the ticket will also contain a unique code to indicate the purchase transaction made for your e-ticket. An e-ticket via GMOVIES will be similarly valid as a transaction made through the cinema operator's web-portal.

5. I entered a promo code via the application but encountered an error message when I tried to redeem the ticket. 
    a. Please check the following:
        1. that you've only entered seats for 2 tickets.
        2. that each ticket costs P 220 or less
        3. that the code entered matches the actual promo code sent this should be 16 characters in length with a mix of numbers and letters
    b. If this didn't resolve the issue. contact Globe / gmovies.suppprt@globelabsbeta.com and provide your mobile number and promo code to verify 

6. What is the validity of my free tickets?
    a. You may retrieve a Promo Code until January 30 2013 (i.e. get a promo code given a PIN). Once you have obtained a Promo Code, you may use this to purchase a ticket / obtain an e-ticket within  60 days. Once a ticket has been redeemed, these are valid only on the date, time and venue of the screening. These are non-refundable and non-transferrable.

7.  Where can I use the tickets? Which cinemas can I select seats and order tickets from.
    a. At participating Ayala cinemas: Glorietta 4 and Greenbelt 3.

8. Do I really need the app to redeem my tickets? Can't I just print the ticket? Or go the cinema to get the ticket?
    a. If you would like to avail of these free movie tickets, you must do so through the GMOVIES app and via the promo code mechanic.

9. You're giving me 2 tickets, what if I need more?  How can I add to my order so we can all sit together?
    a. No problem! When asked to pay in the app, select the "Promo Code" option and input your promo code for two free tickets. Pay for any remaining tickets with either Credit Card or MPASS and enjoy the show! Make sure to select the two seats that you wish to redeem first and then afterwards select the remaining seats you'd like to purchase viva other means.

10. If we encounter problems regarding the e-ticket who can we contact?
    a. If you are encountering issues with the application directly or with problems PRIOR to generating an e-ticket. You can call Globe or send an email to gmovies.suppprt@globelabsbeta.com to help resolve your concern.
    b. If an e-ticket was generated (both via the application as well a receipt was sent via email) but there were issues will billing / charging, with ticket validity or with seating, you can contact the respective Theater Operator:
    c. For Ayala Malls, you can contact their hotline directly (+632)752-7883 or via email through feedback@ayalamallscinemas.com.ph. Please make sure to note your transaction number for faster resolution.

11. I accidentally deleted the SMS message containing the PIN to get the promo code. What do I do? 
    a. Please send an email to gmovies.suppprt@globelabsbeta.com and provide your mobile number and we'll try to confirm the validity of the request.

12. I lost the promo code itself (via deleting the email or losing the info / page). How do I recover this? 
    a. Please send an email to gmovies.suppprt@globelabsbeta.com and provide your mobile number and registered email and PIN and we'll try to confirm the validity of the request.


"""
            message.send()

        except BadRequestError, (instance):
            self.error(instance.code)
            self.response.out.write(instance.msg)


""" Old Code: Python25
def main():
    application = webapp.WSGIApplication([('/mymailer', MailerHandler)],
                                         debug=True)
    util.run_wsgi_app(application)


if __name__ == '__main__':
    main()"""


app = webapp2.WSGIApplication([('/mymailer', MailerHandler)], debug=True)
