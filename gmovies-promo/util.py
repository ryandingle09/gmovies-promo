import datetime
import json
import logging
import random
import re
import string
import urllib
import webapp2

from base64 import urlsafe_b64encode, urlsafe_b64decode
from uuid import UUID

from google.appengine.api import mail
from google.appengine.api import urlfetch

from models import *


log = logging.getLogger(__name__)
CORRELATION_VERSION = 2
FILTER_WORDS = ['the', 'a', 'an', 'or', 'and', 'is', 'in']
DEFAULT_MESSAGE = "An error occurred during your transaction. Kindly contact support@gmovies.ph to address your issue."
DEFAULT_ERROR_MESSAGE_ADMIN = "Sorry!, something went wrong. Kindly contact system admin to address your issue."


def translate_messages(message_code):
    messages = {}
    messages['NOTFOUND'] = "Claim code is not valid. Please try again or contact support."
    messages['INVALID'] = "Claim code is not valid. Please try again or contact support."
    messages['REDEEMED'] = "This claim code has been redeemed. Please select another payment method."
    messages['NOTYETAVAILABLE'] = "Claim code is not yet available. Please select another payment method."
    messages['EXPIRED'] = "This claim code has expired. Please select another payment method."
    messages['EXCEEDMAXAMOUNT'] = "Transaction exceeds allowable value for this claim code. Please try again."
    messages['EXCEEDMAXSEATS'] = "This claim code is not valid for the no. of seats selected. Please try again."
    messages['LOWMINSEATS'] = "This claim code is not valid for the minimum no. of seats selected. Please try again."
    messages['MOVIENOTALLOWED'] = "Claim code is not valid for this selected movie. Please try again."
    messages['THEATERNOTALLOWED'] = "Claim code is not valid for this cinema. Please try again."
    messages['SUCCESS'] = "Thank you for redeeming your FREE MOVIE TREAT! Grab your movie tickets via the GMovies app on your next cinema visit and enjoy a hassle-free experience."
    messages['SUCCESSBANKDEPOSIT'] = ""
    messages['EXCEEDEDMAXREDEMPTIONS'] = "This claimcode already reached maximum number of redemptions. Please select another payment method."
    messages['PLATFORMNOTALLOWED'] = "Claim code is not available on website."
    messages['MOBNUMBRANDNOTALLOWED'] = "This promo is exclusive to Globe Postpaid subscribers only."

    if message_code in messages:
        return messages[message_code]

    return DEFAULT_MESSAGE

def convert_timezone(date_parameter, hours, operation):
    if operation == '+':
        result = date_parameter + datetime.timedelta(hours=hours)
    else:
        result = date_parameter - datetime.timedelta(hours=hours)

    return result

def parse_string_to_date(sdate, fdate='%Y-%m-%d'):
    rdate = None

    try:
        rdate = datetime.datetime.strptime(sdate, fdate).date()
    except ValueError, e:
        log.warn("ERROR!, parse_string_to_date, ValueError...")
        log.error(e)
    except Exception, e:
        log.warn("ERROR!, parse_string_to_date...")
        log.error(e)

    return rdate

def parse_string_to_datetime(sdatetime, fdatetime='%Y-%m-%d %H:%M:%S'):
    rdatetime = None

    try:
        rdatetime = datetime.datetime.strptime(sdatetime, fdatetime)
    except ValueError, e:
        log.warn("ERROR!, parse_string_to_datetime, ValueError...")
        log.error(e)
    except Exception, e:
        log.warn("ERROR!, parse_string_to_datetime...")
        log.error(e)

    return rdatetime

def parse_date_to_string(ddate, fdate='%Y-%m-%d'):
    rdate = None

    try:
        rdate = datetime.date.strftime(ddate, fdate)
    except ValueError, e:
        log.warn("ERROR!, parse_date_to_string, ValueError...")
        log.error(e)
    except Exception, e:
        log.warn("ERROR!, parse_date_to_string...")
        log.error(e)

    return rdate

def parse_datetime_to_string(ddatetime, fdatetime='%Y-%m-%d %H:%M:%S'):
    rdatetime = None

    try:
        rdatetime = datetime.datetime.strftime(ddatetime, fdatetime)
    except ValueError, e:
        log.warn("ERROR!, parse_datetime_to_string, ValueError...")
        log.error(e)
    except Exception, e:
        log.warn("ERROR!, parse_datetime_to_string...")
        log.error(e)

    return rdatetime

def parse_float_to_string(float_parameter, string_format='{:.2f}'):
    return string_format.format(float_parameter)

def strip_punct(w):
    return filter(lambda c: c not in string.punctuation, w)

def get_all_correlation_titles(title):
    return [get_correlation_title(v, title) for v in range(1, CORRELATION_VERSION + 1)]

def get_correlation_title(v, title):
    if title.strip() == '':
        return title

    title = title.lower().strip()
    title_words = title.split(' ')
    title_words = filter(None, [strip_punct(w) for w in title_words])
    title_words = filter(lambda w: w != '', title_words)
    title_words = filter(lambda w: w not in FILTER_WORDS, title_words)

    if v == 1:
        correlation_title = "%s:%s" % (v, '_'.join(title_words))
    else:
        correlation_title = "%s:%s" % (v, ''.join(title_words))

    return correlation_title

def encode_id_paths(*ids):
    return '~'.join(map(str, ids))

def decode_id_paths(id_path):
    return id_path.split('~')

def encode_uuid(uuid):
    encoded = urlsafe_b64encode(uuid.bytes)

    return encoded[:-2]

def decode_uuid(enc_uuid):
    enc_uuid_padded = str(enc_uuid + '==')
    uuid_bytes = urlsafe_b64decode(enc_uuid_padded)

    if len(uuid_bytes) != 16:
        return None

    return UUID(bytes=uuid_bytes)

def encoded_theater_id(theater_key):
    theater_id = theater_key.id()
    parent_org = UUID(theater_key.parent().id())

    return encode_id_paths(encode_uuid(parent_org), theater_id)

def decoded_theater_id(enc_theater_id):
    ids = decode_id_paths(enc_theater_id)

    if len(ids) < 2:
        return (None, None)

    return decode_uuid(ids[0]), int(ids[1])
