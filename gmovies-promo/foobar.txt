1. Are there restrictions to this promo?
	a.	Location. Our agreement with Ayala Cinemas limits acceptance of the e-tickets generated from GMOVIES initially with Greenbelt 3 and Glorietta 4 but with more cinemas coming on-line very soon.
	b.	Ticket cost. Our agreement with Ayala limits the cost of each ticket to P 220 pesos. So special screenings or 3D screenings may not be eligible, depending on the ticket price. If you try to redeem the promo code with tickets costing more that P 220, the app will return an error message. You may select a different movie.
	c.	Period. Redemption will expire within 60 days from the time you received the promo code. So if you gave your PIN, email and mobtel on December 20, you can redeem the ticket anytime until February.
	d.	Obtaining a code. Obtaining a code via the PIN will expire by January 30. So if you haven’t done so, go the mobile site and get your promo code.
2.	I have not claimed my tickets yet as the app does not accept promo codes at the moment. When can I claim my free movie tickets?
	a.	Yes. Promo code issuance will resume on January 10, 2013 and be available until the 60 day limit of expiry. In order to use your promo code on this date, you must first download and install a later version (v 1.2 or higher) of the GMOVIES app from the iTunes app store.
3.	Am I eligible? Are these numbers, 0917XXXXXXX eligible. I bought multiple iPhone5 ‘s so are all these eligible?
	a.	Eligible subscribers must have been given a PIN via SMS. The free ticket offer is on a first come, first served policy and your purchase might not have made the cut-off. 
4.	What exactly is an e-ticket when obtained through GMOVIES?
	a.	When you obtain a ticket via GMOVIES, you will receive an image inside the GMOVIES app that contains the details about the venue, time and seat assignment for the ticket you have purchased or redeemed. In addition, the ticket will also contain a machine readable bar code that the cinema operator will use to validate the ticket. If the bar code is not readable, the ticket will also contain a unique code to indicate the purchase transaction made for your e-ticket. An e-ticket via GMOVIES will be similarly valid as a transaction made through the cinema operator’s web-portal.
5. I entered a promo code via the application but encountered an error message when I tried to redeem the ticket. 
	a.	Please check the following:
		1.	that you’ve only entered seats for 2 tickets.
		2.	that each ticket costs P 220 or less
		3.	that the code entered matches the actual promo code sent this should be 16 characters in length with a mix of numbers and letters
	b.	If this didn’t resolve the issue. contact Globe / gmovies.suppprt@globelabsbeta.com and provide your mobile number and promo code to verify 
6.	What is the validity of my free tickets?
	a.	You may retrieve a Promo Code until January 30 2013 (i.e. get a promo code given a PIN). Once you have obtained a Promo Code, you may use this to purchase a ticket / obtain an e-ticket within  60 days. Once a ticket has been redeemed, these are valid only on the date, time and venue of the screening. These are non-refundable and non-transferrable.
7.	Where can I use the tickets? Which cinemas can I select seats and order tickets from.
	a.	At participating Ayala cinemas: Glorietta 4 and Greenbelt 3.
8.	Do I really need the app to redeem my tickets? Can’t I just print the ticket? Or go the cinema to get the ticket?
	a.	If you would like to avail of these free movie tickets, you must do so through the GMOVIES app and via the promo code mechanic.
9.	You’re giving me 2 tickets, what if I need more?  How can I add to my order so we can all sit together?
	a.	No problem! When asked to pay in the app, select the “Promo Code” option and input your promo code for two free tickets. Pay for any remaining tickets with either Credit Card or MPASS and enjoy the show! Make sure to select the two seats that you wish to redeem first and then afterwards select the remaining seats you’d like to purchase viva other means.
10.	If we encounter problems regarding the e-ticket who can we contact?
	a.	If you are encountering issues with the application directly or with problems PRIOR to generating an e-ticket. You can call Globe or send an email to gmovies.suppprt@globelabsbeta.com to help resolve your concern.
	b.	If an e-ticket was generated (both via the application as well a receipt was sent via email) but there were issues will billing / charging, with ticket validity or with seating, you can contact the respective Theater Operator:
	i.	For Ayala Malls, you can contact their hotline directly (+632)752-7883 or via email through feedback@ayalamallscinemas.com.ph. Please make sure to note your transaction number for faster resolution.
11.	I accidentally deleted the SMS message containing the PIN to get the promo code. What do I do? 
	a.	Please send an email to gmovies.suppprt@globelabsbeta.com and provide your mobile number and we’ll try to confirm the validity of the request.
12.	I lost the promo code itself (via deleting the email or losing the info / page). How do I recover this? 
	a.	Please send an email to gmovies.suppprt@globelabsbeta.com and provide your mobile number and registered email and PIN and we’ll try to confirm the validity of the request.

