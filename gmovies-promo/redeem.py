import datetime
import json
import logging
import random
import re
import os
import sys
import string
import urllib
import webapp2
import traceback

sys.path.insert(0, 'libs')

# import requests
import requests

from decimal import Decimal
from uuid import uuid4 as uuid_gen

from google.appengine.api import mail, urlfetch, users
from google.appengine.ext import deferred, ndb
from google.appengine.runtime import DeadlineExceededError
from google.appengine.api.app_identity import get_application_id

from models import (asia_manila_timezone, BookingDetails, Movie, PromoCode, PromoCategory, MpassAccounts,
        Theater, TheaterOrganization, GenericClaimCodeTransactions, Mobnum, DEFAULT_PRICE_PER_SEAT, DEFAULT_SEAT_COUNT,
        DEFAULT_THEATERORG, THEATERORGS, CliamCodeApiSettings)
from util import (convert_timezone, decoded_theater_id, encoded_theater_id, get_all_correlation_titles,
        parse_float_to_string, parse_string_to_date, parse_string_to_datetime, translate_messages,
        DEFAULT_MESSAGE, DEFAULT_ERROR_MESSAGE_ADMIN)

# from google.appengine.api import users
log = logging.getLogger(__name__)

DEFAULT_MSISDN = '0'
DEFAULT_PIN = '2220'
DEFAULT_NUMBER_OF_PROMO_CODES = 1
CLAIMCODE_BATCH_QUANTITY = 250

DATE_FORMAT = '%Y-%m-%d'
TIME_FORMAT = '%H:%M:%S'
DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S'
DATETIME_FORMAT_WITH_MICROSECONDS = '%Y-%m-%d %H:%M:%S.%f'
SCHEDULE_DATETIME_FORMAT = '%Y-%m-%dT%H:%M:%S'

MAIL_DEFAULT_FROM_TEMPLATE = "GMovies Claim Code <gmovies-noreply@%s.appspotmail.com>" % get_application_id()
BCC_DEFAULT_EMAIL = "Mary Antoniette Real <mreal@yondu.com>, Josh Lemuel Torio <jtorio@yondu.com>, Ryan Dingle <rdingle@yondu.com>"

GENERIC_CLAIMCODE_ALLOWED_PLATFORMS = ['android', 'ios']

class BadRequestError(Exception):
    def __init__(self, param, code):
        super(Exception, self).__init__()
        self.msg = param
        self.code = code


class RedeemHandler(webapp2.RequestHandler):
    def get(self):
        self.response.headers["Content-Type"] = "text/plain"

        try:
            claim_code = self.request.get("promo_code")
            screening_date = self.request.get("show_date")  if self.request.get("show_date") else None
            screening_time = self.request.get("show_time")  if self.request.get("show_time") else None
            theaterorg_id = self.request.get("theaterorg_id") if self.request.get("theaterorg_id") else None
            seat_count = int(self.request.get("seat_count")) if self.request.get("seat_count") else 0
            seat_price = Decimal(self.request.get("seat_price")) if self.request.get("seat_price") else Decimal("0.0")
            total_amount = Decimal(self.request.get("total_amount")) if self.request.get("total_amount") else Decimal("0.0")
            mobnum = self.request.get("mobnum") if self.request.get("mobnum") else None
            platform = self.request.get("platform") if self.request.get("platform") else None
            
            # add convenience fee to total amount
            total_amount = verify_amount(total_amount, theaterorg_id, mobnum, seat_count, platform)

            movie_key = None
            theater_key = None
            claim_code = PromoCode.query(PromoCode.promo_code==claim_code).get()
            status_code, message_code, message = claimcode_verification(claim_code, movie_key, theater_key, seat_count, total_amount, screening_time, screening_date)

            if status_code != 200:
                log.warn("ERROR! RedeemHandler, GET, raise BadRequestError...")

                raise BadRequestError(message, status_code)

            claim_code.redeemed_on = convert_timezone(datetime.datetime.now(), 8, '+')
            claim_code.booked_seat_count = seat_count
            claim_code.booked_price_per_seat = seat_price
            claim_code.theaterorg_id = theaterorg_id
            claim_code.is_redeemed = True
            claim_code.put()
        except BadRequestError, instance:
            log.warn("ERROR!, RedeemHandler, GET, BadRequestError...")
            log.error("status_code: %s, message: %s..." % (instance.code, instance.msg))

            self.error(instance.code)
            self.response.out.write(instance.msg)
        except Exception, e:
            log.warn("ERROR!, RedeemHandler, GET...")
            log.error(e)

            self.error(500)
            self.response.out.write(DEFAULT_MESSAGE)

    def post(self):
        self.response.headers["Content-Type"] = "text/plain"

        try:
            response_json = json.loads(self.request.body)
            claim_code = response_json.get("promo_code", None)
            screening_date = response_json.get("screening_date", None)
            screening_time = response_json.get("screening_time", None)
            theaterorg_id = response_json.get("theaterorg_id", None)
            theater_id = response_json.get("theater_id", None)
            movie = response_json.get("movie", None)
            movie_id = response_json.get("movie_id", None)
            cinema = response_json.get("cinema", None)
            schedule = response_json.get("schedule", None)
            seats = response_json.get("seats", None)
            seat_count = int(response_json.get("seat_count", "0"))
            seat_price = Decimal(response_json.get("seat_price", "0.0"))
            total_amount = Decimal(response_json.get("total_amount", "0.0"))
            mobnum = response_json.get("mobnum", None)
            platform = response_json.get("platform", None)
            is_postpaid = response_json.get("is_postpaid", False)

            # add convenience fee to total amount
            total_amount = verify_amount(total_amount, theaterorg_id, mobnum, seat_count, platform)

            movie_key = ndb.Key(Movie, movie_id) if movie_id else None
            theater_key = ndb.Key(TheaterOrganization, theaterorg_id, Theater, int(theater_id)) if theaterorg_id and theater_id else None
            claim_code = PromoCode.query(PromoCode.promo_code==claim_code).get()
            status_code, message_code, message = claimcode_verification(claim_code, movie_key, theater_key, seat_count, total_amount, mobnum, platform, is_postpaid, screening_time, screening_date)

            if status_code != 200:
                log.warn("ERROR! RedeemHandler, POST, raise BadRequestError...")

                raise BadRequestError(message, status_code)

            schedule = parse_string_to_datetime(schedule, SCHEDULE_DATETIME_FORMAT) if schedule else None
            cinema = cinema.lower().split('-')[-1].replace('cinema', '').strip() if cinema else ''
            seats = seats.split(',') if seats else []

            booking_details = BookingDetails()
            booking_details.movie = movie
            booking_details.movie_key = movie_key
            booking_details.movie_id = movie_id
            booking_details.theater = theater_key
            booking_details.theater_id = theater_id
            booking_details.theaterorg_id = theaterorg_id
            booking_details.cinema = cinema
            booking_details.booked_seats = seats
            booking_details.booked_seat_count = seat_count
            booking_details.booked_price_per_seat = seat_price
            booking_details.schedule = schedule

            if claim_code.is_generic:
                mobnum_key = ndb.Key(Mobnum, mobnum)
                generic_transaction = GenericClaimCodeTransactions(parent=mobnum_key)
                generic_transaction.promo_code = claim_code.promo_code
                generic_transaction.booking_details = booking_details
                generic_transaction.redeemed_on = convert_timezone(datetime.datetime.now(), 8, '+')
                generic_transaction.platform = platform
                claim_code.redemption_count += 1
                generic_transaction.put()
            else:
                claim_code.booked_seat_count = seat_count # deprecated, but retained due to previous data.
                claim_code.booked_price_per_seat = seat_price # deprecated, but retained due to previous data.
                claim_code.theaterorg_id = theaterorg_id # deprecated, but retained due to previous data.
                claim_code.booking_details = booking_details
                claim_code.redeemed_on = convert_timezone(datetime.datetime.now(), 8, '+')
                claim_code.is_redeemed = True
            claim_code.put()

            # for updating client api for voucher status to be tag as redeemed on client side
            prefixes = CliamCodeApiSettings.query().fetch()

            if prefixes is not None:
                for p in prefixes:
                    endpoint = str(p.endpoint)
                    header = {
                        "no-header": "no-header"
                    }
                    data = {}

                    if p.prefix in claim_code.promo_code:
                        if p.payload and p.payload is not None or p.payload != '':
                            if ':code' in p.payload:
                                data = p.payload
                                data = data.replace(":code", str(claim_code.promo_code))
                            
                            log.info('payload pass : %s' % (data))

                        if p.endpoint and p.endpoint is not None:
                            if ':code' in endpoint:
                                endpoint = endpoint.replace(':code', str(claim_code.promo_code))

                            if p.headers and p.headers is not None or p.headers != '':
                                header = json.loads(p.headers)
                            
                            log.info('header pass : %s' % (header))
                            log.info('endpoint to : %s' % (endpoint))
                            log.info('method use : %s' % (p.method))

                            update = ''

                            if p.method and p.method is not None:
                                if 'PUT' in str(p.method):
                                    update = requests.put(endpoint, data=json.dumps(data), headers=header)
                                elif 'POST' in str(p.method):
                                    update = requests.post(endpoint, data=json.dumps(data), headers=header)
                                elif 'delete' in str(p.method):
                                    update = requests.delete(endpoint, data=json.dumps(data), headers=header)
                                else:
                                    update = requests.get(endpoint, headers=header)

                            else:
                                update = requests.get(endpoint, headers=header)

                            log.info('response from %s : %s' % (endpoint, update.text))
                            log.info('response code from %s is %s' % (endpoint, update.status_code))

            self.response.out.write(message)
        except BadRequestError, instance:
            log.warn("ERROR!, RedeemHandler, POST, BadRequestError...")
            log.error("status_code: %s, message: %s..." % (instance.code, instance.msg))

            self.error(instance.code)
            self.response.out.write(instance.msg)
        except Exception, e:
            log.warn("ERROR!, RedeemHandler, POST...")
            log.error(traceback.format_exc(e))

            self.error(500)
            self.response.out.write(DEFAULT_MESSAGE)


class VerifyHandler(webapp2.RequestHandler):
    def get(self):
        self.response.headers["Content-Type"] = "text/plain"

        try:
            claim_code = self.request.get("promo_code")
            screening_time = self.request.get("show_time") if self.request.get("show_time") else None
            screening_date = self.request.get("show_date") if self.request.get("show_date") else None
            seat_count = int(self.request.get("seat_count")) if self.request.get("seat_count") else 0
            total_amount = Decimal(self.request.get("total_amount")) if self.request.get("total_amount") else Decimal("0.0")
            movie_id = self.request.get("movie_id", None)
            theaterorg_id = self.request.get("theaterorg_id", None)
            theater_id = self.request.get("theater_id", None)
            mobnum = self.request.get("mobnum", None)
            platform = self.request.get("platform", None)
            is_postpaid = self.request.get("is_postpaid", False)
            if isinstance(is_postpaid, basestring):
                is_postpaid = True if is_postpaid.lower() == 'true' else False

            # add convenience fee to total amount
            total_amount = verify_amount(total_amount, theaterorg_id, mobnum, seat_count, platform)

            movie_key = ndb.Key(Movie, movie_id) if movie_id else None
            theater_key = ndb.Key(TheaterOrganization, theaterorg_id, Theater, int(theater_id)) if theaterorg_id and theater_id else None
            claim_code = PromoCode.query(PromoCode.promo_code==claim_code).get()
            status_code, message_code, message = claimcode_verification(claim_code, movie_key, theater_key, seat_count, total_amount, mobnum, platform, is_postpaid, screening_time, screening_date)

            response_object = {"response": {"code": status_code, "message": message, "message_code": message_code}}
            response_string = json.dumps(response_object)

            if status_code != 200:
                log.warn("ERROR! VerifyHandler, GET, raise BadRequestError...")
                raise BadRequestError(response_string, status_code)

            self.response.out.write(response_string)
        except BadRequestError, instance:
            log.warn("ERROR!, VerifyHandler, GET, BadRequestError...")
            log.error("status_code: %s, message: %s..." % (instance.code, instance.msg))

            self.error(instance.code)
            self.response.out.write(instance.msg)
        except Exception, e:
            log.warn("ERROR!, VerifyHandler, GET...")
            log.error(traceback.format_exc(e))

            self.error(500)
            self.response.out.write(DEFAULT_MESSAGE)


class ResetHandler(webapp2.RequestHandler):
    def get(self):
        promo_code = self.request.get("promo_code")
        mobnum = self.request.get("mobnum", None)

        try:
            if promo_code == "" or not promo_code or promo_code is None:
                raise BadRequestError("Error in parameters.", 400)

            # check if the claim code exists
            q = PromoCode.query(PromoCode.promo_code == promo_code)
            Record = q.get()

            if Record is None:
                raise BadRequestError("Invalid code.", 401)

            if Record.is_generic:
                mobnum_key = ndb.Key(Mobnum, mobnum)
                generic_transaction = GenericClaimCodeTransactions.query(GenericClaimCodeTransactions.promo_code==promo_code, GenericClaimCodeTransactions.is_invalid==False, ancestor=mobnum_key).get()
                # Set invalid to False then subtract redemption count in PromoCode
                if generic_transaction:
                    if generic_transaction.platform not in GENERIC_CLAIMCODE_ALLOWED_PLATFORMS:
                        raise BadRequestError("Transaction platform can't reset claim code.", 401)

                    generic_transaction.is_invalid = True
                    generic_transaction.put()

                    Record.redemption_count -= 1
                    Record.put()
                else:
                    raise BadRequestError("Mobnum did not redeem claim code.", 401)
            else:
                if Record.expired_date:
                    if asia_manila_timezone(datetime.datetime.now()) <= Record.expired_date and Record.redeemed_on is None and not Record.is_expired:
                        raise BadRequestError("Pending redemption\n", 401)

                    if asia_manila_timezone(datetime.datetime.now()) > Record.expired_date:
                        raise BadRequestError("Unable to reset this claim code, because the expiration date is less than the current date\n", 403)

                if Record.redeemed_on is None and not Record.is_expired:
                    raise BadRequestError("Pending redemption\n", 401)

                Record.redeemed_on = None
                Record.booked_seat_count = None
                Record.booked_price_per_seat = None
                Record.theaterorg_id = None
                Record.is_expired = False
                Record.is_redeemed = False
                Record.booking_details = None
                Record.put()

        except BadRequestError, (instance):
            self.error(instance.code)
            self.response.out.write(instance.msg)


class ExpireHandler(webapp2.RequestHandler):
    def get(self):
        promo_code = self.request.get("promo_code")
        try:
            if promo_code == "" or not promo_code or promo_code is None or re.match('\w{16}$', promo_code) is None:
                raise BadRequestError("Error in parameters.", 400)

            # check if the claim code exists
            q = PromoCode.query(PromoCode.promo_code == promo_code)
            Record = q.get()

            if Record is None:
                raise BadRequestError("Invalid code.", 401)

            if Record.is_expired:
                raise BadRequestError("This claim code is expired\n", 401)

            if Record.expired_date:
                if asia_manila_timezone(datetime.datetime.now()) > Record.expired_date:
                    raise BadRequestError("This claim code is expired\n", 401)

            if Record.redeemed_on is not None:
                raise BadRequestError("This claim code has already been redeemed\n", 401)

            Record.is_expired = True
            Record.put()
        except BadRequestError, (instance):
            self.error(instance.code)
            self.response.out.write(instance.msg)


class PromoCategoryHandler(webapp2.RequestHandler):
    def get(self):
        log.debug("PromoCategoryHandler, GET, start...")

        try:
            action = self.request.get('action', None)

            if not action:
                log.warn("ERROR!, PromoCategoryHandler, GET, missing parameter action...")

                raise BadRequestError(DEFAULT_ERROR_MESSAGE_ADMIN, 401)

            response_object = {}

            if action == 'UPDATE_PROMO':
                log.debug("PromoCategoryHandler, GET, action: UPDATE_PROMO...")

                request_id = self.request.get('id', None)

                if not request_id:
                    log.warn("ERROR!, PromoCategoryHandler, GET, missing parameter id...")

                    raise BadRequestError(DEFAULT_ERROR_MESSAGE_ADMIN, 404)

                promo_category = self.get_exisitng_promocategory(request_id)

                response_object = promo_category.to_entity()
                response_object['id'] = str(promo_category.key.id())
                response_object['seat_price'] = parse_float_to_string(float(promo_category.seat_price))
                response_object['movies'] = [movie_key.id() for movie_key in promo_category.movies] if promo_category.movies else []
                response_object['theaters'] = [encoded_theater_id(theater_key) for theater_key in promo_category.theaters] if promo_category.theaters else []
                response_object['redeem_count'] = str(promo_category.redeem_count)

                if promo_category.globe_subsidize_amount:
                    response_object['globe_subsidize_amount'] = parse_float_to_string(float(promo_category.globe_subsidize_amount))

                if promo_category.thirdparty_subsidize_amount:
                    response_object['thirdparty_subsidize_amount'] = parse_float_to_string(float(promo_category.thirdparty_subsidize_amount))
            elif action == 'UPDATE_CLAIMCODE':
                log.debug("PromoCategoryHandler, GET, action: UPDATE_CLAIMCODE...")
                
                claimcode_string = self.request.get('claim_code', None)
                
                if not claimcode_string:
                    log.warn("ERROR!, PromoCategoryHandler, GET, missing parameter claimcode_string...")

                    raise BadRequestError(DEFAULT_ERROR_MESSAGE_ADMIN, 404)
                
                claim_code = self.get_existing_claimcode(claimcode_string)
        
                response_object = claim_code.to_entity()
                response_object['id'] = str(claim_code.key.id())
                response_object['seat_price'] = parse_float_to_string(float(claim_code.price_per_seat))
                response_object['redeem_count'] = str(claim_code.redeem_count)
                response_object['parent_id'] = str(claim_code.key.parent().id()) if claim_code.key.parent() else ''
            elif action == 'FILTER_MPASS_WALLET':
                log.debug("PromoCategoryHandler, GET, action: FILTER_MPASS_WALLET...")
                mpass_wallets = MpassAccounts.query(MpassAccounts.status==1).fetch()
                wallet_list = [{'id': str(wallet.key.id()), 'name': wallet.name} for wallet in mpass_wallets]
                response_object['wallets'] = wallet_list
            elif action == 'FILTER_MOVIES':
                log.debug("PromoCategoryHandler, GET, action: FILTER_MOVIES...")

                movies = Movie.query(Movie.is_active==True).fetch()
                movies_list = [{'id': str(movie.key.id()), 'name': movie.name} for movie in movies]

                response_object['movies'] = movies_list
            elif action == 'FILTER_THEATERS':
                log.debug("PromoCategoryHandler, GET, action: FILTER_THEATERS...")

                theaters = Theater.query(Theater.is_active==True).fetch()
                theaters_list = [{'id': encoded_theater_id(theater.key), 'name': theater.name} for theater in theaters]

                response_object['theaters'] = theaters_list
            else:
                log.warn("ERROR!, GeneratePromoCategoryHandler, GET, action not supported...")

                raise BadRequestError(DEFAULT_ERROR_MESSAGE_ADMIN, 401)

            results = {'return': {'code': 200, 'message': '', 'results': response_object}}
            self.response.out.write(json.dumps(results))
        except BadRequestError, instance:
            log.warn("ERROR!, PromoCategoryHandler, GET, BadRequestError...")
            log.error("status_code: %s, message: %s..." % (instance.code, instance.msg))

            results = {'return': {'code': instance.code, 'message': instance.msg}}

            self.error(instance.code)
            self.response.out.write(json.dumps(results))
        except Exception, e:
            log.warn("ERROR!, PromoCategoryHandler, GET...")
            log.error(e)

            results = {'return': {'code': 500, 'message': DEFAULT_ERROR_MESSAGE_ADMIN}}

            self.error(500)
            self.response.out.write(json.dumps(results))

    def post(self):
        log.debug("PromoCategoryHandler, POST, start...")

        try:
            json_response = json.loads(self.request.body)

            if 'action' not in json_response:
                log.warn("ERROR!, PromoCategoryHandler, POST, missing parameter action...")

                raise BadRequestError(DEFAULT_ERROR_MESSAGE_ADMIN, 400)

            if 'id' not in json_response and json_response['action'] not in ['CREATE_PROMO', 'RESET_CLAIMCODE', 'EXPIRE_CLAIMCODE']:
                log.warn("PromoCategoryHandler, POST, missing parameter id...")

                raise BadRequestError(DEFAULT_ERROR_MESSAGE_ADMIN, 404)

            if json_response['action'] == 'CREATE_PROMO':
                log.debug("PromoCategoryHandler, POST, action: CREATE_PROMO...")

                promo_category = self.do_create_promocategory(json_response)
            elif json_response['action'] == 'UPDATE_PROMO':
                log.debug("PromoCategoryHandler, POST, action: UPDATE_PROMO...")

                promo_category = self.do_update_promocategory(json_response)
            elif json_response['action'] == 'FILTER_MOVIES':
                log.debug("PromoCategoryHandler, POST, action: FILTER_MOVIES...")

                promo_category = self.do_filter_movies(json_response)
            elif json_response['action'] == 'FILTER_THEATERS':
                log.debug("PromoCategoryHandler, POST, action: FILTER_THEATERS...")

                promo_category = self.do_filter_theaters(json_response)
            elif json_response['action'] == 'GENERATE_CLAIMCODES':
                log.debug("PromoCategoryHandler, POST, action: GENERATE_CLAIMCODES...")

                promo_category = self.get_exisitng_promocategory(json_response['id'])

                deferred.defer(generate_claimcodes, promo_category, int(json_response['claimcode_quantity']))
            elif json_response['action'] == 'GENERATE_SPECIFIC_CLAIMCODES':
                log.debug("PromoCategoryHandler, POST, action: GENERATE_SPECIFIC_CLAIMCODES...")

                promo_category = self.get_exisitng_promocategory(json_response['id'])

                deferred.defer(generate_specific_claimcodes, promo_category, json_response['claim_codes'])
            elif json_response['action'] == 'UPDATE_CLAIMCODE':
                log.debug("PromoCategoryHandler, POST, action: UPDATE_CLAIMCODE...")

                promo_category = None # bypass the parent (PromoCategory) to be usable to claimcodes without parent.
                claim_code = self.do_update_claimcode(json_response)
            elif json_response['action'] == 'RESEND_EMAILTO':
                log.debug("PromoCategoryHandler, POST, action: RESEND_EMAILTO...")

                promo_category = self.get_exisitng_promocategory(json_response['id'])
                claimcodes_queryset = PromoCode.query(ancestor=promo_category.key)
                claim_codes = [claim_code.promo_code for claim_code in claimcodes_queryset]

                mail_body = create_claimcodes_emailbody(promo_category, claim_codes, 'Generated')
                mail.send_mail(sender=MAIL_DEFAULT_FROM_TEMPLATE, to=promo_category.email_to,
                        bcc=BCC_DEFAULT_EMAIL, subject="GMovies Claim Code Generation", body=mail_body)
            elif json_response['action'] == 'EXPORT_XLSREPORTS':
                log.debug("PromoCategoryHandler, POST, action: EXPORT_XLSREPORTS...")

                promo_category = self.get_exisitng_promocategory(json_response['id'])
            elif json_response['action'] == 'RESET_CLAIMCODE':
                log.debug("PromoCategoryHandler, POST, action: RESET_CLAIMCODE...")

                promo_category = None # bypass the parent (PromoCategory) to be usable to claimcodes without parent.
                claim_code = self.do_reset_claimcode(json_response)
            elif json_response['action'] == 'EXPIRE_CLAIMCODE':
                log.debug("PromoCategoryHandler, POST, action: EXPIRE_CLAIMCODE...")

                promo_category = None # bypass the parent (PromoCategory) to be usable to claimcodes without parent.
                claim_code = self.do_expire_claimcode(json_response)
            else:
                log.warn("ERROR!, PromoCategoryHandler, POST, action not supported...")

                raise BadRequestError(DEFAULT_ERROR_MESSAGE_ADMIN, 400)

            parent_id = promo_category.key.id() if promo_category else ''
            results = {'return': {'code': 200, 'message': '', 'id': parent_id}}
            self.response.out.write(json.dumps(results))
        except BadRequestError, instance:
            log.warn("ERROR!, PromoCategoryHandler, POST, BadRequestError...")
            log.error("status_code: %s, message: %s..." % (instance.code, instance.msg))

            results = {'return': {'code': instance.code, 'message': instance.msg}}

            self.error(instance.code)
            self.response.out.write(json.dumps(results))
        except Exception, e:
            import traceback
            log.error(traceback.format_exc())
            log.warn("ERROR!, PromoCategoryHandler, POST...")
            log.error(e)

            results = {'return': {'code': 500, 'message': DEFAULT_ERROR_MESSAGE_ADMIN}}

            self.error(500)
            self.response.out.write(json.dumps(results))

    def get_exisitng_promocategory(self, promo_category_id):
        promo_category_key = ndb.Key(PromoCategory, promo_category_id)
        promo_category = promo_category_key.get()

        if not promo_category:
            log.warn("ERROR!, PromoCategoryHandler, get_exisitng_promocategory, promo_category not found...")

            raise BadRequestError(DEFAULT_ERROR_MESSAGE_ADMIN, 404)

        return promo_category

    def get_existing_claimcode(self, claimcode_string):
        
        claim_code = PromoCode.query(PromoCode.promo_code==claimcode_string).get()
        
        if not claim_code:
            log.warn("ERROR!, PromoCategoryHandler, get_existing_claimcode, claim_code not found...")

            raise BadRequestError(DEFAULT_ERROR_MESSAGE_ADMIN, 404)

        return claim_code

    def get_screening_datetime(self, screening_date, screening_time):
        screening_datetime = None

        if screening_date and screening_time:
            screening_string = '%s %s' % (screening_date, screening_time)
            screening_datetime = parse_string_to_datetime(screening_string, DATETIME_FORMAT)

            if not screening_datetime:
                log.warn("ERROR!, PromoCategory, POST, do_create_promocategory, screening_datetime invalid format...")

                raise BadRequestError("Invalid Format. Please check Screening Date & Time and try again.", 400)
        #elif screening_date and not screening_time:
        #    log.warn("ERROR!, PromoCategory, POST, do_create_promocategory, missing screening_time value...")

        #    raise BadRequestError("Please input Screening Time and try again.", 400)
        elif screening_time and not screening_date:
            log.warn("ERROR!, PromoCategory, POST, do_create_promocategory, missing screening value...")

            raise BadRequestError("Please input Screening Date and try again.", 400)

        return screening_datetime

    def get_start_datetime(self, start_date, start_time):
        start_datetime = None

        if start_date and start_time:
            start_string = '%s %s' % (start_date, start_time)
            start_datetime = parse_string_to_datetime(start_string, DATETIME_FORMAT)

            if not start_datetime:
                log.warn("ERROR!, PromoCategory, POST, do_create_promocategory, start_datetime invalid format...")

                raise BadRequestError("Invalid Format. Please check Start Date & Time and try again.", 400)
        elif start_date and not start_time:
            log.warn("ERROR!, PromoCategory, POST, do_create_promocategory, missing start_time value...")

            raise BadRequestError("Please input Start Time and try again.", 400)
        elif start_time and not start_date:
            log.warn("ERROR!, PromoCategory, POST, do_create_promocategory, missing start_date value...")

            raise BadRequestError("Please input Start Date and try again.", 400)

        return start_datetime

    def get_expiration_datetime(self, expiration_date, expiration_time):
        expiration_datetime = None

        if expiration_date and expiration_time:
            expiration_string = '%s %s' % (expiration_date, expiration_time)
            expiration_datetime = parse_string_to_datetime(expiration_string, DATETIME_FORMAT)

            if not expiration_datetime:
                log.warn("ERROR!, PromoCategory, POST, do_create_promocategory, expiration_datetime invalid format...")

                raise BadRequestError("Invalid Format. Please check Expiration Date & Time and try again.", 400)
        elif expiration_date and not expiration_time:
            log.warn("ERROR!, PromoCategory, POST, do_create_promocategory, missing expiration_time value...")

            raise BadRequestError("Please input Expiration Time and try again.", 400)
        elif expiration_time and not expiration_date:
            log.warn("ERROR!, PromoCategory, POST, do_create_promocategory, missing expiration_date value...")

            raise BadRequestError("Please input Expiration Date and try again.", 400)

        return expiration_datetime

    def format_mobile_numbers(self, mobnum_list):
        formatted_mobile_list = []
        if mobnum_list:
            for mobnum in mobnum_list.split(','):
                mobnum = mobnum.strip()
                if mobnum[0] == '+' and len(mobnum) == 13:
                    mobnum = ''.join(['0', mobnum[3:]])
                elif mobnum[0] == '6' and len(mobnum) == 12:
                    mobnum = ''.join(['0', mobnum[2:]])
                elif mobnum[0] == '9' and len(mobnum) == 10:
                    mobnum = ''.join(['0', mobnum])
                elif mobnum[0] == '0' and len(mobnum) == 11:
                    pass
                elif mobnum[-1].lower() == 'x':
                    continue
                else:
                    continue

                formatted_mobile_list.append(mobnum)
            
        return formatted_mobile_list

    def do_create_promocategory(self, json_response):
        screening_datetime = self.get_screening_datetime(json_response['screening_date'], json_response['screening_time'])
        start_datetime = self.get_start_datetime(json_response['start_date'], json_response['start_time'])
        expiration_datetime = self.get_expiration_datetime(json_response['expiration_date'], json_response['expiration_time'])

        if start_datetime and expiration_datetime and start_datetime > expiration_datetime:
            log.warn("ERROR!, PromoCategoryHandler, POST, do_create_promocategory, start_datetime greater than expiration_datetime...")

            raise BadRequestError("Please input Start Date less than Expiration Date.", 400)
        
        elif screening_datetime and screening_datetime > expiration_datetime:
            log.warn("ERROR!, PromoCategoryHandler, POST, do_create_promocategory, screening_datetime greater than expiration_datetime...")

            raise BadRequestError("Please input Screening Date less than Expiration Date.", 400)

        seat_price = float(json_response['seat_price'])
        max_amount = float(json_response['seat_price']) * int(json_response['max_seats'])
        seat_price_string = parse_float_to_string(seat_price)
        max_amount_string = parse_float_to_string(max_amount)

        globe_subsidize_string = None
        thirdparty_subsidize_string = None
        thirdparty_company_name = None

        if 'globe_subsidize_amount' in json_response and json_response['globe_subsidize_amount']:
            globe_subsidize = float(json_response['globe_subsidize_amount'])
            globe_subsidize_string = parse_float_to_string(globe_subsidize)

        if 'thirdparty_subsidize_amount' in json_response and json_response['thirdparty_subsidize_amount']:
            thirdparty_subsidize = float(json_response['thirdparty_subsidize_amount'])
            thirdparty_subsidize_string = parse_float_to_string(thirdparty_subsidize)

            if 'thirdparty_company_name' in json_response and json_response['thirdparty_company_name']:
                thirdparty_company_name = json_response['thirdparty_company_name']

        promo_category = PromoCategory(key=ndb.Key(PromoCategory, str(uuid_gen())))
        promo_category.name = json_response['name']
        promo_category.correlation = get_all_correlation_titles(json_response['name'])
        promo_category.prefix = json_response['prefix']
        promo_category.created_by = users.get_current_user().email()
        promo_category.redeem_count = int(json_response['redeem_count'])
        promo_category.is_generic = True if 'GENERIC' == json_response['code_type'] else False
        promo_category.max_seats = int(json_response['max_seats'])
        promo_category.min_seats = int(json_response['min_seats'])
        promo_category.max_amount = Decimal(max_amount_string)
        promo_category.seat_price = Decimal(seat_price_string)
        promo_category.screening_date = screening_datetime.date() if screening_datetime else None
        promo_category.screening_time = screening_datetime.time() if screening_datetime else None
        promo_category.start_date = start_datetime.date() if start_datetime else None
        promo_category.start_time = start_datetime.time() if start_datetime else None
        promo_category.expiration_date = expiration_datetime.date() if expiration_datetime else None
        promo_category.expiration_time = expiration_datetime.time() if expiration_datetime else None
        promo_category.whitelist = self.format_mobile_numbers(json_response['whitelist'])
        promo_category.blacklist = self.format_mobile_numbers(json_response['blacklist'])
        promo_category.movies = self.do_filter_movies(json_response['movie_ids'])
        promo_category.theaters = self.do_filter_theaters(json_response['theater_ids'])
        promo_category.mpass_wallet = str(json_response['mpass_wallet']) if str(json_response['mpass_wallet']) else None
        promo_category.is_valid = json_response['is_valid']
        promo_category.globe_subsidize_amount = Decimal(globe_subsidize_string) if globe_subsidize_string else None
        promo_category.thirdparty_company_name = thirdparty_company_name if thirdparty_company_name else ''
        promo_category.thirdparty_subsidize_amount = Decimal(thirdparty_subsidize_string) if thirdparty_subsidize_string else None
        promo_category.email_to = json_response['email_to']
        promo_category.updated_by = users.get_current_user().email()
        promo_category.put()
        
        if 'GENERIC' == json_response['code_type']:
            # For Generic Claim Codes, PREFIX contains the specific claim codes with delimiter ';'
            claim_codes = json_response['generic_codes'].split(';')
            # Remove items that evaluate to False (empty string, 0, False boolean)
            deferred.defer(generate_specific_claimcodes, promo_category, filter(None, claim_codes))
        else:
            deferred.defer(generate_claimcodes, promo_category, int(json_response['claimcode_quantity']))

        return promo_category

    def do_update_promocategory(self, json_response):
        screening_datetime = self.get_screening_datetime(json_response['screening_date'], json_response['screening_time'])
        start_datetime = self.get_start_datetime(json_response['start_date'], json_response['start_time'])
        expiration_datetime = self.get_expiration_datetime(json_response['expiration_date'], json_response['expiration_time'])
        
        if start_datetime and expiration_datetime and start_datetime > expiration_datetime:
            log.warn("ERROR!, PromoCategoryHandler, POST, do_update_promocategory, start_datetime greater than expiration_datetime...")

            raise BadRequestError("Please input Start Date less than Expiration Date.", 400)

        elif screening_datetime and screening_datetime > expiration_datetime:
            log.warn("ERROR!, PromoCategoryHandler, POST, do_create_promocategory, screening_datetime greater than expiration_datetime...")

            raise BadRequestError("Please input Screening Date less than Expiration Date.", 400)

        seat_price = float(json_response['seat_price'])
        max_amount = float(json_response['seat_price']) * int(json_response['max_seats'])
        seat_price_string = parse_float_to_string(seat_price)
        max_amount_string = parse_float_to_string(max_amount)

        globe_subsidize_string = None
        thirdparty_subsidize_string = None
        thirdparty_company_name = None

        if 'globe_subsidize_amount' in json_response and json_response['globe_subsidize_amount']:
            globe_subsidize = float(json_response['globe_subsidize_amount'])
            globe_subsidize_string = parse_float_to_string(globe_subsidize)

        if 'thirdparty_subsidize_amount' in json_response and json_response['thirdparty_subsidize_amount']:
            thirdparty_subsidize = float(json_response['thirdparty_subsidize_amount'])
            thirdparty_subsidize_string = parse_float_to_string(thirdparty_subsidize)

            if 'thirdparty_company_name' in json_response and json_response['thirdparty_company_name']:
                thirdparty_company_name = json_response['thirdparty_company_name']

        promo_category = self.get_exisitng_promocategory(json_response['id'])
        promo_category.min_seats = int(json_response['min_seats'])
        promo_category.max_seats = int(json_response['max_seats'])
        promo_category.max_amount = Decimal(max_amount_string)
        promo_category.seat_price = Decimal(seat_price_string)
        promo_category.screening_date = screening_datetime.date() if screening_datetime else None
        promo_category.screening_time = screening_datetime.time() if screening_datetime else None
        promo_category.start_date = start_datetime.date() if start_datetime else None
        promo_category.start_time = start_datetime.time() if start_datetime else None
        promo_category.expiration_date = expiration_datetime.date() if expiration_datetime else None
        promo_category.expiration_time = expiration_datetime.time() if expiration_datetime else None
        promo_category.whitelist = self.format_mobile_numbers(json_response['whitelist'])
        promo_category.blacklist = self.format_mobile_numbers(json_response['blacklist'])
        promo_category.movies = self.do_filter_movies(json_response['movie_ids'])
        promo_category.theaters = self.do_filter_theaters(json_response['theater_ids'])
        promo_category.mpass_wallet = str(json_response['mpass_wallet']) if str(json_response['mpass_wallet']) else None
        promo_category.is_valid = json_response['is_valid']
        promo_category.globe_subsidize_amount = Decimal(globe_subsidize_string) if globe_subsidize_string else None
        promo_category.thirdparty_company_name = thirdparty_company_name if thirdparty_company_name else ''
        promo_category.thirdparty_subsidize_amount = Decimal(thirdparty_subsidize_string) if thirdparty_subsidize_string else None
        promo_category.email_to = json_response['email_to']
        promo_category.updated_by = users.get_current_user().email()
        promo_category.redeem_count = int(json_response['redeem_count'])
        promo_category.put()

        deferred.defer(update_claimcodes, promo_category)

        return promo_category

    def do_update_claimcode(self, json_response):
        screening_datetime = self.get_screening_datetime(json_response['screening_date'], json_response['screening_time'])
        expiration_datetime = self.get_expiration_datetime(json_response['expiration_date'], json_response['expiration_time'])
        claim_code = self.get_existing_claimcode(json_response['claim_code'])

        seat_price = float(json_response['seat_price'])
        seat_price_string = parse_float_to_string(seat_price)

        claim_code.seat_count = int(json_response['max_seats'])
        claim_code.min_seats = int(json_response['min_seats'])
        claim_code.price_per_seat = Decimal(seat_price_string)
        claim_code.screening_date = screening_datetime.date() if screening_datetime else None
        claim_code.screening_time = screening_datetime.time() if screening_datetime else None
        claim_code.expired_date = expiration_datetime
        claim_code.whitelist = self.format_mobile_numbers(json_response['whitelist'])
        claim_code.blacklist = self.format_mobile_numbers(json_response['blacklist'])
        claim_code.is_invalid = True if not json_response['is_valid'] else False
        claim_code.redeem_count = int(json_response['redeem_count'])
        claim_code.put()

        return claim_code

    def do_reset_claimcode(self, json_response):
        claim_code = self.get_existing_claimcode(json_response['claim_code'])

        if claim_code.expired_date and convert_timezone(datetime.datetime.now(), 8, '+') > claim_code.expired_date:
            log.warn("ERROR!, PromoCategoryHandler, POST, do_reset_claimcode, expired_date less than current date...")

            raise BadRequestError("Unable to reset %s claim code, because expiration date is less than current date." % claim_code.promo_code, 401)

        if (claim_code.expired_date and convert_timezone(datetime.datetime.now(), 8, '+') <= claim_code.expired_date and
                not claim_code.is_redeemed and not claim_code.is_expired and not claim_code.redeemed_on):
            log.warn("ERROR!, PromoCategoryHandler, POST, do_reset_claimcode, pending for redemption...")

            raise BadRequestError("%s claim code pending for redemption." % claim_code.promo_code, 401)

        if not claim_code.is_redeemed and not claim_code.is_expired and not claim_code.redeemed_on:
            log.warn("ERROR!, PromoCategoryHandler, POST, do_reset_claimcode, pending for redemption...")

            raise BadRequestError("%s claim code pending for redemption." % claim_code.promo_code, 401)

        claim_code.is_redeemed = False
        claim_code.is_expired = False
        claim_code.redeemed_on = None
        claim_code.booking_details = None
        claim_code.booked_seat_count = None
        claim_code.booked_price_per_seat = None
        claim_code.theaterorg_id = None
        claim_code.put()

        return claim_code

    def do_expire_claimcode(self, json_response):
        claim_code = self.get_existing_claimcode(json_response['claim_code'])

        if claim_code.is_expired:
            log.warn("ERROR!, PromoCategoryHandler, POST, do_expire_claimcode, already expired...")

            raise BadRequestError("%s claim code already expired." % claim_code.promo_code, 401)

        if claim_code.expired_date and convert_timezone(datetime.datetime.now(), 8, '+') > claim_code.expired_date:
            log.warn("ERROR!, PromoCategoryHandler, POST, do_expire_claimcode, already expired...")

            raise BadRequestError("%s claim code already expired." % claim_code.promo_code, 401)

        if claim_code.redeemed_on or claim_code.is_redeemed:
            log.warn("ERROR!, PromoCategoryHandler, POST, do_expire_claimcode, already redeemed...")

            raise BadRequestError("%s claim code already redeemed." % claim_code.promo_code, 401)

        claim_code.is_expired = True
        claim_code.put()

        return claim_code

    def do_filter_movies(self, movie_ids):
        movie_keys = []

        for movie_id in movie_ids:
            movie_key = ndb.Key(Movie, movie_id)

            if movie_key.get():
                movie_keys.append(movie_key)

        return movie_keys

    def do_filter_theaters(self, theater_ids):
        theater_keys = []

        for theater_id in theater_ids:
            org_uuid, theater_id = decoded_theater_id(theater_id)
            theater_key = ndb.Key(TheaterOrganization, str(org_uuid), Theater, int(theater_id))

            if theater_key.get():
                theater_keys.append(theater_key)

        return theater_keys

    def do_filter_cinemas_and_seats(self, json_response):
        promo_category = self.get_exisitng_promocategory(json_response['id'])
        promo_category.cinemas = json_response['cinemas']
        promo_category.seats = json_response['seats']
        promo_category.put()

        return promo_category


def do_create_claimcode(prefix=None):
    prefix = '' if not prefix else prefix

    while(True):
        suffix_length = 16 - len(prefix)
        claimcode_suffix = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(suffix_length))
        claimcode_string =  prefix.upper() + claimcode_suffix
        claimcode_string = claimcode_string.strip()
        is_claimcode_existing = PromoCode.query(PromoCode.promo_code==claimcode_string).get()

        if not is_claimcode_existing:
            break

    return claimcode_string

def generate_claimcodes(promo_category, claimcode_quantity):
    counter = 0
    batch_quantity = CLAIMCODE_BATCH_QUANTITY
    expiration_datetime = None
    screening_datetime = None

    if promo_category.expiration_date and promo_category.expiration_time:
        expiration_string = '%s %s' % (str(promo_category.expiration_date), str(promo_category.expiration_time))
        expiration_datetime = parse_string_to_datetime(expiration_string, DATETIME_FORMAT)

    if promo_category.screening_date and promo_category.screening_time:
        screening_string = '%s %s' % (str(promo_category.screening_date), str(promo_category.screening_time))
        screening_datetime = parse_string_to_datetime(screening_string, DATETIME_FORMAT)

    if claimcode_quantity <= batch_quantity:
        batch_quantity = claimcode_quantity

    for i in range(batch_quantity):
        counter += 1
        claimcode_string = do_create_claimcode(promo_category.prefix)

        claim_code = PromoCode(parent=promo_category.key)
        claim_code.promo_code = claimcode_string
        claim_code.promo_name = promo_category.name
        claim_code.seat_count = promo_category.max_seats
        claim_code.min_seats = promo_category.min_seats
        claim_code.price_per_seat = promo_category.seat_price
        claim_code.screening_date = screening_datetime.date() if screening_datetime else None
        claim_code.screening_time = screening_datetime.time() if screening_datetime else None
        claim_code.expired_date = expiration_datetime
        claim_code.whitelist = promo_category.whitelist
        claim_code.blacklist = promo_category.blacklist
        claim_code.batch_code = promo_category.prefix
        claim_code.is_invalid = True if not promo_category.is_valid else False
        claim_code.is_batch = True
        claim_code.msisdn = DEFAULT_MSISDN
        claim_code.pin = DEFAULT_PIN
        claim_code.issued_on = convert_timezone(datetime.datetime.now(), 8, '+')
        claim_code.redeem_count = promo_category.redeem_count
        claim_code.put()

    claimcode_quantity -= counter
    claimcodes_queryset = PromoCode.query(ancestor=promo_category.key)

    if claimcode_quantity > 0:
        log.debug("generate_claimcodes, put %d entities to datastore for a total of %d" % (counter, claimcodes_queryset.count()))

        deferred.defer(generate_claimcodes, promo_category, claimcode_quantity)
    else:
        log.debug("generate_claimcodes, complete, with %d generated" % claimcodes_queryset.count())

        try:
            log.debug("generate_claimcodes, sending email...")

            claimcodes_generated = [claim_code.promo_code for claim_code in claimcodes_queryset]
            mail_body = create_claimcodes_emailbody(promo_category, claimcodes_generated, 'Generated')
            mail.send_mail(sender=MAIL_DEFAULT_FROM_TEMPLATE, to=promo_category.email_to,
                    bcc=BCC_DEFAULT_EMAIL, subject="GMovies Claim Code Generation", body=mail_body)

            log.debug("generate_claimcodes, email sent...")
        except Exception, e:
            log.warn("ERROR!, generate_claimcodes, failed to send email...")
            log.error(e)

def update_claimcodes(promo_category, cursor=''):
    counter = 0
    expiration_datetime = None
    screening_datetime = None
    cursor = ndb.Cursor(urlsafe=cursor)
    claimcodes_queryset = PromoCode.query(ancestor=promo_category.key)
    claimcodes_batch, next_cursor, more = claimcodes_queryset.fetch_page(CLAIMCODE_BATCH_QUANTITY, start_cursor=cursor)

    if promo_category.expiration_date and promo_category.expiration_time:
        expiration_string = '%s %s' % (str(promo_category.expiration_date), str(promo_category.expiration_time))
        expiration_datetime = parse_string_to_datetime(expiration_string, DATETIME_FORMAT)

    if promo_category.screening_date and promo_category.screening_time:
        screening_string = '%s %s' % (str(promo_category.screening_date), str(promo_category.screening_time))
        screening_datetime = parse_string_to_datetime(screening_string, DATETIME_FORMAT)

    for claim_code in claimcodes_batch:
        counter += 1
        claim_code.seat_count = promo_category.max_seats
        claim_code.min_seats = promo_category.min_seats
        claim_code.price_per_seat = promo_category.seat_price
        claim_code.screening_date = screening_datetime.date() if screening_datetime else None
        claim_code.screening_time = screening_datetime.time() if screening_datetime else None
        claim_code.expired_date = expiration_datetime
        claim_code.whitelist = promo_category.whitelist
        claim_code.blacklist = promo_category.blacklist
        claim_code.is_invalid = True if not promo_category.is_valid else False
        claim_code.redeem_count = promo_category.redeem_count
        claim_code.put()

    if more:
        log.debug("update_claimcodes, put %d entities to datastore for a total of %d" % (counter, claimcodes_queryset.count()))

        deferred.defer(update_claimcodes, promo_category, cursor=next_cursor.urlsafe())
    else:
        log.debug("update_claimcodes, complete, with %d updates" % claimcodes_queryset.count())

        try:
            log.debug("update_claimcodes, sending email...")

            claimcodes_updated = [claim_code.promo_code for claim_code in claimcodes_queryset]
            mail_body = create_claimcodes_emailbody(promo_category, claimcodes_updated, 'Updated')
            mail.send_mail(sender=MAIL_DEFAULT_FROM_TEMPLATE, to=promo_category.email_to,
                    bcc=BCC_DEFAULT_EMAIL, subject="GMovies Claim Code Update", body=mail_body)

            log.debug("update_claimcodes, email sent...")
        except Exception, e:
            log.warn("ERROR!, update_claimcodes, failed to send email...")
            log.error(e)

def generate_specific_claimcodes(promo_category, claim_codes):
    counter = 0
    batch_quantity = CLAIMCODE_BATCH_QUANTITY
    expiration_datetime = None
    screening_datetime = None

    if promo_category.expiration_date and promo_category.expiration_time:
        expiration_string = '%s %s' % (str(promo_category.expiration_date), str(promo_category.expiration_time))
        expiration_datetime = parse_string_to_datetime(expiration_string, DATETIME_FORMAT)

    if promo_category.screening_date and promo_category.screening_time:
        screening_string = '%s %s' % (str(promo_category.screening_date), str(promo_category.screening_time))
        screening_datetime = parse_string_to_datetime(screening_string, DATETIME_FORMAT)

    for code in [c for c in claim_codes][:batch_quantity]:
        claim_code = PromoCode.query(PromoCode.promo_code==code).get()

        if code in claim_codes:
            log.debug("generate_specific_claimcodes, remove claim code from list...")

            claim_codes.remove(code)

        if claim_code:
            log.warn("SKIP!, generate_specific_claimcodes, claim code already existing...")

            continue

        counter += 1
        claim_code = PromoCode(parent=promo_category.key)
        claim_code.promo_code = code
        claim_code.promo_name = promo_category.name
        claim_code.seat_count = promo_category.max_seats
        claim_code.min_seats = promo_category.min_seats
        claim_code.price_per_seat = promo_category.seat_price
        claim_code.screening_date = screening_datetime.date() if screening_datetime else None
        claim_code.screening_time = screening_datetime.time() if screening_datetime else None
        claim_code.expired_date = expiration_datetime
        claim_code.whitelist = promo_category.whitelist
        claim_code.blacklist = promo_category.blacklist
        claim_code.batch_code = promo_category.prefix
        claim_code.is_invalid = True if not promo_category.is_valid else False
        claim_code.is_batch = True
        claim_code.msisdn = DEFAULT_MSISDN
        claim_code.pin = DEFAULT_PIN
        claim_code.issued_on = convert_timezone(datetime.datetime.now(), 8, '+')
        claim_code.is_generic = True
        claim_code.redeem_count = promo_category.redeem_count
        claim_code.put()

    claimcodes_queryset = PromoCode.query(ancestor=promo_category.key)

    if claim_codes:
        log.debug("generate_specific_claimcodes, put %d entities to datastore for a total of %d" % (counter, claimcodes_queryset.count()))

        deferred.defer(generate_specific_claimcodes, promo_category, claim_codes)
    else:
        log.debug("generate_specific_claimcodes, complete, with %d generated" % claimcodes_queryset.count())

        try:
            log.debug("generate_specific_claimcodes, sending email...")

            claimcodes_generated = [claim_code.promo_code for claim_code in claimcodes_queryset]
            mail_body = create_claimcodes_emailbody(promo_category, claimcodes_generated, 'Generated')
            mail.send_mail(sender=MAIL_DEFAULT_FROM_TEMPLATE, to=promo_category.email_to,
                    bcc=BCC_DEFAULT_EMAIL, subject="GMovies Claim Code Generation", body=mail_body)

            log.debug("generate_specific_claimcodes, email sent...")
        except Exception, e:
            log.warn("ERROR!, generate_specific_claimcodes, failed to send email...")
            log.error(e)

def create_claimcodes_emailbody(promo, claim_codes, action):
    mail_body = 'This is an auto generated email sent by the system. No need to reply.\n\n\n'
    max_seats = str(promo.max_seats)
    min_seats = str(promo.min_seats)
    max_amount = parse_float_to_string(float(promo.max_amount))
    screening = '%s %s' % (str(promo.screening_date), str(promo.screening_time)) if promo.screening_date and promo.screening_time is not None else 'N/A'
    availability = '%s %s' % (str(promo.start_date), str(promo.start_time)) if promo.start_date and promo.start_time is not None else 'N/A'
    expiration = '%s %s' % (str(promo.expiration_date), str(promo.expiration_time)) if promo.expiration_date and promo.expiration_time is not None else 'N/A'
    movie_titles = ', '.join([movie_key.get().name if movie_key.get() else '' for movie_key in promo.movies])
    movie_titles = ', '.join(movie_titles.split(', '))
    theater_names = ', '.join([theater_key.get().name if theater_key.get() else '' for theater_key in promo.theaters])
    theater_names = ', '.join(theater_names.split(', '))
    validity = 'VALID' if promo.is_valid else 'INVALID'

    mail_body += 'Promo Name: %s\n' % promo.name
    mail_body += 'Min Seats: %s\n' % min_seats
    mail_body += 'Max Seats: %s\n' % max_seats
    mail_body += 'Max Amount: %s\n' % max_amount
    mail_body += 'Screening Date: %s\n' % screening
    mail_body += 'Start Date: %s\n' % availability
    mail_body += 'Expiration Date: %s\n' % expiration
    mail_body += 'Movies Allowed: %s\n' % movie_titles if movie_titles else 'Movies Allowed: N/A\n'
    mail_body += 'Theaters Allowed: %s\n' % theater_names if theater_names else 'Theaters Allowed: N/A\n'
    mail_body += 'Validity: %s\n' % validity
    mail_body += 'Claim Codes %s: %s\n\n' % (action, len(claim_codes))
    mail_body += '\n'.join(claim_codes)

    return mail_body


class ClaimCodeSubsidizeDetailsHandler(webapp2.RequestHandler):
    def get(self):
        claim_code_string = self.request.get("promo_code")

        try:
            claim_code = PromoCode.query(PromoCode.promo_code==claim_code_string).get()

            if not claim_code:
                log.warn("ERROR!, ClaimCodeSubsidizeDetailsHandler, claim_code not found...")

                raise BadRequestError(DEFAULT_ERROR_MESSAGE_ADMIN, 404)

            promo_category = claim_code.key.parent().get() if claim_code.key.parent() else None
            globe_subsidize = parse_float_to_string(promo_category.globe_subsidize_amount) if promo_category and promo_category.globe_subsidize_amount else ''
            thirdparty_company_name = promo_category.thirdparty_company_name if promo_category and promo_category.thirdparty_company_name else ''
            thirdparty_subsidize = parse_float_to_string(promo_category.thirdparty_subsidize_amount) if promo_category and promo_category.thirdparty_subsidize_amount else ''

            response_object = {}
            response_object['claim_code'] = claim_code.promo_code
            response_object['globe_subsidize'] = globe_subsidize
            response_object['thirdparty_company_name'] = thirdparty_company_name
            response_object['thirdparty_subsidize'] = thirdparty_subsidize

            results = {'return': {'code': 200, 'message': '', 'results': response_object}}
            self.response.out.write(json.dumps(results))
        except BadRequestError, instance:
            log.warn("ERROR!, ClaimCodeSubsidizeDetailsHandler, BadRequestError...")
            log.error("status_code: %s, message: %s..." % (instance.code, instance.msg))

            results = {'return': {'code': instance.code, 'message': instance.msg}}

            self.error(instance.code)
            self.response.out.write(json.dumps(results))
        except Exception, e:
            log.warn("ERROR!, ClaimCodeSubsidizeDetailsHandler...")
            log.error(e)

            results = {'return': {'code': 500, 'message': DEFAULT_ERROR_MESSAGE_ADMIN}}

            self.error(500)
            self.response.out.write(json.dumps(results))


class ClaimCodeSubsidizeListHandler(webapp2.RequestHandler):
    def get(self):
        try:
            cursor = ndb.Cursor(urlsafe=self.request.get('cursor'))
            subsidize_details = []
            queryset = PromoCode.query(PromoCode.is_redeemed==True)

            if self.request.get('date_created'):
                created_from_str = self.request.get('date_created')
                created_from = datetime.datetime.strptime(created_from_str + ' 00:00:00', '%d %b %Y %H:%M:%S')
                created_from = convert_timezone(created_from, 8, '-') # convert to UTC.
                created_to_str = datetime.date.today().strftime('%d %b %Y')
                created_to = datetime.datetime.strptime(created_to_str + ' 23:59:59', '%d %b %Y %H:%M:%S')
                queryset = queryset.filter(PromoCode.created_on >= created_from, PromoCode.created_on <= created_to)

            queryset = queryset.order(-PromoCode.created_on)
            claim_codes_batch, next_cursor, more = queryset.fetch_page(100, start_cursor=cursor)
            next_cursor = next_cursor.urlsafe() if more else ''

            for claim_code in claim_codes_batch:
                promo_category = claim_code.key.parent().get() if claim_code.key.parent() else None
                globe_subsidize = parse_float_to_string(promo_category.globe_subsidize_amount) if promo_category and promo_category.globe_subsidize_amount else ''
                thirdparty_company_name = promo_category.thirdparty_company_name if promo_category and promo_category.thirdparty_company_name else ''
                thirdparty_subsidize = parse_float_to_string(promo_category.thirdparty_subsidize_amount) if promo_category and promo_category.thirdparty_subsidize_amount else ''

                response_object = {}
                response_object['claim_code'] = claim_code.promo_code
                response_object['globe_subsidize'] = globe_subsidize
                response_object['thirdparty_company_name'] = thirdparty_company_name
                response_object['thirdparty_subsidize'] = thirdparty_subsidize

                subsidize_details.append(response_object)

            results = {'return': {'code': 200, 'message': '', 'next_cursor': next_cursor, 'more': more, 'results': subsidize_details}}
            self.response.out.write(json.dumps(results))
        except BadRequestError, instance:
            log.warn("ERROR!, ClaimCodeSubsidizeListHandler, BadRequestError...")
            log.error("status_code: %s, message: %s..." % (instance.code, instance.msg))

            results = {'return': {'code': instance.code, 'message': instance.msg}}

            self.error(instance.code)
            self.response.out.write(json.dumps(results))
        except Exception, e:
            log.warn("ERROR!, ClaimCodeSubsidizeListHandler...")
            log.error(e)

            results = {'return': {'code': 500, 'message': DEFAULT_ERROR_MESSAGE_ADMIN}}

            self.error(500)
            self.response.out.write(json.dumps(results))


class BankDepositHandler(webapp2.RequestHandler):
    def get(self):
        expiration_date = None
        msisdn = DEFAULT_MSISDN
        pin = DEFAULT_PIN
        seat_count = 10
        email = self.request.get("email")
        purchase_amount = self.request.get("purchase_amount")
        expired_date = self.request.get("expired_date")
        expired_time = self.request.get("expired_time")
        issued_on = asia_manila_timezone(datetime.datetime.now())

        try:
            if email == "" or not email or email is None:
                raise BadRequestError("Error in email format.", 400)

            if purchase_amount == "" or not purchase_amount or purchase_amount is None:
                raise BadRequestError("Amount is Required.", 400)

            if expired_date:
                if expired_time == "" or not expired_time or expired_time is None:
                    expired_time = '23:59'

                expiration_date = parse_expiration_date(expired_date, expired_time)

            while(True):
                promo_code_suffix = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(9))
                promo_code = 'GMOVBNK' + promo_code_suffix
                qs = PromoCode.query(PromoCode.promo_code == promo_code) # Check if this value exists
                Collided = qs.get()

                if Collided is None:
                    break

            PromoCodeModel = PromoCode()
            PromoCodeModel.promo_code = promo_code
            PromoCodeModel.is_bank_deposit = True
            PromoCodeModel.issued_on = issued_on
            PromoCodeModel.msisdn = msisdn
            PromoCodeModel.pin = pin
            PromoCodeModel.seat_count = int(seat_count)
            PromoCodeModel.purchase_amount = Decimal(purchase_amount)
            PromoCodeModel.price_per_seat = None
            PromoCodeModel.expired_date = expiration_date
            PromoCodeModel.put()

            mail_body = 'Purchased Amount: %s\n' % purchase_amount
            
            if expiration_date:
                mail_body = mail_body + 'Expiration Date: %s\n' % str(expiration_date)
            else:
                mail_body = mail_body + 'Expiration Date: N/A\n'

            mail_body = mail_body + 'Generated Claim Code: %s' % promo_code

            mail.send_mail(sender=MAIL_DEFAULT_FROM_TEMPLATE, to=email,
                    bcc=BCC_DEFAULT_EMAIL,
                    subject="GMovies Bank Deposit: Claim Code Generation", body=mail_body)

            response_object = {"return": {"code": 200, "message": "Successfully generated claim code.", "promo_code": promo_code}}
            self.response.out.write(json.dumps(response_object))
        except BadRequestError, (instance):
            response_object = {"return": {"code": instance.code, "message": instance.msg}}
            self.response.out.write(json.dumps(response_object))


class GeneratePromoCodeHandler(webapp2.RequestHandler):
    def get(self):
        is_batch = False
        expiration_date = None
        generated_promo_codes = []
        msisdn = DEFAULT_MSISDN
        pin = DEFAULT_PIN
        number_of_promo_codes = DEFAULT_NUMBER_OF_PROMO_CODES
        email = self.request.get("email")
        batch_code = self.request.get("batch_code")
        seat_count = self.request.get("seat_count")
        price_per_seat = self.request.get("price_per_seat")
        expired_date = self.request.get("expired_date")
        expired_time = self.request.get("expired_time")
        promo_name = self.request.get("promo_name").lower() if self.request.get("promo_name") else None
        claim_code_type = self.request.get("claim_code_type")
        issued_on = asia_manila_timezone(datetime.datetime.now())

        try:
            if users.get_current_user().email() != 'ajasmin@yondu.com':
                log.warn("SKIP!, user is not allowed to generate claim code from this page, raise error...")

                raise BadRequestError("You're not allowed to generate claim codes from this form.", 401)

            if self.request.get("number_of_promo_per_batch"):
                is_batch = True
                number_of_promo_codes = self.request.get("number_of_promo_per_batch")

                if number_of_promo_codes == "" or not number_of_promo_codes or number_of_promo_codes is None or re.match('^[0-9]+$', number_of_promo_codes) is None:
                    raise BadRequestError("Error in No. of Claim Codes per Batch. Invalid format.", 400)

                if int(number_of_promo_codes) <= 0 or int(number_of_promo_codes) > 500:
                    raise BadRequestError("Error in No. of Claim Codes per Batch. Value must be greater than 0 and less than or equal to 500.", 400)
            else:
                if self.request.get("msisdn")[1:]:
                    msisdn = self.request.get("msisdn")[1:]
                    pin = self.request.get("pin")
                    is_use_msisdn = PromoCode.query(PromoCode.msisdn == msisdn).get()

                    if msisdn == "" or not msisdn or msisdn is None or re.match('9\d{9}$', msisdn) is None:
                        raise BadRequestError("Error in the mobile number.", 400)

                    if pin == "" or not pin or pin is None or re.match('\d{4}$', pin) is None:
                        raise BadRequestError("Error in PIN format.", 400)

                    if is_use_msisdn is not None:
                        raise BadRequestError("This Mobile No. was already used to generate Claim Code.", 401)

            if batch_code:
                if batch_code == "" or not batch_code or batch_code is None or re.match('^[A-Z0-9]{1,14}$', batch_code) is None:
                    raise BadRequestError("Error in Batch Code format.", 400)

            if email == "" or not email or email is None:
                raise BadRequestError("Error in email format.", 400)

            if seat_count == "" or not seat_count or seat_count is None:
                seat_count = DEFAULT_SEAT_COUNT

            if price_per_seat == "" or not price_per_seat or price_per_seat is None:
                price_per_seat = DEFAULT_PRICE_PER_SEAT

            if expired_date:
                if expired_time == "" or not expired_time or expired_time is None:
                    expired_time = '23:59'

                expiration_date = parse_expiration_date(expired_date, expired_time)

            try:
                for batch in range(0, int(number_of_promo_codes)):
                    promo_code = create_promo_code(batch_code)
                    PromoCodeModel = PromoCode()
                    PromoCodeModel.promo_code = promo_code
                    PromoCodeModel.msisdn = msisdn
                    PromoCodeModel.pin = pin
                    PromoCodeModel.issued_on = issued_on
                    PromoCodeModel.batch_code = batch_code
                    PromoCodeModel.is_batch = is_batch
                    PromoCodeModel.seat_count = int(seat_count)
                    PromoCodeModel.price_per_seat = Decimal(price_per_seat)
                    PromoCodeModel.expired_date = expiration_date
                    PromoCodeModel.promo_name = promo_name

                    if claim_code_type.lower() == 'invalid':
                        PromoCodeModel.is_invalid = True

                    PromoCodeModel.put()
                    generated_promo_codes.append(promo_code)

                generation_msg = "Successfully generated claim codes."
            except DeadlineExceededError, e:
                log.info(e)
                generation_msg = "Unable to finish claim code generation."
                pass
            except Exception, e:
                log.info(e)
                generation_msg = "Unable to finish claim code generation."
                pass

            log.info("Claim Code Generation: %s" % generation_msg)

            mail_body = create_mail_body(generated_promo_codes, seat_count, price_per_seat, claim_code_type, expiration_date, promo_name)
            mail.send_mail(sender=MAIL_DEFAULT_FROM_TEMPLATE, to=email,
                    bcc=BCC_DEFAULT_EMAIL,
                    subject="GMovies Claim Code Generation", body=mail_body)

            response_object = {"return": {"code": 200, "message": generation_msg, "promo_codes": generated_promo_codes}}
            self.response.out.write(json.dumps(response_object))
        except BadRequestError, (instance):
            response_object = {"return": {"code": instance.code, "message": instance.msg}}
            self.response.out.write(json.dumps(response_object))


class GetCodeHandler(webapp2.RequestHandler):
    def get(self):
        #create a test record
        #Test = PromoCode(msisdn = "9175889615", pin = "1234")
        #Test.put()
        #return

        msisdn = self.request.get("msisdn")[1:]
        pin = self.request.get("pin")
        email = self.request.get("email")
        device_id = self.request.get("device_id")

        try:
            if msisdn == "" or not msisdn or msisdn is None or re.match('9\d{9}$', msisdn) is None:
                raise BadRequestError("Error in the mobile number.", 400)

            if pin == "" or not pin or pin is None or re.match('\d{4}$', pin) is None:
                raise BadRequestError("Error in PIN format.", 400)

            if email == "" or not email or email is None:
                raise BadRequestError("Error in email format.", 400)

            # check if the claim code is actually valid
            q = PromoCode.query(PromoCode.msisdn == msisdn)
            Record = q.get()

            if Record is None:
                raise BadRequestError("Invalid Parameter.", 401)

            if Record.pin != pin:
                raise BadRequestError("Invalid Parmeters.", 401)

            if Record.issued_on is not None or Record.promo_code is not None or Record.promo_code == "":
                raise BadRequestError("Already issued code.", 402)

            while(True):
                promo_code = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(16))
                # check if this value exists
                z = PromoCode.query(PromoCode.promo_code == promo_code)
                Collided = z.get()
                if Collided is None:
                    break

            issued_on = asia_manila_timezone(datetime.datetime.now())
            Record.issued_on = issued_on
            Record.promo_code = promo_code
            Record.email = email
            Record.device_id = device_id
            Record.put()
            mail.send_mail(sender="GMovies by Globe Telecom<gmovies.support@globelabsbeta.com>", to=email, subject="GMovies Claim Code Registration ", body="Your claim code is: " + promo_code)
            response_object = {"return":{"code": 200, "message": "Successfully obtained claim code.", "promo_code":promo_code}}
            self.response.out.write(json.dumps(response_object))

        except BadRequestError, (instance):
            response_object = {"return":{"code": instance.code, "message": instance.msg}}
            #self.error(instance.code)
            self.response.out.write(json.dumps(response_object))


def create_promo_code(batch_code=None):
    while(True):
        if batch_code:
            suffix_length = 16 - len(batch_code)
            promo_code_suffix = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(suffix_length))
            promo_code = batch_code + promo_code_suffix
        else:
            promo_code = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(16))

        # check if this value exists
        z = PromoCode.query(PromoCode.promo_code == promo_code)
        Collided = z.get()

        if Collided is None:
            break

    return promo_code

def create_mail_body(promo_codes, seat_count, price_per_seat, claim_code_type, expiration_date=None, promo_name=None):
    total_amount = Decimal(price_per_seat) * int(seat_count)
    mail_body = 'Allowed Seats: %s\nMaximum Total Amount: %s\n' % (str(seat_count), str(total_amount))

    if expiration_date:
        mail_body = mail_body + 'Expiration Date: %s\n' % str(expiration_date)
    else:
        mail_body = mail_body + 'Expiration Date: N/A\n'

    if promo_name:
        mail_body = mail_body + 'Promo Name: %s\n' % promo_name.title()
    else:
        mail_body = mail_body + 'Promo Name: N/A\n'

    mail_body = mail_body + "Type: %s\n\n" % claim_code_type.upper()

    if len(promo_codes) > 1:
        mail_body = mail_body + 'Generated %s Claim Codes:\n\n' % str(len(promo_codes))
        mail_body = mail_body + '\n'.join(promo_codes)
    elif len(promo_codes) == 1:
        mail_body = mail_body + 'Generated Claim Code: %s' % promo_codes[0]

    return mail_body

def is_whiteOrBlacklisted(mobnum, mobnum_list):
    if mobnum[0] == '+':
        mobnum = ''.join(['0', mobnum[3:]])
    elif mobnum[0] == '6':
        mobnum = ''.join(['0', mobnum[2:]])
    elif mobnum[0] == '9':
        mobnum = ''.join(['0', mobnum])
    
    if mobnum in mobnum_list:
        return True
    else:
        return False

def claimcode_verification(claim_code, movie_key, theater_key, seat_count, total_amount, mobnum=None, platform=None, is_postpaid=False, screening_time=None, screening_date=None):
    log.debug("claimcode_verification, verifying claim_code: %s..." % claim_code)

    # making sure to return error if claim_code is None.
    if not claim_code:
        log.warn("ERROR!, claimcode_verification, claim_code not found...")

        return 404, 'NOTFOUND', translate_messages('NOTFOUND')

    # making sure to return error if claim_code prefix is GMOVTRY.
    if claim_code.promo_code[:7] == "GMOVTRY":
        log.warn("ERROR!, claimcode_verification, claim_code with prefix GMOVTRY not allowed...")

        return 401, 'INVALID', translate_messages('INVALID')

    if claim_code.is_generic and (mobnum is None or platform is None):
        log.warn("ERROR!, claimcode_verification, generic claim_code needs mobnum/platform for verification...")

        return 401, 'INVALID', translate_messages('INVALID')

    status_code = 200
    message_code = 'SUCCESS' if not claim_code.is_bank_deposit else 'SUCCESSBANKDEPOSIT'
    promo_category = claim_code.key.parent().get() if claim_code.key.parent() else None

    # making sure that the claim code parent (PromoCategory) is valid.
    if promo_category:
        log.debug("claimcode_verification, claim_code has parent (PromoCategory)...")

        start_datetime = None
        expiration_datetime = None

        if promo_category.start_date and promo_category.start_time is not None:
            start_string = '%s %s' % (str(promo_category.start_date), str(promo_category.start_time))
            start_datetime = parse_string_to_datetime(start_string, DATETIME_FORMAT)

        if promo_category.expiration_date and promo_category.expiration_time is not None:
            expiration_string = '%s %s' % (str(promo_category.expiration_date), str(promo_category.expiration_time))
            expiration_datetime = parse_string_to_datetime(expiration_string, DATETIME_FORMAT)

        if start_datetime and convert_timezone(datetime.datetime.now(), 8, '+') < start_datetime:
            log.warn("ERROR!, claimcode_verification, PromoCategory, claim_code is not yet available...")

            return 401, 'NOTYETAVAILABLE', translate_messages('NOTYETAVAILABLE')

        if not promo_category.is_valid:
            log.warn("ERROR!, claimcode_verification, PromoCategory, claim_code is invalid...")

            status_code = 401
            message_code = 'INVALID'
        elif expiration_datetime and convert_timezone(datetime.datetime.now(), 8, '+') > expiration_datetime:
            log.warn("WARN!, claimcode_verification, PromoCategory, claim_code is expired via PromoCategory expired_date...")

            # Check if claim code expiry itself is expired
            if claim_code.expired_date and convert_timezone(datetime.datetime.now(), 8, '+') > claim_code.expired_date:
                log.warn("ERROR!, claimcode_verification, PromoCategory, claim_code is expired via expired_date...")
                status_code = 401
                message_code = 'EXPIRED'

        elif total_amount > promo_category.max_amount or total_amount == 0:
            log.warn("ERROR!, claimcode_verification, PromoCategory, claim_code exceed the max total amount...")

            status_code = 401
            message_code = 'EXCEEDMAXAMOUNT'
        elif promo_category.movies and movie_key not in promo_category.movies:
            log.warn("ERROR!, claimcode_verification, PromoCategory, claim_code is not allowed for this movie...")

            status_code = 401
            message_code = 'MOVIENOTALLOWED'
        elif promo_category.theaters and theater_key not in promo_category.theaters:
            log.warn("ERROR!, claimcode_verification, PromoCategory, claim_code is not allowed for this theater...")

            status_code = 401
            message_code = 'THEATERNOTALLOWED'

    # making sure that the specific claim code is valid even with or without parent (PromoCategory).
    if claim_code.is_invalid:
        log.warn("ERROR!, claimcode_verification, claim_code is invalid...")

        status_code = 401
        message_code = 'INVALID'
    elif claim_code.is_redeemed or claim_code.redeemed_on:
        log.warn("ERROR!, claimcode_verification, claim_code is redeemed...")

        status_code = 401
        message_code = 'REDEEMED'
    elif claim_code.is_expired:
        log.warn("ERROR!, claimcode_verification, claim_code is expired via is_expired...")

        status_code = 401
        message_code = 'EXPIRED'
    elif claim_code.expired_date and convert_timezone(datetime.datetime.now(), 8, '+') > claim_code.expired_date:
        log.warn("ERROR!, claimcode_verification, claim_code is expired via expired_date...")

        status_code = 401
        message_code = 'EXPIRED'
    #checking for screening date and time
    elif claim_code.screening_date is not None and screening_date is not None:
        log.warn("WARN!, claimcode_verification, PromoCode, claim_code has screening date and time check...")

        date_str = screening_date
        date_screen = datetime.datetime.strptime('{} 00:00:00'.format(date_str), '%d %b %Y %H:%M:%S')
        date_screen = convert_timezone(date_screen, 8, '+') # convert to UTC.

        #Convert time if screening_time has AM/PM format
        if 'AM' in screening_time or 'PM' in screening_time:
            pm_am = datetime.datetime.strptime(screening_time, "%I:%M %p")
            screening_time = datetime.datetime.strftime(pm_am, "%H:%M")

        screening_string = '%s %s' % (date_screen.date(), '{}:00'.format(screening_time))
        screening_datetime = parse_string_to_datetime(screening_string, DATETIME_FORMAT)

        # Check if screening date is same in claim code
        log.warn('request screening Time : {}'.format(screening_datetime.time()))
        log.warn('request screening Date : {}'.format(screening_datetime.date()))
        log.warn('db screening time : {}'.format(claim_code.screening_time))
        log.warn('db screening Date : {}'.format(claim_code.screening_date))
        if claim_code.screening_date:
            if claim_code.screening_date != screening_datetime.date():
                log.warn("WARN!, claimcode_verification, PromoCode, claim_code is invalid via PromoCode screening date...")
                log.warn('claim_code.screening_date: {} and date_screen: {}'.format(claim_code.screening_date, screening_datetime.date()))
                status_code = 401
                message_code = 'INVALID'
        # Check if screening date is same in claim code
        if claim_code.screening_time:
            if claim_code.screening_time != screening_datetime.time():
                log.warn("WARN!, claimcode_verification, PromoCode, claim_code is invalid via PromoCode screening time...")
                log.warn('claim_code.screening_time: {} and screening_time: {}'.format(str(claim_code.screening_time), screening_datetime.time()))
                status_code = 401
                message_code = 'INVALID'
                
        if screening_datetime.time() == claim_code.screening_time:
            log.warn('request screening Time:{} match to db screening time:{}'.format(screening_datetime.time(), claim_code.screening_time))
        if screening_datetime.date() == claim_code.screening_date:
            log.warn('request screening Date:{} match to db screening date:{}'.format(screening_datetime.date(), claim_code.screening_date))
                
    #checking for max seat and min seat
    elif seat_count > claim_code.seat_count:
        log.warn("ERROR!, claimcode_verification, PromoCategory or PromoCode, claim_code exceed the max seats...")

        status_code = 401
        message_code = 'EXCEEDMAXSEATS'

    elif seat_count < claim_code.min_seats:
        log.warn("ERROR!, claimcode_verification, PromoCategory or PromoCode, claim_code min seats...")

        status_code = 401
        message_code = 'LOWMINSEATS'

    # White and Blacklisting verification
    if claim_code.whitelist:
        if not is_whiteOrBlacklisted(mobnum, claim_code.whitelist):
            log.warn("ERROR!, claimcode_verification, PromoCategory, mobile number is not whitelisted...")

            status_code = 401
            message_code = 'INVALID'

    if claim_code.blacklist:
        if is_whiteOrBlacklisted(mobnum, claim_code.blacklist):
            log.warn("ERROR!, claimcode_verification, PromoCategory, mobile number is blacklisted...")

            status_code = 401
            message_code = 'INVALID'

    # Addtl verification for Generic Claim Codes
    if claim_code.is_generic:
        # Check if platform can redeem Generic Claim Code
        if platform not in GENERIC_CLAIMCODE_ALLOWED_PLATFORMS:
            log.warn("ERROR!, claimcode_verification, platform %s not applicable for generic claim_code..." % platform)

            status_code = 401
            message_code = 'PLATFORMNOTALLOWED'

        # Check if mobnum is postpaid
        log.debug("claimcode_verification, is_postpaid: %s , type(is_postpaid): %s" % (is_postpaid, type(is_postpaid)))
        if not is_postpaid:
            log.warn("ERROR!, claimcode_verification, non postpaid number is not allowed for generic claim_code...")

            status_code = 401
            message_code = 'MOBNUMBRANDNOTALLOWED'

        # Check if mobnum already redeemed generic claim code
        mobnum_key = ndb.Key(Mobnum, mobnum)
        generic_transaction = GenericClaimCodeTransactions.query(GenericClaimCodeTransactions.promo_code==claim_code.promo_code, GenericClaimCodeTransactions.is_invalid==False, ancestor=mobnum_key).get()

        if generic_transaction:
            log.warn("ERROR!, claimcode_verification, generic claim_code is redeemed by mobnum...")

            status_code = 401
            message_code = 'REDEEMED'

        # Check if max number of redemptions for generic claim code reached
        # generic_transactions_queryset = GenericClaimCodeTransactions.query(GenericClaimCodeTransactions.promo_code==claim_code.promo_code, GenericClaimCodeTransactions.is_invalid==False)

        if claim_code.redemption_count >= claim_code.redeem_count:
            log.warn("ERROR!, claimcode_verification, generic claim_code max redemptions reached...")

            status_code = 401
            message_code = 'EXCEEDEDMAXREDEMPTIONS'

    # verify if the claim code is bank_payment.
    if claim_code.is_bank_deposit:
        log.debug("claimcode_verification, claim_code is generated via bank_deposit...")

        if seat_count > 10:
            log.warn("ERROR!, claimcode_verification, claim_code exceed the max seats...")

            status_code = 401
            message_code = 'EXCEEDMAXSEATS'
        elif total_amount > claim_code.purchase_amount or total_amount == 0:
            log.warn("ERROR!, claimcode_verification, claim_code exceed the max total amount...")

            status_code = 401
            message_code = 'EXCEEDMAXAMOUNT'
    else:
        log.debug("claimcode_verification, regular claim_code...")

        max_seat_count = DEFAULT_SEAT_COUNT
        max_total_amount = Decimal(DEFAULT_PRICE_PER_SEAT) * DEFAULT_SEAT_COUNT

        if claim_code.get_total_amount():
            max_seat_count = claim_code.seat_count
            max_total_amount = claim_code.get_total_amount()

        if seat_count > max_seat_count:
            log.warn("ERROR!, claimcode_verification, claim_code exceed the max seats...")

            status_code = 401
            message_code = 'EXCEEDMAXSEATS'
        elif total_amount > max_total_amount or total_amount == 0:
            log.warn("ERROR!, claimcode_verification, claim_code exceed the max total amount...")

            status_code = 401
            message_code = 'EXCEEDMAXAMOUNT'

    return status_code, message_code, translate_messages(message_code)

def parse_expiration_date(expired_date, expired_time):
    try:
        expiration_date_str = expired_date + " " + expired_time
        expiration_date = datetime.datetime.strptime(expiration_date_str, '%m/%d/%Y %H:%M')
    except ValueError:
        raise BadRequestError("Error in expiration date/time format.", 400)

    return expiration_date

def verify_amount(total_amount, theaterorg_id, mobnum, seat_count, platform):

    if total_amount == 0:
        log.debug("MUST THROW AND ERROR: total amount is %s" % (total_amount))

    else:
        log.debug("old_total_amount: %s" % (total_amount))
        log.debug("seat_count: %s" % (seat_count))
        log.debug("org_id: %s" % (theaterorg_id))

        cf = float(get_convenience_fee(theaterorg_id)) * int(seat_count)

        log.debug("c_fee: %s" % (cf))

        total_amount = float(total_amount) + cf

        log.debug("new_total_amount: %s" % (total_amount))

    return total_amount

def get_convenience_fee(org_id):
    endpoint = 'https://globe-gmovies-staging.appspot.com/api/3/convenience_fee/%s' % (org_id)
    log.debug('getting_cf: %s' % (endpoint))

    cf = requests.get(endpoint, headers={'X-GMovies-DeviceId': '1111'})
    data = json.loads(cf.text)

    log.debug('response: %s' % (data))

    if str(data['status']) == 'success':
        log.debug('cf_use_amount: %s' % (data['data']['credit_card']))
        return data['data']['credit_card']
    
    else:
        return data['status']


class CheckCodeHandler(webapp2.RequestHandler):
    def get(self):
        self.response.headers["Content-Type"] = "text/plain"

        try:
            claim_code = self.request.get("promo_code")
            claim_code = PromoCode.query(PromoCode.promo_code==claim_code).get()

            if not claim_code:
                log.warn("ERROR!, CheckCodeHandler, claim_code not found...")
                raise BadRequestError("Invalid code.", 400)

            # Check if Claim Code is valid for specific Theater
            is_theater_valid = True
            theaterorg_id = self.request.get("theaterorg_id", None)
            theater_id = self.request.get("theater_id", None)
            if theaterorg_id and theater_id:
                theater_key = ndb.Key(TheaterOrganization, theaterorg_id, Theater, int(theater_id))
                promo_category = claim_code.key.parent().get() if claim_code.key.parent() else None
                if promo_category and promo_category.theaters and theater_key not in promo_category.theaters:
                    is_theater_valid = False

            # For now only returns 'is_bank_deposit'. Needed for checking of convenience_fee
            response_object = {"return": {"code": 200, "message": "Successfully retrieved claim code.",
                    "claim_code_details": {
                        "is_bank_deposit": claim_code.is_bank_deposit,
                        "is_theater_valid": is_theater_valid,
                        "is_generic": claim_code.is_generic if hasattr(claim_code, 'is_generic') else False
                        }
                    }}
            self.response.out.write(json.dumps(response_object))

        except BadRequestError, instance:
            log.warn("ERROR!, CheckCodeHandler, GET, BadRequestError...")
            log.error("status_code: %s, message: %s..." % (instance.code, instance.msg))

            self.error(instance.code)
            self.response.out.write(instance.msg)
        except Exception, e:
            log.warn("ERROR!, CheckCodeHandler, GET...")
            log.error(e)

            self.error(500)
            self.response.out.write(DEFAULT_MESSAGE)

app = webapp2.WSGIApplication([('/redeem', RedeemHandler), ('/reset', ResetHandler), ('/verify', VerifyHandler),
        ('/expire', ExpireHandler), ('/get_code', GetCodeHandler), ('/generate_promo_code', GeneratePromoCodeHandler),
        ('/bank_deposit/generate_promo_code', BankDepositHandler), ('/promos/generate', PromoCategoryHandler),
        ('/check_code', CheckCodeHandler), ('/subsidize-details/list', ClaimCodeSubsidizeListHandler), ('/subsidize-details', ClaimCodeSubsidizeDetailsHandler)], debug=True)