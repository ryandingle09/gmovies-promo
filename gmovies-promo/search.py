import datetime
import logging
log = logging.getLogger(__name__)

import jinja2
import json
import os
import re
import urllib
import webapp2

from google.appengine.api import mail
from google.appengine.ext import ndb

from models import PromoCode, asia_manila_timezone


JINJA_ENVIRONMENT = jinja2.Environment(
            loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
            extensions=['jinja2.ext.autoescape'], autoescape=True)


class BadRequestError(Exception):
    def __init__(self, param, code):
        super(Exception, self).__init__()
        self.msg = param
        self.code = code


class SearchHandler(webapp2.RequestHandler):
    def get(self):
        promo_code = self.request.get("promo_code")
        promo_code_filter = self.request.get("search_by")
        cursor = ndb.Cursor(urlsafe=self.request.get('cursor'))

        try:
            if promo_code_filter == "promo_code":
                log.info('Fetching with Promo Code...')
                if promo_code:
                    query_set = PromoCode.query(ndb.AND(PromoCode.promo_code >= promo_code.upper(),
                                    PromoCode.promo_code < unicode(promo_code.upper()) + u'\ufffd')).order(PromoCode.promo_code, PromoCode.key)
                else:
                    query_set = PromoCode.query().order(-PromoCode.redeemed_on, PromoCode.key)
            else:
                if promo_code_filter == "available":
                    log.info('Fetching Available Promo Codes...')
                    query_set = PromoCode.query(ndb.AND(PromoCode.redeemed_on == None, PromoCode.is_expired == False,
                            ndb.OR(PromoCode.expired_date > asia_manila_timezone(datetime.datetime.now()),
                                    PromoCode.expired_date <= None))).order(-PromoCode.expired_date, PromoCode.key)
                elif promo_code_filter == "redeemed":
                    log.info('Fetching Redeemed Promo Codes...')
                    query_set = PromoCode.query(PromoCode.redeemed_on > None).order(-PromoCode.redeemed_on, PromoCode.key)
                elif promo_code_filter == "expired":
                    log.info('Fetching Expired Promo Codes...')
                    query_set = PromoCode.query(ndb.OR(PromoCode.is_expired == True,
                            ndb.AND(PromoCode.expired_date <= asia_manila_timezone(datetime.datetime.now()),
                                    PromoCode.expired_date > None, PromoCode.redeemed_on == None))).order(-PromoCode.expired_date, PromoCode.key)
                else:
                    log.info('Fetching All Promo Codes...')
                    query_set = PromoCode.query().order(-PromoCode.redeemed_on, PromoCode.key)

            query_set_page, next_cursor, more = query_set.fetch_page(50, start_cursor=cursor)

            template_values = {'query_set_page': query_set_page, 'next_cursor': next_cursor,
                        'more': more, 'promo_code': promo_code,
                        'promo_code_filter': promo_code_filter}
            template = JINJA_ENVIRONMENT.get_template('templates/promo_codes.html')
            self.response.write(template.render(template_values))
        except BadRequestError, (instance):
            response_object = {'return': {'code': instance.code, 'message': instance.msg}}
            self.response.out.write(json.dumps(response_object))

class SearchClaimCode(webapp2.RequestHandler):
    def get(self):
        code = self.request.get("claim_code")
        code_details = {}
        try:
            details = PromoCode.query(PromoCode.promo_code==code).fetch()
            for data in details:
                code_details = {
                    'claim_code' : data.promo_code,
                    'min_seats' : data.min_seats,
                    'max_seats' : data.seat_count,
                    'price_per_seat' : str(data.price_per_seat),
                    'theaterorg_id' : data.theaterorg_id,
                    'screening_date' : str(data.screening_date),
                    'screening_time' : str(data.screening_time)
                }

        except Exception, e:
            log.warn("ERROR! GetClaimCodeDetails, GET...")
            log.error(e)

        return self.response.out.write(json.dumps(code_details))

app = webapp2.WSGIApplication([('/admin/0/search_promo_codes', SearchHandler),('/api/search/claim_code', SearchClaimCode)],
                        debug=True)
