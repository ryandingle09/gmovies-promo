$(document).ready(function() {
    var transaction_locked = false;

    $.ajax({
        url: "/promos/generate",
        type: "GET",
        data: {action: "FILTER_MPASS_WALLET"},
        success: function(data) {
            var response = jQuery.parseJSON(data);
            var wallets = response.return.results.wallets;
            var filter_mpass_wallet = $('#filter_mpass_wallet').empty();

            filter_mpass_wallet.append($('<option value="">Default </option>'));
            $.each(wallets, function(index, wallet) {
                filter_mpass_wallet.append($('<option value="' + wallet['id'] + '">'+ wallet['name'] +'</option>'));
            });

            $("#filter_mpass_wallet").selectmenu('refresh', true);
        },
        error: function(data) {
            var response = jQuery.parseJSON(data.responseText);

            switch (response.return.code) {
                case 404:
                    showError(response.return.message, "Not Found!");

                    return;
                default:
                    showError(response.return.message, "Error!");

                    return;
            }
        }
    });

    $.ajax({
        url: "/promos/generate",
        type: "GET",
        data: {action: "FILTER_MOVIES"},
        success: function(data) {
            var response = jQuery.parseJSON(data);
            var movies = response.return.results.movies;
            var filter_movies = $('#filter_movies').empty();

            $.each(movies, function(index, movie) {
                filter_movies.append($('<option value="' + movie['id'] + '">'+ movie['name'] +'</option>'));
            });

            $("#filter_movies").selectmenu('refresh', true);
        },
        error: function(data) {
            var response = jQuery.parseJSON(data.responseText);

            switch (response.return.code) {
                case 404:
                    showError(response.return.message, "Not Found!");

                    return;
                default:
                    showError(response.return.message, "Error!");

                    return;
            }
        }
    });

    $.ajax({
        url: "/promos/generate",
        type: "GET",
        data: {action: "FILTER_THEATERS"},
        success: function(data) {
            var response = jQuery.parseJSON(data);
            var theaters = response.return.results.theaters;
            var filter_theaters = $('#filter_theaters').empty();

            $.each(theaters, function(index, theater) {
                filter_theaters.append($('<option value="' + theater['id'] + '">'+ theater['name'] +'</option>'));
            });

            $("#filter_theaters").selectmenu('refresh', true);
        },
        error: function(data) {
            var response = jQuery.parseJSON(data.responseText);

            switch (response.return.code) {
                case 404:
                    showError(response.return.message, "Not Found!");

                    return;
                default:
                    showError(response.return.message, "Error!");

                    return;
            }
        }
    });

    $("#create_promo_btn").click(function() {
        var code_type = $("#code_type").val();
        var name = $("#name").val();
        var prefix = $("#prefix").val();
        var redeem_count = $("#redeem_count").val();
        var claimcode_quantity = $("#claimcode_quantity").val();
        var validity_type = $("#validity_type").val();
        var max_seats = $("#max_seats").val();
        var min_seats = $("#min_seats").val();
        var seat_price = $("#seat_price").val();
        var screening_date = $("#screening_date").val();
        var screening_time = $("#screening_time").val();
        var start_date = $("#start_date").val();
        var start_time = $("#start_time").val();
        var expiration_date = $("#expiration_date").val();
        var expiration_time = $("#expiration_time").val();
        var whitelist = $("#whitelist").val();
        var blacklist = $("#blacklist").val();
        var filter_movies = $("#filter_movies").val();
        var filter_theaters = $("#filter_theaters").val();
        var globe_subsidize_amount = $("#globe_subsidize_amount").val();
        var thirdparty_company_name = $("#thirdparty_company_name").val();
        var thirdparty_subsidize_amount = $("#thirdparty_subsidize_amount").val();
        var email_to = $("#email_to").val();
        var email_retry = $("#email_retry").val();
        var is_valid = true
        var filter_mpass_wallet = $("#filter_mpass_wallet").val();

        var seat_price_pattern = /^[0-9]+.[0-9]{2}$/;
        var subsidize_amount_pattern = /^[0-9]+.[0-9]{2}$/;
        var prefix_pattern = /^[A-Z0-9]{1,14}$/;
        var generic_pattern = /^[;*A-Z0-9]{6,}$/;
        var claimcode_quantity_pattern = /^[0-9]+$/;
        var redeem_count_pattern = /^[0-9]+$/;

        var generic_codes = '';
        if (code_type == 'GENERIC') {
            generic_codes = $("#generic_codes").val();
        }

        if (name == "" || name === null || name === undefined) {
            showError("Please input Promo Name and try again.", "Promo Name Required!");

            return;
        }

        if (prefix != "" && prefix != null && prefix != undefined){
            if (prefix_pattern.test(prefix) == false) {
                showError("Prefix invalid format. Please try again.", "Check Prefix!");

                return;
            }
        }

        if (code_type == "GENERIC" && generic_codes != "" && generic_codes != null && generic_codes != undefined){
            if (generic_pattern.test(generic_codes) == false) {
                showError("Generic Code invalid format. Please try again.", "Check Generic Code!");

                return;
            }
        }

        if (code_type == "GENERIC" && redeem_count != "" && redeem_count != null && redeem_count != undefined){
            if (redeem_count_pattern.test(redeem_count) == false) {
                showError("Max Uses invalid format. Please try again.", "Check Max number of Uses!");

                return;
            }
        }

        if (claimcode_quantity == "" || claimcode_quantity === null || claimcode_quantity === undefined) {
            showError("Please input Claim Code Quantity and try again.", "Claim Code Quantity Required!");

            return;
        } else {
            if (claimcode_quantity_pattern.test(claimcode_quantity) == false) {
                showError("Claim Code Quantity invalid format. Please try again.", "Check Claim Code Quantity!");

                return;
            }
        }

        if (validity_type == "" || validity_type === null || validity_type === undefined) {
            showError("Please input Validity Type and try again.", "Validity Type Required!");

            return;
        }

        if (max_seats == "" || max_seats === null || max_seats === undefined) {
            showError("Please input Max Seats and try again.", "Max Seats Required!");

            return;
        }

        if (seat_price == "" || seat_price === null || seat_price === undefined) {
            showError("Please input Seat Price and try again.", "Seat Price Required!");

            return;
        } else {
            if (seat_price_pattern.test(seat_price) == false) {
                showError("Seat Price invalid format. Please try again.", "Check Seat Price!");

                return;
            }
        }

        if (globe_subsidize_amount != "" && globe_subsidize_amount != null && globe_subsidize_amount != undefined) {
            if (subsidize_amount_pattern.test(globe_subsidize_amount) == false) {
                showError("Globe Subsidize invalid format. Please try again.", "Check Globe Subsidize!");

                return;
            }
        } else {
            globe_subsidize_amount = '';
        }

        if (thirdparty_subsidize_amount != "" && thirdparty_subsidize_amount != null && thirdparty_subsidize_amount != undefined) {
            if (subsidize_amount_pattern.test(thirdparty_subsidize_amount) == false) {
                showError("Third Party Subsidize invalid format. Please try again.", "Check Third Party Subsidize!");

                return;
            } else if (thirdparty_company_name == "" || thirdparty_company_name == null || thirdparty_company_name == undefined) {
                showError("Please input Third Party Company Name and try again.", "Check Third Party Company Name!");

                return;
            }
        } else {
            thirdparty_subsidize_amount = '';
        }

        if (thirdparty_company_name != "" && thirdparty_company_name != null && thirdparty_company_name != undefined) {
            if (thirdparty_subsidize_amount == "" || thirdparty_subsidize_amount == null || thirdparty_subsidize_amount == undefined) {
                showError("Please input Third Party Subsidize and try again.", "Check Third Party Subsidize!");

                return;
            }
        }

        if (email_to == "" || email_to === null || email_to === undefined) {
            showError("Please input Email and try again.", "Email Required!");

            return;
        }

        if (email_to != email_retry) {
            showError("Email Address don't match. Please try again.", "Check Email Address!");

            return;
        }

        if (validity_type === 'invalid') {
            is_valid = false;
        }

        if (filter_movies === null || filter_movies === undefined) {
            filter_movies = [];
        }

        if (filter_theaters === null || filter_theaters === undefined) {
            filter_theaters = [];
        }

        if (transaction_locked) {
            return;
        } else {
            transaction_locked = true;
        }

        $.mobile.showPageLoadingMsg("Processing...");

        var payload = {
            action: "CREATE_PROMO",
            code_type: code_type,
            name: name,
            prefix: prefix,
            generic_codes: generic_codes,
            redeem_count: redeem_count,
            claimcode_quantity: claimcode_quantity,
            is_valid: is_valid,
            mpass_wallet: filter_mpass_wallet,
            min_seats: min_seats,
            max_seats: max_seats,
            seat_price: seat_price,
            screening_date: screening_date,
            screening_time: screening_time,
            start_date: start_date,
            start_time: start_time,
            expiration_date: expiration_date,
            expiration_time: expiration_time,
            whitelist: whitelist,
            blacklist: blacklist,
            movie_ids: filter_movies,
            theater_ids: filter_theaters,
            email_to: email_to,
            globe_subsidize_amount: globe_subsidize_amount,
            thirdparty_company_name: thirdparty_company_name,
            thirdparty_subsidize_amount: thirdparty_subsidize_amount
        };

        $.ajax({
            url: "/promos/generate",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify(payload),
            success: function(data) {
                var response = data;
                transaction_locked = false;

                $.mobile.hidePageLoadingMsg();

                window.location.href = '/admin/0/gmovies-promos/' + response.return.id;
            },
            error: function(data) {
                var response = jQuery.parseJSON(data.responseText);
                transaction_locked = false;

                $.mobile.hidePageLoadingMsg();

                switch (response.return.code) {
                    case 400:
                        showError(response.return.message, "Invalid Input!");

                        return;
                    case 401:
                        showError(response.return.message, "Invalid Input!");

                        return;
                    case 404:
                        showError(response.return.message, "Not Found!");

                        return;
                    default:
                        showError(response.return.message, "Error!");

                        return;
                }
            }
        });
    });

    function showError(input_text, header) {
        header = (typeof header === "undefined") ? "Error" : header;
        $("#error-message").text(input_text);
        $("#error-header").text(header);
        $.mobile.changePage('#error', {transition: 'pop'});
    };
});