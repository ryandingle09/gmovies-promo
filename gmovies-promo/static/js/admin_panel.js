$(document).ready(function() {
    var filter = $("#id_status").val();
    var today = new Date();

    $("#id_status").change(function() {
        var filter = $("#id_status").val();
        
        if(filter == "search_by") {
            $("#id_promo_code").prop('disabled', false);
            $("#id_promo_code").show();
        } else {
            $("#id_promo_code").prop('disabled', true);
            $("#id_promo_code").hide();
        }
    });

    $("#from").addClass("datepicker").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        showOtherMonths: true,
        selectOtherMonths: true,
        maxDate: today,
        onSelect: function(selectedDate) {
            var option = $(this).id == "to" ? "minDate": "minDate",
            instance = $(this).data("datepicker"),
            date = $.datepicker.parseDate(
                instance.settings.dateFormat ||
                $.datepicker._defaults.dateFormat,
                selectedDate, instance.settings);
            to.not(this).datepicker("option", option, date);
        }
    });

    var to = $("#to").addClass("datepicker").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        showOtherMonths: true,
        selectOtherMonths: true,
        maxDate: today,
        minDate: today
    });

    $("#id_compute_btn").click(function() {
        var dict = $.parseJSON($(this).attr("data-button"));
        var theaterorg = dict.theaterorg;
        var theater = dict.theater;
        var date_from = dict.date_from;
        var date_to = dict.date_to;

        $.isLoading({ text: "Loading..." });

        $.ajax({
            url: "/admin/1/promo_codes/redeemed/" + theaterorg + "/compute",
            type: 'GET',
            data: {theater: theater, from: date_from, to: date_to},
            success: function(data) {
                var response = jQuery.parseJSON(data);
                if ((date_from != "" && date_from != null && date_from != undefined) && (date_to != "" && date_to != null && date_to != undefined)) {
                    date_range = "<h3>" + date_from + "" + " to " + "" + date_to + "</h3>"
                } else {
                    date_range = "<h3>All</h3>"
                }

                switch (response.return.code) {
                    case 200:
                        $.isLoading("hide");
                        bootbox.alert(date_range + "<br>" +
                                      "<strong>Total Booked Seats:</strong> " + response.return.total_booked_seats + " seats" +
                                      "<br><br>" +
                                      "<strong>Total Booked Amount:</strong> Php " + response.return.total_booked_amount);
                        return;
                    default:
                        $.isLoading("hide");
                        bootbox.alert("ERROR!, " + response.return.message);
                        return;
                }
            }
        });
    });

    $("#id_filter_btn").click(function() {
        var form = $("#id_filter_form");

        $("#action").val("filter");
        form.submit();
    });

    $("#id_export_btn").click(function() {
        var form = $("#id_filter_form");

        $("#action").val("export");
        form.submit();
    });
});