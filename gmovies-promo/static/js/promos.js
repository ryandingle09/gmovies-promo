$(document).ready(function() {
  $("#id_redeem_promo_btn").live("click", function(event) {
    event.preventDefault();
    var promo_code = $(this).attr("data-button");
    bootbox.confirm("Redeem promo code " + promo_code + "?", function(result) {
      if (result) {
        var request = $.ajax({
          url: "/admin/0/promos/redeem",
          type: 'GET',
          data: {promo_code: promo_code},
          success: function(res) {
            bootbox.alert("Successfully redeemed promo code " + promo_code, function() {
                location.reload();
            });
            return;
          },
          error: function(err) {
            switch (err.status) {
              case 400:
                bootbox.alert("Invalid Input: Error in parameters.");
                return;
              case 401:
                bootbox.alert("Promo code " + promo_code + " has already been redeemed/expired.");
                return;
              case 403:
                bootbox.alert("Promo code " + promo_code + " is not entitled to win free movie tickets.");
                return;
              case 404:
                bootbox.alert("Promo code " + promo_code + " seems to be invalid. Please check it and try again.");
                return;
              default:
                bootbox.alert("Error!");
                return;
            }
          }
        });
      }
    });
  });

  $("#id_reset_promo_btn").live("click", function(event) {
    event.preventDefault();
    var promo_code = $(this).attr("data-button");

    bootbox.confirm("Reset promo code " + promo_code + "?", function(result) {
      if (result) {
        var request = $.ajax({
          url: "/admin/0/promos/reset",
          type: 'GET',
          data: {promo_code: promo_code},
          success: function(res) {
            bootbox.alert("Successfully reset promo code " + promo_code, function() {
                location.reload();
            });
            return;
          },
          error: function(err) {
            switch (err.status) {
              case 400:
                bootbox.alert("Invalid Input: Error in parameters.");
                return;
              case 401:
                bootbox.alert("Pending redemption Promo code " + promo_code + ".");
                return;
              case 403:
                bootbox.alert("Unable to reset this promo code " + promo_code + ", because the expiration date is less than the current date.");
                return;
              default:
                bootbox.alert("Error!");
                return;
            }
          }
        });
      }
    });
  });

$("#id_expire_promo_btn").live("click", function(event) {
    event.preventDefault();
    var promo_code = $(this).attr("data-button");

    bootbox.confirm("Expire promo code " + promo_code + "?", function(result) {
      if (result) {
        var request = $.ajax({
          url: "/admin/0/promos/expire",
          type: 'GET',
          data: {promo_code: promo_code},
          success: function(res) {
            bootbox.alert("Successfully expired promo code " + promo_code, function() {
                location.reload();
            });
            return;
          },
          error: function(err) {
            switch (err.status) {
              case 400:
                bootbox.alert("Invalid Input: Error in parameters.");
                return;
              case 401:
                bootbox.alert("Promo code " + promo_code + " has already been redeemed/expired.");
                return;
              default:
                bootbox.alert("Error!");
                return;
            }
          }
        });
      }
    });
  });

  function showError(input_text, header) {
    $("#error-message").text(input_text);
    header = (typeof header === "undefined") ? "Error" : header;
    $("#error-header").text(header);
    $.mobile.changePage('#error', {transition: 'pop'});
  };
});
