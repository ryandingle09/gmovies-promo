$(document).ready(function() {
    var transaction_locked = false;

    $("#id_generate_btn").click(function() {
        var purchase_amount = $("#purchase_amount").val();
        var expired_date = $("#expired_date").val();
        var expired_time = $("#expired_time").val();
        var email = $("#email").val();
        var email_retry = $("#email_retry").val();

        var purchase_amount_pattern = /^[0-9]+.[0-9]{2}$/; // purchase_amount must be valid -- numeric characters

        if (purchase_amount != "" && purchase_amount != null && purchase_amount != undefined) {
            if (purchase_amount_pattern.test(purchase_amount) == false) {
                showError("Please check the Amount. Invalid format.", "Check Amount");
                return;
            }
        }

        if (purchase_amount == "" || purchase_amount == null || purchase_amount == undefined) {
            showError("Amount is Required.", "Check Amount");
            return;
        }

        if (email != email_retry || email == "" || email === undefined) {
            showError("Check the given email address. Email addresses don't match or are empty", "Check Email");
            return;
        }

        // lock first
        if (transaction_locked) {
            return;
        } else {
            transaction_locked = true;
        }

        $.mobile.showPageLoadingMsg("Processing...");
        $.ajax({
            url: "/bank_deposit/generate_promo_code",
            type: 'GET',
            data: {email: email, expired_date: expired_date, expired_time: expired_time, purchase_amount: purchase_amount},
            success: function(data) {
                var response = jQuery.parseJSON(data);
                transaction_locked = false;

                $.mobile.hidePageLoadingMsg();

                switch (response.return.code) {
                    case 400: // wrong formatting or wrong parameters
                        showError(response.return.message, "Invalid Input");
                        return;
                    case 200:
                        $("#promo-code-value").text(response.return.promo_code);
                        $.mobile.changePage($("#success"));
                        break;
                    default:
                        showError(response.return.message, "Error");
                        return;
                }
            }
        });
    });

    function showError(input_text, header) {
        $("#error-message").text(input_text);
        header = (typeof header === "undefined") ? "Error" : header;
        $("#error-header").text(header);
        $.mobile.changePage('#error', {transition: 'pop'});
    };
});