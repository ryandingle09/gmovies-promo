$(document).ready(function() {
    var filter_by = $("#id_filter_by").val();

    if(filter_by != "promo_code" && filter_by != "promo_name" && filter_by != "batch_code") {
        $("#id_filter_value").prop('disabled', true);
        $("#id_filter_value").hide();
    }

    $("#id_filter_by").change(function() {
        filter_by = $("#id_filter_by").val();

        $("#id_filter_value").val("");

        if(filter_by == "promo_code" || filter_by == "promo_name" || filter_by == "batch_code") {
            $("#id_filter_value").prop('disabled', false);
            $("#id_filter_value").show();
        } else {
            $("#id_filter_value").prop('disabled', true);
            $("#id_filter_value").hide();
        }
  });

    $(document).on("click", "button[name='reset_btn']", function(e) {
        e.preventDefault();

        var claim_code = $(this).attr("data-button");

        bootbox.confirm("Do you want to reset this claim  code " + claim_code + "?", function(result) {
            if (result) {
                $.ajax({
                    url: "/promos/generate",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({action: "RESET_CLAIMCODE", claim_code: claim_code}),
                    success: function(data) {
                        bootbox.alert("Successfully reset claim code " + claim_code + "!", function() {
                            location.reload();
                        });

                        return;
                    },
                    error: function(data) {
                        var response = jQuery.parseJSON(data.responseText);

                        bootbox.alert(response.return.message);

                        return;
                    }
                });
            }
        });        
    });

    $(document).on("click", "button[name='expire_btn']", function(e) {
        e.preventDefault();

        var claim_code = $(this).attr("data-button");

        bootbox.confirm("Do you want to expire this claim  code " + claim_code + "?", function(result) {
            if (result) {
                $.ajax({
                    url: "/promos/generate",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({action: "EXPIRE_CLAIMCODE", claim_code: claim_code}),
                    success: function(data) {
                        bootbox.alert("Successfully expired claim code " + claim_code + "!", function() {
                            location.reload();
                        });

                        return;
                    },
                    error: function(data) {
                        var response = jQuery.parseJSON(data.responseText);

                        bootbox.alert(response.return.message);

                        return;
                    }
                });
            }
        });
    });

    $("#id_count_btn").click(function() {
        var dict = $.parseJSON($(this).attr("data-button"));
        var filter_by = dict.filter_by;

        if (filter_by == '' || filter_by == null || filter_by == undefined) {
            filter_by = "all"
        }

        $.isLoading({text: "Loading..."});

        $.ajax({
            url: "/admin/0/promo_codes/count",
            type: 'GET',
            data: {filter_by: dict.filter_by, filter_value: dict.filter_value},
            success: function(data) {
                var response = jQuery.parseJSON(data);
                var filter_by_label = "All";

                if (filter_by == 'promo_code') {
                    filter_by_label = 'Promo Code';
                } else if (filter_by == 'promo_name') {
                    filter_by_label = 'Promo Name'
                } else if (filter_by == 'batch_code') {
                    filter_by_label = 'Batch Code'
                }

                $.isLoading("hide");
                bootbox.alert("<strong>Filter:</strong> " + filter_by_label + "<br><br>" +
                        "<strong>Available:</strong> " + response.return.count_available + "<br>" +
                        "<strong>Redeemed:</strong> " + response.return.count_redeemed + "<br>" +
                        "<strong>Expired:</strong> " + response.return.count_expired + "<br>" +
                        "<strong>Invalid:</strong> " + response.return.count_invalid + "<br><br>" +
                        "<strong>Total:</strong> " + response.return.total_count);

                return;
            },
            error: function(data) {
                var response = jQuery.parseJSON(data.responseText);

                switch (response.return.code) {
                    default:
                        $.isLoading("hide");
                        bootbox.alert("ERROR!, " + response.return.message);

                        return;
                }
            }
        });
    });
});