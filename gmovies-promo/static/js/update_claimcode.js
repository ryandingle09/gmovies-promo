$(document).ready(function() {
    var parent_id = '';
    var transaction_locked = false;
    var claimcode_string = window.location.href.slice(window.location.href.indexOf('?') + 1).split('=')[1];

    $.mobile.showPageLoadingMsg("Processing...");

    $.ajax({
        url: "/promos/generate",
        type: "GET",
        data: {action: "UPDATE_CLAIMCODE", claim_code: claimcode_string},
        success: function(data) {
            var validity_type = 'valid';
            var response = jQuery.parseJSON(data);
            var claim_code = response.return.results;
            parent_id = claim_code['parent_id'];

            if (claim_code['is_valid'] === false) {
                validity_type = 'invalid';
            }

            var min_seats = (claim_code['min_seats'] !== null) ? claim_code['min_seats'].toString() : '';

            $('#name').val(claim_code['promo_name']);
            $('#claim_code').val(claim_code['claim_code']);
            $('#redeem_count').val(claim_code['redeem_count']);
            $('#validity_type').val(validity_type);
            $('#max_seats').val(claim_code['max_seats'].toString());
            $('#min_seats').val(min_seats);
            $('#seat_price').val(claim_code['seat_price']);
            $('#screening_date').val(claim_code['screening_date']);
            $('#screening_time').val(claim_code['screening_time']);
            $('#expiration_date').val(claim_code['expiration_date']);
            $('#expiration_time').val(claim_code['expiration_time']);
            $('#whitelist').val(claim_code['whitelist']);
            $('#blacklist').val(claim_code['blacklist']);
            $('#email_to').val(claim_code['email_to']);

            $("#validity_type").selectmenu('refresh', true);
            $("#max_seats").selectmenu('refresh', true);
            $("#min_seats").selectmenu('refresh', true);

            $.mobile.hidePageLoadingMsg();
        },
        error: function(data) {
            var response = jQuery.parseJSON(data.responseText);

            switch (response.return.code) {
                case 404:
                    showError(response.return.message, "Not Found!");

                    return;
                default:
                    showError(response.return.message, "Error!");

                    return;
            }
        }
    });

    $("#update_claimcode_btn").click(function() {
        var redeem_count = $("#redeem_count").val();
        var validity_type = $("#validity_type").val();
        var max_seats = $("#max_seats").val();
        var min_seats = $("#min_seats").val();
        var seat_price = $("#seat_price").val();
        var screening_date = $("#screening_date").val();
        var screening_time = $("#screening_time").val();
        var expiration_date = $("#expiration_date").val();
        var expiration_time = $("#expiration_time").val();
        var whitelist = $("#whitelist").val();
        var blacklist = $("#blacklist").val();
        var email_to = $("#email_to").val();
        var email_retry = $("#email_retry").val();
        var is_valid = true

        var seat_price_pattern = /^[0-9]+.[0-9]{2}$/;
        var redeem_count_pattern = /^[0-9]+$/;

        if (redeem_count != "" && redeem_count != null && redeem_count != undefined){
            if (redeem_count_pattern.test(redeem_count) == false) {
                showError("Max Uses invalid format. Please try again.", "Check Max number of Uses!");

                return;
            }
        }

        if (validity_type == "" || validity_type === null || validity_type === undefined) {
            showError("Please input Validity Type and try again.", "Validity Type Required!");

            return;
        }

        if (max_seats == "" || max_seats === null || max_seats === undefined) {
            showError("Please input Max Seats and try again.", "Max Seats Required!");

            return;
        }

        if (seat_price == "" || seat_price === null || seat_price === undefined) {
            showError("Please input Seat Price and try again.", "Seat Price Required!");

            return;
        } else {
            if (seat_price_pattern.test(seat_price) == false) {
                showError("Seat Price invalid format. Please try again.", "Check Seat Price!");

                return;
            }
        }

        if (email_to == "" || email_to === null || email_to === undefined) {
            showError("Please input Email and try again.", "Email Required!");

            return;
        }

        if (email_to != email_retry) {
            showError("Email Address don't match. Please try again.", "Check Email Address!");

            return;
        }

        if (validity_type == 'invalid') {
            is_valid = false
        }

        if (transaction_locked) {
            return;
        } else {
            transaction_locked = true;
        }

        $.mobile.showPageLoadingMsg("Processing...");

        var payload = {
            action: "UPDATE_CLAIMCODE",
            id: parent_id,
            claim_code: claimcode_string,
            redeem_count: redeem_count,
            is_valid: is_valid,
            min_seats: min_seats,
            max_seats: max_seats,
            seat_price: seat_price,
            screening_date: screening_date,
            screening_time: screening_time,
            expiration_date: expiration_date,
            expiration_time: expiration_time,
            whitelist: whitelist,
            blacklist: blacklist,
            email_to: email_to
        };

        $.ajax({
            url: "/promos/generate",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify(payload),
            success: function(data) {
                var response = data;
                transaction_locked = false;

                $.mobile.hidePageLoadingMsg();

                if (parent_id != '' && parent_id != null && parent_id != undefined){
                    window.location.href = '/admin/0/gmovies-promos/' + parent_id;
                } else {
                    window.location.href = '/admin/0/gmovies-promos';
                }
            },
            error: function(data) {
                var response = jQuery.parseJSON(data.responseText);
                transaction_locked = false;

                $.mobile.hidePageLoadingMsg();

                switch (response.return.code) {
                    case 400:
                        showError(response.return.message, "Invalid Input!");

                        return;
                    case 401:
                        showError(response.return.message, "Invalid Input!");

                        return;
                    case 404:
                        showError(response.return.message, "Not Found!");

                        return;
                    default:
                        showError(response.return.message, "Error!");

                        return;
                }
            }
        });
    });

    function showError(input_text, header) {
        header = (typeof header === "undefined") ? "Error" : header;
        $("#error-message").text(input_text);
        $("#error-header").text(header);
        $.mobile.changePage('#error', {transition: 'pop'});
    };
});