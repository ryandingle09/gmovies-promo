$(document).ready(function() {
  var transaction_locked = false;

  $("#id_generate_btn").click(function() {
    var email = $("#email").val();
    var email_retry = $("#email_retry").val();
    var batch_code = $("#batch_code").val();
    var number_of_promo_per_batch = $("#number_of_promo_per_batch").val();
    var promo_name = $("#promo_name").val();
    var seat_count = $("#seat_count").val();
    var price_per_seat = $("#price_per_seat").val();
    var expired_date = $("#expired_date").val();
    var expired_time = $("#expired_time").val();
    var promo_id = $("#promo_id").val();
    var batch_code_pattern = /^[A-Z0-9]{7}$/; // batch_code must be valid -- 7-alphanumeric characters
    var promo_per_batch_pattern = /^[0-9]+$/; // number_of_promo_per_batch must be valid -- numeric characters
    var price_per_seat_pattern = /^[0-9]+.[0-9]{2}$/; // price_per_seat must be valid -- numeric characters

    if (batch_code) {
      if (batch_code_pattern.test(batch_code) == false) {
        showError("Please check the Batch Code. Invalid format.", "Check Batch Code");
        return;
      } 
    };

    if (promo_per_batch_pattern.test(number_of_promo_per_batch) == false) {
      showError("Please check the No. of Claim Codes per Batch. Invalid format.", "Check No. of Claim Codes per Batch");
      return;
    } 

    if (parseInt(number_of_promo_per_batch) <= 0 || parseInt(number_of_promo_per_batch) > 500) {
      showError("Please check the No. of Claim Codes per Batch. Value must be greater than 0 and less than or equal to 500.", "Check No. of Claim Codes per Batch");
      return;
    }

    if (price_per_seat != "" && price_per_seat != null && price_per_seat != undefined) {
      if (price_per_seat_pattern.test(price_per_seat) == false) {
        showError("Please check the Price per Seat Reservation. Invalid format.", "Check Price per Seat Reservation");
        return;
      }
    }

    // emails must match
    if (email != email_retry || email == "" || email === undefined) {
      showError("Check the given email address. Email addresses don't match or are empty", "Check Email");
      return;
    }

    // lock first
    if (transaction_locked) {
      return;
    } else {
      transaction_locked = true;
    }

    $.mobile.showPageLoadingMsg("Processing...");
    $.ajax({
      url: "/admin/0/promos/generate_promo_code",
      type: 'POST',
      data: { email: email, batch_code: batch_code, promo_id: promo_id, number_of_promo_per_batch: number_of_promo_per_batch, promo_name: promo_name, seat_count: seat_count, price_per_seat: price_per_seat, expired_date: expired_date, expired_time: expired_time },
      // dataType: 'json',
      success: function(data) {
        var response = jQuery.parseJSON(data);
        transaction_locked = false;

        $.mobile.hidePageLoadingMsg();
        switch (response.return.code) {
          case 400: // wrong formatting or wrong parameters
            showError(response.return.message, "Invalid Input");
            return;
          case 401: // already used msisdn or already used batch_code
            showError(response.return.message, "Already Used");
            return;
          case 200:
            var generated_promo_codes = "";
            var promo_code_count = "<h5>Generated " + response.return.promo_codes.length + " Claim Codes:</h5>";
            for (i=0; i<response.return.promo_codes.length; i++) {
              generated_promo_codes = generated_promo_codes + response.return.promo_codes[i] + "<br />"
            }
            $("#promo-code-count").html(promo_code_count);
            $("#promo-code-value").html(generated_promo_codes);
            // TODO
            // $("#promo-code-count").html("<h5>Try Promos Generated Count:</h5>");
            // $("#promo-code-value").html("<h5>Try Promos Generated Value:</h5>");
            $.mobile.changePage( $("#success"));
            break;
          default:
            showError(response.return.message, "Error");
            return;
        }
      }
    });
  });

  function showError(input_text, header) {
    $("#error-message").text(input_text);
    header = (typeof header === "undefined") ? "Error" : header;
    $("#error-header").text(header);
    $.mobile.changePage('#error', {transition: 'pop'});
  };
});
