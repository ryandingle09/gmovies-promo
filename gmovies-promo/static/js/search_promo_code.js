var filter = $("#id_search_by").val();

$(document).ready(function() {
  $("#btn-group-export").show();
  if ($("#gmovies-promo-table").attr("table-data-length") == 0) {
    $("#btn-group-export").hide();
  };

  if(filter != "promo_code" && filter != "promo_name" && filter != "created_on" && filter != "redeemed_on" && filter != "expired_date" && filter != "batch_code") {
    $("#id_search").prop('disabled', true);
    $("#id_search").hide();
  }

  $("#id_search_by").change(function() {
    var filter = $("#id_search_by").val();

    $("#id_search").val("");

    if(filter == "promo_code" || filter == "promo_name" || filter == "created_on" || filter == "redeemed_on" || filter == "expired_date" || filter == "batch_code") {
      $("#id_search").prop('disabled', false);
      $("#id_search").show();
    } else {
      $("#id_search").prop('disabled', true);
      $("#id_search").hide();
    }
  });

  $("#id_count_btn").click(function() {
    var dict = $.parseJSON($(this).attr("data-button"));
    var search_by = dict.search_by;

    if (search_by == '' || search_by == null || search_by == undefined) {
      search_by = "all"
    }

    $.isLoading({ text: "Loading..." });

    $.ajax({
      url: "/admin/0/promo_codes/count",
      type: 'GET',
      data: {search_by: dict.search_by, search: dict.search},
      success: function(data) {
        var response = jQuery.parseJSON(data);

        switch (response.return.code) {
          case 200:
            $.isLoading("hide");
            bootbox.alert("<strong>Filter:</strong> " + search_by + "<br><br>" +
                          "<strong>Available:</strong> " + response.return.count_available + "<br>" +
                          "<strong>Redeemed:</strong> " + response.return.count_redeemed + "<br>" +
                          "<strong>Expired:</strong> " + response.return.count_expired + "<br><br>" +
                          "<strong>Total:</strong> " + response.return.total_count);
            return;
          default:
            $.isLoading("hide");
            bootbox.alert("ERROR!, " + response.return.message);
            return;
        }
      }
    });
  });

  $(document).on("click", "button[name='redeem_btn']", function(e) {
    e.preventDefault();
    var promo_code = $(this).attr("data-button");
    bootbox.confirm("Redeem promo code " + promo_code + "?", function(result) {
      if (result) {
        var request = $.ajax({
          url: "/redeem",
          type: 'GET',
          data: {promo_code: promo_code},
          success: function(res) {
            bootbox.alert("Successfully redeemed promo code " + promo_code, function() {
                location.reload();
            });
            return;
          },
          error: function(err) {
            switch (err.status) {
              case 400:
                bootbox.alert("Invalid Input: Error in parameters.");
                return;
              case 401:
                bootbox.alert("Promo code " + promo_code + " has already been redeemed/expired.");
                return;
              case 403:
                bootbox.alert("Promo code " + promo_code + " is not entitled to win free movie tickets.");
                return;
              case 404:
                bootbox.alert("Promo code " + promo_code + " seems to be invalid. Please check it and try again.");
                return;
              default:
                bootbox.alert("Error!");
                return;
            }
          }
        });
      }
    });
  });

  $(document).on("click", "button[name='reset_btn']", function(e) {
    e.preventDefault();
    var promo_code = $(this).attr("data-button");
    bootbox.confirm("Reset promo code " + promo_code + "?", function(result) {
      if (result) {
        var request = $.ajax({
          url: "/reset",
          type: 'GET',
          data: {promo_code: promo_code},
          success: function(res) {
            bootbox.alert("Successfully reset promo code " + promo_code, function() {
                location.reload();
            });
            return;
          },
          error: function(err) {
            switch (err.status) {
              case 400:
                bootbox.alert("Invalid Input: Error in parameters.");
                return;
              case 401:
                bootbox.alert("Pending redemption Promo code " + promo_code + ".");
                return;
              case 403:
                bootbox.alert("Unable to reset this promo code " + promo_code + ", because the expiration date is less than the current date.");
                return;
              default:
                bootbox.alert("Error!");
                return;
            }
          }
        });
      }
    });
  });

  $(document).on("click", "button[name='expire_btn']", function(e) {
    e.preventDefault();
    var promo_code = $(this).attr("data-button");
    bootbox.confirm("Expire promo code " + promo_code + "?", function(result) {
      if (result) {
        var request = $.ajax({
          url: "/expire",
          type: 'GET',
          data: {promo_code: promo_code},
          success: function(res) {
            bootbox.alert("Successfully expired promo code " + promo_code, function() {
                location.reload();
            });
            return;
          },
          error: function(err) {
            switch (err.status) {
              case 400:
                bootbox.alert("Invalid Input: Error in parameters.");
                return;
              case 401:
                bootbox.alert("Promo code " + promo_code + " has already been redeemed/expired.");
                return;
              default:
                bootbox.alert("Error!");
                return;
            }
          }
        });
      }
    });
  });
});