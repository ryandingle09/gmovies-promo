// Initialize js tools
$('button').tooltip();

// initialize actions
$(document).on('click', '.action', function(){
    var dis = $(this)
    var action = dis.attr('data-action');
    var id = dis.attr('data-id');
    var type = 'GET';
    var form = new FormData($('.form-data')[0]);

    $('.inputs, .done_loading').hide();
    $('.is_loading').show();

    if(action == 'confirm-delete')
    {
        $('.modal-confirm .action-delete').attr('data-id', id);
        $('.modal-confirm').modal('show');
    }
    else
    {
        var title = '<i class="glyphicon glyphicon-list"></i> Details'

        if(action == 'edit')
        {
            title = '<i class="glyphicon glyphicon-edit"></i> Edit Details';
            $('.not-crud').attr('data-action', 'update');
        }
        else if(action == 'action-add' || action == 'insert') 
        {
            type = 'POST';
            title = '<i class="glyphicon glyphicon-plus"></i> Add New';
            $('input, textarea').val('');

            if(action == 'action-add')
            {
                $('.not-crud').attr('data-action', 'insert');
                $('.not-crud').attr('data-id', '');
            }
        }

        if(action == 'insert' || action == 'update' || action == 'delete') type = 'POST';

        if(action != 'delete' || action != 'update' || action != 'insert')
        {
            $('.message').hide();
            $('.dynamicModal .modal-title').html(title);
            $('.dynamicModal').modal('show');
        }
        
        if(action == 'action-add' || action == 'edit') 
        {
            $('.is_loading, .list').hide();
            $('.inputs, .done_loading, .not-crud').show();
        }
        else if(action == 'view')
        {
            $('.is_loading, .inputs, .not-crud').hide();
            $('.list, .done_loading').show();
        }

        if(action == 'view' || action == 'edit' || action == 'insert' || action == 'update' || action == 'delete')
            ajax_run(form, type, action, id)
    }
});

function ajax_run(form, type, action, id) 
{
    if(action == 'edit' || action == 'view')
    {
        $.ajax({
            url: '/admin/0/api-settings?action='+action+'&id='+id,
            type: type,
            success: function(data){
                item = JSON.parse(data);
                $('input[name="name"]').val(item.name);
                $('input[name="slug"]').val(item.slug);
                $('textarea[name="promo_name"]').val(item.promo_name);
                $('textarea[name="prefix"]').val(item.prefix);
                $('input[name="endpoint"]').val(item.endpoint);
                $('input[name="method"]').val(item.method);
                $('textarea[name="payload"]').val(item.payload);
                $('textarea[name="headers"]').val(item.headers);

                $('.name').html('<b>'+item.name+'</b>');
                $('.slug').html('<b>'+item.slug+'</b>');
                $('.token').html('<b>'+item.token+'</b>');
                $('.promo_name').html('<b>'+item.promo_name+'</b>');
                $('.prefix').html('<b>'+item.prefix+'</b>');
                $('.endpoint').html('<b>'+item.endpoint+'</b>');
                $('.method').html('<b>'+item.method+'</b>');
                $('.payload').html('<b>'+item.payload+'</b>');
                $('.headers').html('<b>'+item.headers+'</b>');
                $('.created_at').html('<b>'+item.created_at+'</b>');
                $('.updated_at').html('<b>'+item.updated_at+'</b>');

                $('.not-crud').attr('data-id', item.id);
            }
        });
    }
    else
    {
        if(!isValidURL(form.get('endpoint'))) {
            $('.done_loading,.not-crud,.inputs').show();
            $('.is_loading, .list').hide();
            $('.message').html('Invalid Client Call Endpoint URL').show();
        } else {
            $.ajax({
                url: '/admin/0/api-settings?action='+action+'&id='+id,
                type: type,
                data: form,
                processData: false,
                contentType: false,
                success: function(response){
                    data = JSON.parse(response);

                    if(data.status == 'success'){
                        url = window.location.href = window.location.protocol+'//'+window.location.hostname+(location.port ? ':'+location.port: '')+'/admin/0/api-settings'
                        location.href = url;
                    }else{
                        $('.done_loading,.not-crud,.inputs').show();
                        $('.is_loading, .list').hide();
                        $('.message').html(data.status).show();
                    }
                }
            });
        }
    }
}

function isValidURL(string) {
    var res = string.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
    if (res == null)
      return false;
    else
      return true;
}