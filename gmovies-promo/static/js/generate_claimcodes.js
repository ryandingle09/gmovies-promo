$(document).ready(function() {
    var transaction_locked = false;
    var promo_category_id = window.location.href.slice(window.location.href.indexOf('?') + 1).split('=')[1]

    $.mobile.showPageLoadingMsg("Processing...");

    $.ajax({
        url: "/promos/generate",
         type: "GET",
        data: {action: "UPDATE_PROMO", id: promo_category_id},
        success: function(data) {
            var response = jQuery.parseJSON(data);
            var promo_category = response.return.results;

            $('#name').val(promo_category['name']);

            $.mobile.hidePageLoadingMsg();
        },
        error: function(data) {
            var response = jQuery.parseJSON(data.responseText);

            switch (response.return.code) {
                case 404:
                    showError(response.return.message, "Not Found!");

                    return;
                default:
                    showError(response.return.message, "Error!");

                    return;
            }
        }
    });

    $("#generate_claimcodes_btn").click(function() {
        var claimcode_quantity = $("#claimcode_quantity").val();
        var claimcode_quantity_pattern = /^[0-9]+$/;

        if (claimcode_quantity == "" || claimcode_quantity === null || claimcode_quantity === undefined) {
            showError("Please input Claim Code Quantity and try again.", "Claim Code Quantity Required!");

            return;
        } else {
            if (claimcode_quantity_pattern.test(claimcode_quantity) == false) {
                showError("Claim Code Quantity invalid format. Please try again.", "Check Claim Code Quantity!");

                return;
            }
        }

        var payload = {
            action: "GENERATE_CLAIMCODES",
            id: promo_category_id,
            claimcode_quantity: claimcode_quantity
        };

        $.ajax({
            url: "/promos/generate",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify(payload),
            success: function(data) {
                var response = data;
                transaction_locked = false;

                $.mobile.hidePageLoadingMsg();

                window.location.href = '/admin/0/gmovies-promos/' + response.return.id;
            },
            error: function(data) {
                var response = jQuery.parseJSON(data.responseText);
                transaction_locked = false;

                $.mobile.hidePageLoadingMsg();

                switch (response.return.code) {
                    case 404:
                        showError(response.return.message, "Not Found!");

                        return;
                    default:
                        showError(response.return.message, "Error!");

                        return;
                }
            }
        });
    });

    function showError(input_text, header) {
        header = (typeof header === "undefined") ? "Error" : header;
        $("#error-message").text(input_text);
        $("#error-header").text(header);
        $.mobile.changePage('#error', {transition: 'pop'});
    };
});