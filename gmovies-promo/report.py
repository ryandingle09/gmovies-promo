import os
import jinja2
import webapp2
import logging
import datetime
import sys
import StringIO
import re
from calendar import monthrange

sys.path.insert(0, 'libs')
import xlwt

from models import PromoCode
from main import get_theater_name_and_code

log = logging.getLogger(__name__)

JINJA_ENVIRONMENT = jinja2.Environment(
            loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
            extensions=['jinja2.ext.autoescape'], autoescape=True)

class BadRequestError(Exception):
    def __init__(self, param, code):
        super(Exception, self).__init__()
        self.msg = param
        self.code = code

class ReportClaimCodeHandler(webapp2.RequestHandler):
    def get(self, year, month):
        try:
            # check if year is valid
            y_pattern = re.compile('^[1-9]\d{3}$')
            if not y_pattern.match(year):
                return self.response.out.write("Invalid Year")
            m_pattern = re.compile('^(([1-9])|([1][0-2]))$')
            if not m_pattern.match(month):
                return self.response.out.write("Invalid Month (Usage: 1 - 12)")

            year = int(year)
            month = int(month)
            status_list = {'is_expired': 'EXPIRED', 'is_invalid': 'INVALID', 'is_redeemed': 'REDEEMED', 'available': 'AVAILABLE'}
            headers = ['DATE GENERATED','CLAIM CODE','PROMO NAME','SEAT COUNT','TOTAL AMOUNT','EXPIRATION DATE','DATE REDEEMED','REDEEMED NO.\nOF SEATS','REDEEMED\nAMOUNT','CINEMA\nPARTNER']

            workbook = xlwt.Workbook(encoding='utf-8')
            # set format for cells
            headers_font_style = xlwt.XFStyle()
            headers_font_style.font.bold = True
            headers_font_style.alignment.horz = headers_font_style.alignment.HORZ_CENTER
            headers_font_style.alignment.vert = headers_font_style.alignment.VERT_CENTER
            font_style = xlwt.XFStyle()
            font_style.font.bold = False
            font_style.alignment.horz = font_style.alignment.HORZ_CENTER
            empty_font_style = xlwt.XFStyle()
            empty_font_style.font.bold = False
            empty_font_style.alignment.horz = empty_font_style.alignment.HORZ_LEFT

            # partition query per status
            for status, name in status_list.items():
                # Create Sheet for STATUS
                worksheet = workbook.add_sheet(name)
                row = 0

                # write headers
                col_i = 0
                for h in headers:
                    worksheet.write(row, col_i, h, headers_font_style)
                    worksheet.col(col_i).width = 5500
                    col_i += 1

                # flag to know if there is claim code data for the specific status
                empty_flag = True

                start_day = datetime.datetime(year, month, 1)
                end_day = datetime.datetime(year, month, monthrange(year, month)[1], 23, 59, 59)

                query_set = PromoCode.query(PromoCode.created_on >= start_day, PromoCode.created_on <= end_day).order(-PromoCode.created_on)

                if 'is_expired' == status:
                    qs = query_set.filter(PromoCode.is_expired == True)
                elif 'is_invalid' == status:
                    qs = query_set.filter(PromoCode.is_invalid == True)
                elif 'is_redeemed' == status:
                    qs = query_set.filter(PromoCode.is_redeemed == True)
                else:
                    qs = query_set.filter(PromoCode.is_expired == False, PromoCode.is_invalid == False, PromoCode.is_redeemed == False)

                if 0 < qs.count():
                    empty_flag = False

                for promo_code in qs:
                    row += 1

                    dt_created = promo_code.created_on.strftime('%m/%d/%Y %I:%M %p') if promo_code.created_on else ""
                    worksheet.write(row, 0, dt_created, font_style)

                    worksheet.write(row, 1, getattr(promo_code, "promo_code", ""), font_style)
                    worksheet.write(row, 2, getattr(promo_code, "promo_name", ""), font_style)

                    seat_count = getattr(promo_code, "seat_count", 0)
                    worksheet.write(row, 3, seat_count, font_style)

                    price_per_seat = getattr(promo_code, "price_per_seat", 0)
                    purchase_amount = getattr(promo_code, "purchase_amount", 0)
                    total_amount = purchase_amount if True == promo_code.is_bank_deposit else (seat_count * price_per_seat)
                    worksheet.write(row, 4, total_amount, font_style)

                    dt_expiry = promo_code.expired_date.strftime('%m/%d/%Y %I:%M %p') if promo_code.expired_date else ""
                    worksheet.write(row, 5, dt_expiry, font_style)

                    dt_redeemed = promo_code.redeemed_on.strftime('%m/%d/%Y %I:%M %p') if promo_code.redeemed_on else ""
                    worksheet.write(row, 6, dt_redeemed, font_style)

                    redeemed_seat_count = 0
                    booked_price_per_seat = 0
                    redeemed_amount = 0
                    cinema = ""
                    if promo_code.booking_details:
                        if promo_code.booking_details.booked_seat_count:
                            redeemed_seat_count = promo_code.booking_details.booked_seat_count
                        if promo_code.booking_details.booked_price_per_seat:
                            booked_price_per_seat = promo_code.booking_details.booked_price_per_seat
                        redeemed_amount = redeemed_seat_count * booked_price_per_seat
                        if promo_code.booking_details.theater:
                            theater = promo_code.booking_details.theater.get()
                            if theater and theater.name:
                                cinema = theater.name
                        if not cinema and promo_code.booking_details.theater_id and promo_code.booking_details.theaterorg_id:
                            cinema = get_theater_name_and_code(
                            promo_code.booking_details.theater_id,
                            promo_code.booking_details.theaterorg_id)[0]

                    worksheet.write(row, 7, redeemed_seat_count, font_style)
                    worksheet.write(row, 8, redeemed_amount, font_style)
                    worksheet.write(row, 9, cinema, font_style)

                if empty_flag:
                    worksheet.write(1, 0, "No CLAIM CODE for %s status" % name, empty_font_style)

            file_xls = datetime.datetime.today().strftime("claimcodesreports_" + str(year) + "_" + str(month).zfill(2) + "_%Y%m%d.xls")
            xls_reports = StringIO.StringIO()
            workbook.save(xls_reports)
            self.response.headers['Content-Type'] = 'application/vnd.ms-excel'
            self.response.headers['Content-Disposition'] = 'attachment; filename="' + file_xls + '"'
            self.response.out.write(xls_reports.getvalue())

        except BadRequestError, (instance):
            self.error(instance.code)
            self.response.out.write(instance.msg)

        except Exception, e:
            log.warn("ERROR!, ReportClaimCodeHandler, GET...")
            log.error(e)

app = webapp2.WSGIApplication([webapp2.Route('/admin/0/reports/claim_code/<year>/<month>', ReportClaimCodeHandler)],
        debug=True)