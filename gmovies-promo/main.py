import calendar
import datetime
import jinja2
import json
import logging
import os
import re
import urllib
import webapp2
import StringIO
import sys

sys.path.insert(0, 'libs')

import xlwt

from decimal import Decimal
from operator import itemgetter
from uuid import uuid4 as uuid_gen

from google.appengine.api import users
from google.appengine.ext import deferred, ndb
from google.appengine.runtime import DeadlineExceededError

from models import (asia_manila_timezone, Movie, PromoCategory, PromoCode, Theater, TheaterOrganization, MpassAccounts,
        DEFAULT_PRICE_PER_SEAT, DEFAULT_SEAT_COUNT, DEFAULT_THEATERORG, THEATERORGS)
from util import DEFAULT_ERROR_MESSAGE_ADMIN


log = logging.getLogger(__name__)
JINJA_ENVIRONMENT = jinja2.Environment(loader=jinja2.FileSystemLoader(os.path.dirname(__file__)), extensions=['jinja2.ext.autoescape'], autoescape=True)


class BadRequestError(Exception):
    def __init__(self, param, code):
        super(Exception, self).__init__()
        self.msg = param
        self.code = code


class PromoCategoryIndexHandler(webapp2.RequestHandler):
    def get(self):
        try:
            cursor = ndb.Cursor(urlsafe=self.request.get('cursor'))
            promo_categories = PromoCategory.query().order(-PromoCategory.date_created)
            promo_categories_batch, next_cursor, more = promo_categories.fetch_page(100, start_cursor=cursor)

            template_values = {'promo_categories': promo_categories_batch, 'next_cursor': next_cursor, 'more': more}
            template = JINJA_ENVIRONMENT.get_template('templates/promo_category_index.html')
            self.response.write(template.render(template_values))
        except BadRequestError, instance:
            log.warn("ERROR!, PromoCategoryIndexHandler, GET, BadRequestError...")

            self.error(instance.code)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)
        except Exception, e:
            log.warn("ERROR!, PromoCategoryIndexHandler, GET...")
            log.error(e)

            self.error(instance.code)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)


class PromoCategoryShowHandler(webapp2.RequestHandler):
    def get(self, promo_category_id):
        try:
            promo_category_key = ndb.Key(PromoCategory, promo_category_id)
            promo_category = promo_category_key.get()

            if not promo_category:
                raise BadRequestError(DEFAULT_ERROR_MESSAGE_ADMIN, 404)

            cursor_available = ndb.Cursor(urlsafe=self.request.get('cursor_available'))
            claimcodes_available = PromoCode.query(PromoCode.is_redeemed==False, PromoCode.is_expired==False,
                    PromoCode.is_invalid==False, ancestor=promo_category_key).order(-PromoCode.created_on)
            claimcodes_available_batch, a_next_cursor, a_more = claimcodes_available.fetch_page(100, start_cursor=cursor_available)

            cursor_redeemed = ndb.Cursor(urlsafe=self.request.get('cursor_redeemed'))
            claimcodes_redeemed = PromoCode.query(PromoCode.is_redeemed==True, ancestor=promo_category_key).order(-PromoCode.redeemed_on)
            claimcodes_redeemed_batch, r_next_cursor, r_more = claimcodes_redeemed.fetch_page(100, start_cursor=cursor_redeemed)

            cursor_expired = ndb.Cursor(urlsafe=self.request.get('cursor_expired'))
            claimcodes_expired = PromoCode.query(PromoCode.is_expired==True, ancestor=promo_category_key)
            claimcodes_expired_batch, e_next_cursor, e_more = claimcodes_expired.fetch_page(100, start_cursor=cursor_expired)

            cursor_invalid = ndb.Cursor(urlsafe=self.request.get('cursor_invalid'))
            claimcodes_invalid = PromoCode.query(PromoCode.is_invalid==True, ancestor=promo_category_key)
            claimcodes_invalid_batch, i_next_cursor, i_more = claimcodes_invalid.fetch_page(100, start_cursor=cursor_invalid)

            template_values = {'promo_category': promo_category, 'claimcodes_available': claimcodes_available_batch,
                    'claimcodes_redeemed': claimcodes_redeemed_batch, 'claimcodes_expired': claimcodes_expired_batch,
                    'claimcodes_invalid': claimcodes_invalid_batch, 'a_next_cursor': a_next_cursor, 'r_next_cursor': r_next_cursor,
                    'e_next_cursor': e_next_cursor, 'i_next_cursor': i_next_cursor, 'a_more': a_more, 'r_more': r_more, 'e_more': e_more,
                    'i_more': i_more}
            template = JINJA_ENVIRONMENT.get_template('templates/promo_category_show.html')
            self.response.write(template.render(template_values))
        except BadRequestError, instance:
            log.warn("ERROR!, PromoCategoryShowHandler, GET, BadRequestError...")

            self.error(instance.code)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)
        except Exception, e:
            log.warn("ERROR!, PromoCategoryShowHandler, GET...")
            log.error(e)

            self.error(500)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)


class PromoCodesHandler(webapp2.RequestHandler):
    def get(self):
        try:
            cursor = ndb.Cursor(urlsafe=self.request.get('cursor'))
            filter_by = self.request.get('filter_by')
            filter_value = self.request.get('filter_value')

            if filter_by == 'promo_code':
                log.debug("PromoCodesHandler, GET, filter_by, promo_code: %s..." % filter_value)

                claimcodes_queryset = PromoCode.query(PromoCode.promo_code==filter_value.upper())
            elif filter_by == 'promo_name':
                log.debug("PromoCodesHandler, GET, filter_by, promo_name: %s..." % filter_value)

                filter_value_list = [filter_value, filter_value.lower(), filter_value.title()] if filter_value else []
                claimcodes_queryset = PromoCategory.query(PromoCategory.name.IN(filter_value_list)).order(-PromoCategory.date_created, PromoCategory._key)
            elif filter_by == 'batch_code':
                log.debug("PromoCodesHandler, GET, filter_by, batch_code: %s..." % filter_value)

                claimcodes_queryset = PromoCode.query(PromoCode.batch_code==filter_value.upper()).order(-PromoCode.redeemed_on, PromoCode.key)
            else:
                log.debug("PromoCodesHandler, GET, all...")

                claimcodes_queryset = PromoCode.query().order(-PromoCode.redeemed_on)

            claimcodes_queryset_batch, next_cursor, more = claimcodes_queryset.fetch_page(100, start_cursor=cursor)

            template_values = {'claimcodes': claimcodes_queryset_batch, 'next_cursor': next_cursor, 'more': more,
                    'filter_by': filter_by, 'filter_value': filter_value}
            template = JINJA_ENVIRONMENT.get_template('templates/promo_codes.html')
            self.response.write(template.render(template_values))
        except BadRequestError, instance:
            log.warn("ERROR!, PromoCodesHandler, GET, BadRequestError...")

            self.error(instance.code)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)
        except Exception, e:
            log.warn("ERROR!, PromoCodesHandler, GET...")
            log.error(e)

            self.error(500)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)


class MovieIndexHandler(webapp2.RequestHandler):
    def get(self):
        try:
            movies = Movie.query().fetch()

            template_values = {'movies': movies}
            template = JINJA_ENVIRONMENT.get_template('templates/movie_index.html')
            self.response.write(template.render(template_values))
        except BadRequestError, instance:
            log.warn("ERROR!, MovieIndexHandler, GET, BadRequestError...")

            self.error(instance.code)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)
        except Exception, e:
            log.warn("ERROR!, MovieIndexHandler, GET...")
            log.error(e)

            self.error(500)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)


class MovieCreateHandler(webapp2.RequestHandler):
    def get(self):
        try:
            template_values = {}
            template = JINJA_ENVIRONMENT.get_template('templates/movie_create.html')
            self.response.write(template.render(template_values))
        except BadRequestError, instance:
            log.warn("ERROR!, MovieCreateHandler, GET, BadRequestError...")

            self.error(instance.code)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)
        except Exception, e:
            log.warn("ERROR!, MovieCreateHandler, GET...")
            log.error(e)

            self.error(500)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)

    def post(self):
        try:
            movie_id = self.request.POST['movie_id']
            movie_title = self.request.POST['movie_title']

            if not movie_id:
                raise BadRequestError('Movie ID Required!', 404)

            if not movie_title:
                raise BadRequestError('Movie Title Required!', 404)

            movie = Movie(key=ndb.Key(Movie, movie_id))
            movie.name = movie_title
            movie.gmovies_movie_id = movie_id
            movie.put()

            return webapp2.redirect('/admin/0/movies/%s' % movie_id)
        except BadRequestError, instance:
            log.warn("ERROR!, MovieCreateHandler, POST, BadRequestError...")

            self.error(instance.code)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)
        except Exception, e:
            log.warn("ERROR!, MovieCreateHandler, POST...")
            log.error(e)

            self.error(500)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)


class MovieShowUpdateHandler(webapp2.RequestHandler):
    def get(self, movie_id):
        try:
            movie_key = ndb.Key(Movie, movie_id)
            log.debug("MovieShowUpdateHandler, GET, movie_key: {}".format(movie_key))
            movie = movie_key.get()
            log.debug("MovieShowUpdateHandler, GET, movie: {}".format(movie))

            if not movie:
                raise BadRequestError('Movie Not Found!', 404)

            template_values = {'movie': movie}
            template = JINJA_ENVIRONMENT.get_template('templates/movie_show.html')
            self.response.write(template.render(template_values))
        except BadRequestError, instance:
            log.warn("ERROR!, MovieShowUpdateHandler, GET, BadRequestError...")

            self.error(instance.code)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)
        except Exception, e:
            log.warn("ERROR!, MovieShowUpdateHandler, GET...")
            log.error(e)

            self.error(500)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)

    def post(self, movie_id):
        try:
            movie_title = self.request.POST['movie_title']

            movie_key = ndb.Key(Movie, movie_id)
            movie = movie_key.get()

            if not movie:
                raise BadRequestError('Movie Not Found!', 404)

            if not movie_title:
                raise BadRequestError('Movie Title Required!', 404)

            movie.name = movie_title
            movie.put()

            return webapp2.redirect('/admin/0/movies/%s' % movie_id)
        except BadRequestError, instance:
            log.warn("ERROR!, MovieShowUpdateHandler, POST, BadRequestError...")

            self.error(instance.code)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)
        except Exception, e:
            log.warn("ERROR!, MovieShowUpdateHandler, POST...")
            log.error(e)

            self.error(500)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)


class TheaterOrganizationIndexHandler(webapp2.RequestHandler):
    def get(self):
        try:
            theaterorgs = dict([(t.key.id(), t.name) for t in TheaterOrganization.query().fetch(projection=['name'])])

            for org_id in THEATERORGS:
                if org_id not in theaterorgs:
                    theaterorgs[org_id] = THEATERORGS[org_id]

            template_values = {'theaterorgs': theaterorgs}
            template = JINJA_ENVIRONMENT.get_template('templates/theaterorg_index.html')
            self.response.write(template.render(template_values))
        except BadRequestError, instance:
            log.warn("ERROR!, TheaterOrganizationIndexHandler, GET, BadRequestError...")

            self.error(instance.code)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)
        except Exception, e:
            log.warn("ERROR!, TheaterOrganizationIndexHandler, GET...")
            log.error(e)

            self.error(500)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)


class TheaterOrganizationCreateHandler(webapp2.RequestHandler):
    def get(self):
        try:
            template_values = {}
            template = JINJA_ENVIRONMENT.get_template('templates/theaterorg_create.html')
            self.response.write(template.render(template_values))
        except BadRequestError, instance:
            log.warn("ERROR!, TheaterOrganizationCreateHandler, GET, BadRequestError...")

            self.error(instance.code)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)
        except Exception, e:
            log.warn("ERROR!, TheaterOrganizationCreateHandler, GET...")
            log.error(e)

            self.error(500)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)

    def post(self):
        try:
            gmovies_theaterorg_id = self.request.POST['gmovies_theaterorg_id']
            theaterorg_name = self.request.POST['theaterorg_name']
            url_parameter = self.request.POST['url_parameter']

            if not gmovies_theaterorg_id or not theaterorg_name or not url_parameter:
                raise BadRequestError("ERROR!, Missing Field/s!", 404)

            theaterorg = TheaterOrganization(key=ndb.Key(TheaterOrganization, gmovies_theaterorg_id))
            theaterorg.name = theaterorg_name
            theaterorg.gmovies_theaterorg_id = gmovies_theaterorg_id
            theaterorg.url_parameter = url_parameter
            theaterorg.put()

            return webapp2.redirect('/admin/0/theaterorgs')
        except BadRequestError, instance:
            log.warn("ERROR!, TheaterOrganizationCreateHandler, POST, BadRequestError...")

            self.error(instance.code)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)
        except Exception, e:
            log.warn("ERROR!, TheaterOrganizationCreateHandler, POST...")
            log.error(e)

            self.error(500)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)


class TheaterOrganizationShowUpdateHandler(webapp2.RequestHandler):
    def get(self, theaterorg_id):
        try:
            allowed_users = ''
            theaterorg_key = ndb.Key(TheaterOrganization, theaterorg_id)
            theaterorg = theaterorg_key.get()

            if theaterorg is None:
                if theaterorg_id in THEATERORGS:
                    theaterorg = TheaterOrganization(name=THEATERORGS[theaterorg_id])
                    theaterorg.key = ndb.Key(TheaterOrganization, theaterorg_id)
                    theaterorg.gmovies_theaterorg_id = theaterorg_id
                    theaterorg.url_parameter = THEATERORGS[theaterorg_id].lower().replace(' ', '')
                    theaterorg.put()
                else:
                    raise BadRequestError("Theater Organization Not Found!", 404)

            if theaterorg.allowed_users:
                allowed_users = ','.join(theaterorg.allowed_users)

            theaters = Theater.query(ancestor=theaterorg.key).fetch(projection=['name'])
            template_values = {'theaterorg': theaterorg, 'theaters':theaters, 'allowed_users': allowed_users}
            template = JINJA_ENVIRONMENT.get_template('templates/theaterorg_show.html')
            self.response.write(template.render(template_values))
        except BadRequestError, instance:
            log.warn("ERROR!, TheaterOrganizationShowUpdateHandler, GET, BadRequestError...")

            self.error(instance.code)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)
        except Exception, e:
            log.warn("ERROR!, TheaterOrganizationShowUpdateHandler, GET...")
            log.error(e)

            self.error(500)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)

    def post(self, theaterorg_id):
        try:
            gmovies_theaterorg_id = self.request.POST['gmovies_theaterorg_id']
            theaterorg_name = self.request.POST['theaterorg_name']
            url_parameter = self.request.POST['url_parameter']
            allowed_users = self.request.POST['allowed_users']

            theaterorg_key = ndb.Key(TheaterOrganization, theaterorg_id)
            theaterorg = theaterorg_key.get()

            if theaterorg is None:
                raise BadRequestError("Theater Organization Not Found!", 404)

            if not gmovies_theaterorg_id or not theaterorg_name or not url_parameter:
                raise BadRequestError("ERROR!, Missing Field/s!", 404)

            theaterorg.name = theaterorg_name
            theaterorg.gmovies_theaterorg_id = gmovies_theaterorg_id
            theaterorg.url_parameter = url_parameter
            theaterorg.allowed_users = allowed_users.split(',')
            theaterorg.put()

            return  webapp2.redirect('/admin/0/theaterorgs')
        except BadRequestError, instance:
            log.warn("ERROR!, TheaterOrganizationShowUpdateHandler, POST, BadRequestError...")

            self.error(instance.code)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)
        except Exception, e:
            log.warn("ERROR!, TheaterOrganizationShowUpdateHandler, POST...")
            log.error(e)

            self.error(500)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)


class TheaterCreateHandler(webapp2.RequestHandler):
    def get(self, theaterorg_id):
        try:
            theaterorg_key = ndb.Key(TheaterOrganization, theaterorg_id)
            theaterorg = theaterorg_key.get()

            if theaterorg is None:
                raise BadRequestError("Theater Organization Not Found!", 404)

            template_values = {'theaterorg': theaterorg}
            template = JINJA_ENVIRONMENT.get_template('templates/theater_create.html')
            self.response.write(template.render(template_values))
        except BadRequestError, instance:
            log.warn("ERROR!, TheaterCreateHandler, GET, BadRequestError...")

            self.error(instance.code)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)
        except Exception, e:
            log.warn("ERROR!, TheaterCreateHandler, GET...")
            log.error(e)

            self.error(500)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)

    def post(self, theaterorg_id):
        try:
            gmovies_theater_id = self.request.POST['gmovies_theater_id']
            theater_name = self.request.POST['theater_name']
            theater_code = self.request.POST['theater_code']
            url_parameter = self.request.POST['url_parameter']

            theaterorg_key = ndb.Key(TheaterOrganization, theaterorg_id)
            theaterorg = theaterorg_key.get()

            if theaterorg is None:
                raise BadRequestError("Theater Organization Not Found!", 404)

            if not gmovies_theater_id or not theater_name or not url_parameter:
                raise BadRequestError("ERROR!, Missing Field/s!", 404)

            theater = Theater()
            theater.key = ndb.Key(Theater, int(gmovies_theater_id), parent=theaterorg_key)
            theater.name = theater_name
            theater.theater_code = theater_code
            theater.gmovies_theater_id = gmovies_theater_id
            theater.url_parameter = url_parameter
            theater.put()

            return webapp2.redirect('/admin/0/theaterorgs/' + theaterorg_id)
        except BadRequestError, instance:
            log.warn("ERROR!, TheaterCreateHandler, POST, BadRequestError...")

            self.error(instance.code)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)
        except Exception, e:
            log.warn("ERROR!, TheaterCreateHandler, POST...")
            log.error(e)

            self.error(500)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)


class TheaterShowUpdateHandler(webapp2.RequestHandler):
    def get(self, theaterorg_id, theater_id):
        try:
            allowed_users = ''
            theaterorg_key = ndb.Key(TheaterOrganization, theaterorg_id)
            theaterorg = theaterorg_key.get()

            if theaterorg is None:
                raise BadRequestError("Theater Organization Not Found!", 404)

            theater_key = ndb.Key(Theater, int(theater_id), parent=theaterorg_key)
            theater = theater_key.get()

            if theater is None:
                raise BadRequestError("Theater Not Found!", 404)

            if theater.allowed_users:
                allowed_users = ','.join(theater.allowed_users)

            template_values = {'theaterorg': theaterorg, 'theater':theater,
                    'allowed_users': allowed_users}
            template = JINJA_ENVIRONMENT.get_template('templates/theater_show.html')
            self.response.write(template.render(template_values))
        except BadRequestError, instance:
            log.warn("ERROR!, TheaterShowUpdateHandler, GET, BadRequestError...")

            self.error(instance.code)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)
        except Exception, e:
            log.warn("ERROR!, TheaterShowUpdateHandler, GET...")
            log.error(e)

            self.error(500)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)

    def post(self, theaterorg_id, theater_id):
        try:
            gmovies_theater_id = self.request.POST['gmovies_theater_id']
            theater_name = self.request.POST['theater_name']
            theater_code = self.request.POST['theater_code']
            url_parameter = self.request.POST['url_parameter']
            allowed_users = self.request.POST['allowed_users']

            theaterorg_key = ndb.Key(TheaterOrganization, theaterorg_id)
            theaterorg = theaterorg_key.get()

            if theaterorg is None:
                raise BadRequestError("Theater Organization Not Found!", 404)

            theater_key = ndb.Key(Theater, int(theater_id), parent=theaterorg_key)
            theater = theater_key.get()

            if theater is None:
                raise BadRequestError("Theater Not Found!", 404)

            if not gmovies_theater_id or not theater_name or not url_parameter:
                raise BadRequestError("ERROR!, Missing Field/s!", 404)

            theater.name = theater_name
            theater.theater_code = theater_code
            theater.gmovies_theaterorg_id = gmovies_theater_id
            theater.url_parameter = url_parameter
            theater.allowed_users = allowed_users.split(',')
            theater.put()

            return  webapp2.redirect('/admin/0/theaterorgs/' + theaterorg_id)
        except BadRequestError, instance:
            log.warn("ERROR!, TheaterShowUpdateHandler, POST, BadRequestError...")

            self.error(instance.code)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)
        except Exception, e:
            log.warn("ERROR!, TheaterShowUpdateHandler, POST...")
            log.error(e)

            self.error(500)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)


class MainHandler(webapp2.RequestHandler):
    def get(self):
        try:
            theaterorgs = TheaterOrganization.query().fetch()
            template_values = {'theaterorgs': theaterorgs, 'users': users}
            template = JINJA_ENVIRONMENT.get_template('templates/admin_panel/index.html')
            self.response.write(template.render(template_values))
        except BadRequestError, instance:
            log.warn("ERROR!, MainHandler, GET, BadRequestError...")

            self.error(instance.code)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)
        except Exception, e:
            log.warn("ERROR!, MainHandler, GET...")
            log.error(e)

            self.error(500)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)


class AdminPanelHandler(webapp2.RequestHandler):
    def get(self, theaterorg_url_parameter):
        cursor = ndb.Cursor(urlsafe=self.request.get('cursor'))
        theater_url_parameter = self.request.get('theater')
        action = self.request.get('action')
        current_user = users.get_current_user()

        try:
            today = datetime.datetime.now()
            date_from = None
            date_to = None

            theaterorg = TheaterOrganization.query(TheaterOrganization.url_parameter == theaterorg_url_parameter).get()
            theater = theater = Theater.query(Theater.url_parameter == theater_url_parameter).get()

            if theaterorg is None and not users.is_current_user_admin():
                raise BadRequestError("Current logged in user %s is not authorized to view this page." % current_user.email(), 401)
            elif theaterorg is None:
                raise BadRequestError("Theater Organization Not Found!", 404)

            if  theater is None and current_user.email() not in theaterorg.allowed_users and not users.is_current_user_admin():
                raise BadRequestError("Current logged in user %s is not authorized to view this page." % current_user.email(), 401)

            if 'from' not in self.request.params and 'to' not in self.request.params:
                queryset = PromoCode.query(PromoCode.booking_details.theaterorg_id == theaterorg.gmovies_theaterorg_id).order(-PromoCode.redeemed_on, PromoCode.key)
            else:
                if not self.request.get('from') and not self.request.get('to'):
                    queryset = PromoCode.query(PromoCode.booking_details.theaterorg_id == theaterorg.gmovies_theaterorg_id).order(-PromoCode.redeemed_on, PromoCode.key)
                else:
                    if self.request.get('from'):
                        date_from = parse_date(self.request.get('from') + ' 00:00:00')
                    else:
                        date_from = parse_date(str(today.date()) + ' 00:00:00')

                    if self.request.get('to'):
                        date_to = parse_date(self.request.get('to') + ' 23:59:59')
                    else:
                        date_to = parse_date(str(today.date()) + ' 23:59:59')

                    if date_from is None or date_to is None:
                        raise BadRequestError("Invalid Parameters.", 400)

                    queryset = PromoCode.query(PromoCode.booking_details.theaterorg_id == theaterorg.gmovies_theaterorg_id,
                                        PromoCode.redeemed_on >= date_from,
                                        PromoCode.redeemed_on <= date_to).order(-PromoCode.redeemed_on, PromoCode.key)

            if theater_url_parameter:
                if theater:
                    if (current_user.email() not in theater.allowed_users and
                            current_user.email() not in theaterorg.allowed_users and
                            not users.is_current_user_admin()):
                        raise BadRequestError("Current logged in user %s is not authorized to view this page." % current_user.email(), 401)

                    queryset = queryset.filter(PromoCode.booking_details.theater_id == theater.gmovies_theater_id)
                else:
                    raise BadRequestError("Theater Not Found!", 404)

            if action == 'export':
                xls_reports = self.export_to_xls(queryset)
                filename_xls =  datetime.datetime.today().strftime("claimcodesreports_%Y%m%d.xls")
                self.response.headers['Content-Type'] = 'application/vnd.ms-excel'
                self.response.headers['Content-Disposition'] = 'attachment; filename="' + filename_xls + '"'
                self.response.out.write(xls_reports)
            else:
                promo_codes, next_cursor, more = queryset.fetch_page(100, start_cursor=cursor)
                template_values = {'promo_codes': promo_codes,
                        'theaterorg': theaterorg, 'theater': theater,
                        'next_cursor': next_cursor, 'more': more,
                        'date_from': date_from, 'date_to': date_to, 'users': users,
                        'get_theater_name_and_code': get_theater_name_and_code}
                template = JINJA_ENVIRONMENT.get_template('templates/admin_panel/admin_panel.html')
                self.response.write(template.render(template_values))
        except BadRequestError, (instance):
            response_object = {'return': {'code': instance.code, 'message': instance.msg}}
            self.error(instance.code)
            self.response.out.write(instance.msg)

    def export_to_xls(self, queryset):
        row_counter = 0
        booked_seat_counts = []
        booked_amounts = []
        workbook = xlwt.Workbook(encoding='utf-8')
        worksheet = workbook.add_sheet("Sheet 1")
        headers_font_style = xlwt.XFStyle()
        headers_font_style.font.bold = True
        font_style = xlwt.XFStyle()
        font_style.font.bold = False

        worksheet.write(0, 0, 'CLAIM CODE', headers_font_style)
        worksheet.write(0, 1, 'DATE REDEEMED', headers_font_style)
        worksheet.write(0, 2, 'THEATER', headers_font_style)
        worksheet.write(0, 3, 'MOVIE', headers_font_style)
        worksheet.write(0, 4, 'SCHEDULE', headers_font_style)
        worksheet.write(0, 5, 'CINEMA', headers_font_style)
        worksheet.write(0, 6, 'BOOKED SEATS', headers_font_style)
        worksheet.write(0, 7, 'BOOKED AMOUNT', headers_font_style)

        for claim_code in queryset:
            row_counter += 1
            booked_seat_counts.append(claim_code.booked_seat_count)
            booked_amounts.append(claim_code.get_booked_total_amount())

            worksheet.write(row_counter, 0, claim_code.promo_code, font_style)
            worksheet.write(row_counter, 1, claim_code.redeemed_on.strftime('%m/%d/%Y %I:%M %p'), font_style)

            if claim_code.booking_details:
                theater_name = ''
                schedule = ''
                booked_seats = ''
                movie = claim_code.booking_details.movie
                cinema = 'Cinema ' + claim_code.booking_details.cinema
                booked_amount = claim_code.get_booked_total_amount()

                if claim_code.booking_details.theater:
                    theater_name = claim_code.booking_details.theater.get().name
                else:
                    theater_name = get_theater_name_and_code(
                            claim_code.booking_details.theater_id,
                            claim_code.booking_details.theaterorg_id)[0]

                if claim_code.booking_details.schedule:
                    schedule = claim_code.booking_details.schedule.strftime('%m/%d/%Y %I:%M %p')

                if claim_code.booking_details.booked_seats:
                    booked_seats = ','.join(claim_code.booking_details.booked_seats)

                if claim_code.booking_details.booked_seat_count:
                    booked_seats = booked_seats + ' (' + str(claim_code.booking_details.booked_seat_count) + ')'

                worksheet.write(row_counter, 2, theater_name, font_style)
                worksheet.write(row_counter, 3, movie, font_style)
                worksheet.write(row_counter, 4, schedule, font_style)
                worksheet.write(row_counter, 5, cinema, font_style)
                worksheet.write(row_counter, 6, booked_seats, font_style)
                worksheet.write(row_counter, 7, booked_amount, font_style)

        row_counter += 2
        total_booked_seats = sum(booked_seat_counts)
        total_booked_seats = "{:,}".format(int(total_booked_seats))
        total_booked_amount = sum(booked_amounts)
        total_booked_amount = "{:,}".format(int(total_booked_amount))

        worksheet.write(row_counter, 6, 'TOTAL SEATS', headers_font_style)
        worksheet.write(row_counter+1, 6, total_booked_seats, font_style)
        worksheet.write(row_counter, 7, 'TOTAL AMOUNT', headers_font_style)
        worksheet.write(row_counter+1, 7, total_booked_amount, font_style)

        xls_reports = StringIO.StringIO()
        workbook.save(xls_reports)

        return xls_reports.getvalue()


class BankDepositPromoCodesHandler(webapp2.RequestHandler):
    def get(self):
        cursor = ndb.Cursor(urlsafe=self.request.get('cursor'))
        promo_code = self.request.get("promo_code")
        status = 'available'

        try:
            if self.request.get('status'):
                status = self.request.get('status')

            if status == 'search_by':
                log.info('Bank Deposit: Fetching with Claim Code...')
                if promo_code:
                    queryset = PromoCode.query(ndb.AND(PromoCode.is_bank_deposit == True,
                            PromoCode.promo_code >= promo_code.upper(),
                            PromoCode.promo_code < unicode(promo_code.upper()) + u'\ufffd')).order(PromoCode.promo_code, PromoCode.key)
                else:
                    queryset = PromoCode.query(PromoCode.is_bank_deposit == True)
            else:
                if status == 'available':
                    log.info('Bank Deposit: Fetching Available Claim Codes...')
                    queryset = PromoCode.query(ndb.AND(PromoCode.is_bank_deposit == True,
                            PromoCode.redeemed_on == None,PromoCode.is_expired == False,
                            ndb.OR(PromoCode.expired_date > asia_manila_timezone(datetime.datetime.now()),
                                    PromoCode.expired_date <= None))).order(-PromoCode.expired_date, PromoCode.key)
                elif status == 'redeemed':
                    log.info('Bank Deposit: Fetching Redeemed Claim Codes...')
                    queryset = PromoCode.query(PromoCode.is_bank_deposit == True,
                            PromoCode.redeemed_on > None).order(-PromoCode.redeemed_on, PromoCode.key)
                elif status == 'expired':
                    log.info('Bank Deposit: Fetching Expired Claim Codes...')
                    queryset = PromoCode.query(ndb.AND(PromoCode.is_bank_deposit == True,
                            ndb.OR(PromoCode.is_expired == True,
                                    ndb.AND(PromoCode.expired_date <= asia_manila_timezone(datetime.datetime.now()),
                                            PromoCode.expired_date > None,
                                            PromoCode.redeemed_on == None)))).order(-PromoCode.expired_date, PromoCode.key)
                else:
                    queryset = PromoCode.query(PromoCode.is_bank_deposit == True)

            promo_codes, next_cursor, more = queryset.fetch_page(100, start_cursor=cursor)

            template_values = {'promo_codes': promo_codes, 'next_cursor': next_cursor,
                        'more': more, 'status':  status, 'promo_code': promo_code,
                        'users': users, 'get_theaterorg_name': get_theaterorg_name}
            template = JINJA_ENVIRONMENT.get_template('templates/admin_panel/bank_deposit_promo_codes.html')
            self.response.write(template.render(template_values))
        except BadRequestError, (instance):
            response_object = {'return': {'code': instance.code, 'message': instance.msg}}
            self.response.out.write(json.dumps(response_object))


class CountAvailableRedeemedExpiredHandler(webapp2.RequestHandler):
    def get(self):
        try:
            count_available = 0
            count_redeemed = 0
            count_expired = 0
            count_invalid = 0
            filter_by = self.request.get('filter_by')
            filter_value = self.request.get('filter_value')

            if filter_by == 'promo_code':
                log.debug("CountAvailableRedeemedExpiredHandler, GET, filter_by, promo_code: %s..." % filter_value)

                claimcodes_queryset = PromoCode.query(PromoCode.promo_code==filter_value.upper())
            elif filter_by == 'promo_name':
                log.debug("CountAvailableRedeemedExpiredHandler, GET, filter_by, promo_name: %s..." % filter_value)

                filter_value_list = [filter_value, filter_value.lower(), filter_value.title()] if filter_value else []
                claimcodes_queryset = PromoCode.query(PromoCode.promo_name.IN(filter_value_list)).order(PromoCode.key)
            elif filter_by == 'batch_code':
                log.debug("CountAvailableRedeemedExpiredHandler, GET, filter_by, batch_code: %s..." % filter_value)

                claimcodes_queryset = PromoCode.query(PromoCode.batch_code==filter_value.upper())
            else:
                log.debug("CountAvailableRedeemedExpiredHandler, GET, all...")

                claimcodes_queryset = PromoCode.query()

            for claimcode in claimcodes_queryset:
                if claimcode.is_redeemed:
                    count_redeemed += 1
                elif claimcode.is_expired:
                    count_expired += 1
                elif claimcode.is_invalid:
                    count_invalid += 1
                elif claimcode.expired_date and claimcode.expired_date <= asia_manila_timezone(datetime.datetime.now()):
                    count_expired += 1
                else:
                    count_available += 1

            response_object = {'return': {'code': 200, 'message': 'Successfully counted Available, Redeemed, Expired, and Invalid Claim Codes.',
                    'count_available': count_available, 'count_redeemed': count_redeemed, 'count_expired': count_expired,
                    'count_invalid': count_invalid, 'total_count': claimcodes_queryset.count()}}
            self.response.out.write(json.dumps(response_object))
        except BadRequestError, instance:
            log.warn("ERROR!, CountAvailableRedeemedExpiredHandler, GET, BadRequestError...")

            results = {'return': {'code': instance.code, 'message': DEFAULT_ERROR_MESSAGE_ADMIN}}

            self.error(instance.code)
            self.response.out.write(json.dumps(results))
        except Exception, e:
            log.warn("ERROR!, CountAvailableRedeemedExpiredHandler, GET...")
            log.error(e)

            results = {'return': {'code': 500, 'message': DEFAULT_ERROR_MESSAGE_ADMIN}}

            self.error(500)
            self.response.out.write(json.dumps(results))


class PromoCodesComputationHandler(webapp2.RequestHandler):
    def get(self, theaterorg_url_parameter):
        theater_url_parameter = self.request.get('theater')
        current_user = users.get_current_user()

        try:
            today = datetime.datetime.now()
            booked_seat_counts = []
            booked_amounts = []
            date_from = None
            date_to = None

            theaterorg = TheaterOrganization.query(TheaterOrganization.url_parameter == theaterorg_url_parameter).get()
            theater = Theater.query(Theater.url_parameter == theater_url_parameter).get()

            if theaterorg is None and not users.is_current_user_admin():
                raise BadRequestError("Current logged in user %s is not authorized to view this page." % current_user.email(), 401)
            elif theaterorg is None:
                raise BadRequestError("Theater Organization Not Found!", 404)

            if theater is None and current_user.email() not in theaterorg.allowed_users and not users.is_current_user_admin():
                raise BadRequestError("Current logged in user %s is not authorized to view this page." % current_user.email(), 401)

            if 'from' not in self.request.params and 'to' not in self.request.params:
                queryset = PromoCode.query(PromoCode.booking_details.theaterorg_id == theaterorg.gmovies_theaterorg_id).order(-PromoCode.redeemed_on, PromoCode.key)
            else:
                if not self.request.get('from') and not self.request.get('to'):
                    queryset = PromoCode.query(PromoCode.booking_details.theaterorg_id == theaterorg.gmovies_theaterorg_id).order(-PromoCode.redeemed_on, PromoCode.key)
                else:
                    if self.request.get('from'):
                        date_from = parse_date(self.request.get('from') + ' 00:00:00')
                    else:
                        date_from = parse_date(str(today.date()) + ' 00:00:00')

                    if self.request.get('to'):
                        date_to = parse_date(self.request.get('to') + ' 23:59:59')
                    else:
                        date_to = parse_date(str(today.date()) + ' 23:59:59')

                    if date_from is None or date_to is None:
                        raise BadRequestError("Invalid Parameters.", 400)

                    queryset = PromoCode.query(
                            PromoCode.booking_details.theaterorg_id == theaterorg.gmovies_theaterorg_id,
                            PromoCode.redeemed_on >= date_from,
                            PromoCode.redeemed_on <= date_to).order(-PromoCode.redeemed_on, PromoCode.key)

            if theater_url_parameter:
                if theater:
                    if (current_user.email() not in theater.allowed_users and
                            current_user.email() not in theaterorg.allowed_users and
                            not users.is_current_user_admin()):
                        raise BadRequestError("Current logged in user %s is not authorized to view this page." % current_user.email(), 401)

                    queryset = queryset.filter(PromoCode.booking_details.theater_id == theater.gmovies_theater_id)
                else:
                    raise BadRequestError("Theater Not Found!", 404)

            try:
                for promo_code in queryset:
                    booked_seat_counts.append(promo_code.booked_seat_count)
                    booked_amounts.append(promo_code.get_booked_total_amount())
            except DeadlineExceededError, e:
                log.info(e)
                raise BadRequestError("Unable to calculate total amount and seat.", 400)
            except Exception, e:
                log.info(e)
                raise BadRequestError("Unable to calculate total amount and seat.", 400)

            total_booked_seats = sum(booked_seat_counts)
            total_booked_seats = "{:,}".format(int(total_booked_seats))
            total_booked_amount = sum(booked_amounts)
            total_booked_amount = "{:,}".format(int(total_booked_amount))

            response_object = {"return": {"code": 200,
                    "message": "Successfully calculated total amount and seat.",
                    "total_booked_seats": str(total_booked_seats),
                    "total_booked_amount": str(total_booked_amount)}}
            self.response.out.write(json.dumps(response_object))
        except BadRequestError, (instance):
            response_object = {'return': {'code': instance.code, 'message': instance.msg}}
            self.response.out.write(json.dumps(response_object))


class UpdateSchemaHandler(webapp2.RequestHandler):
    def get(self):
        deferred.defer(update_schema)
        self.response.out.write('GET, chema migration successfully initiated.')

    def post(self):
        try:
            seat_count = None
            price_per_seat = None
            expired_date = None
            status = 'available'

            if 'filter_by' not in self.request.POST:
                raise BadRequestError("Missing parameter, filter_by", 400)

            if 'filter_value' not in self.request.POST:
                raise BadRequestError("Missing parameter, filter_value", 400)

            filter_by = self.request.POST['filter_by']
            filter_value = self.request.POST['filter_value']

            if 'status' in self.request.POST:
                status = self.request.POST['status'].lower()

            if 'seat_count' in self.request.POST:
                seat_count = self.request.POST['seat_count']

            if 'price_per_seat' in self.request.POST:
                price_per_seat = self.request.POST['price_per_seat']

            if 'expired_date' in self.request.POST:
                expired_date = self.request.POST['expired_date']
                expired_date = parse_date(expired_date)

            log.info("filter_by: %s", filter_by)
            log.info("filter_value: %s", filter_value)
            log.info("status: %s", status)
            log.info("seat_count: %s", seat_count)
            log.info("price_per_seat: %s", price_per_seat)
            log.info("expired_date: %s", str(expired_date))

            deferred.defer(update_schema_post, (filter_by, filter_value),
                    seat_count=seat_count, price_per_seat=price_per_seat,
                    expired_date=expired_date, status=status)

            self.response.out.write('POST, schema migration successfully initiated.')
        except BadRequestError, (instance):
            response_object = {'return': {'code': instance.code, 'message': instance.msg}}
            self.response.out.write(json.dumps(response_object))

class PromoCodesBatchUpdateHandler(webapp2.RequestHandler):
    def get(self):
        # template_values = {'query_set_page': query_set_page, 'next_cursor': next_cursor, 'more': more, 'search_value': search_value, 'promo_code_filter': promo_code_filter, 'return_query_set': return_query_set['pages'][page_num], 'n': page_num + 1, 'filtered': filtered}
        template_values = {}
        template = JINJA_ENVIRONMENT.get_template('templates/promo_codes_batch_update.html')
        self.response.write(template.render(template_values))


# Theater Organizations Integration
# One time execution only, just to fill-out the missing data needed for the
# previously redeemed claim codes, default theaterorg is Ayala Malls.
class UpdateSchemaWithTheaterOrganization(webapp2.RequestHandler):
    def get(self):
        deferred.defer(update_schema_with_theaterorg)
        self.response.out.write('Integrate Theater Organization: Schema migration successfully initiated.')

def update_schema(cursor='', number_updated=0):
    cursor = ndb.Cursor(urlsafe=cursor)
    queryset = PromoCode.query()
    queryset_batch, next_cursor, more = queryset.fetch_page(100, start_cursor=cursor)

    for qs in queryset_batch:
        if not qs.seat_count or qs.seat_count is None:
            qs.seat_count = DEFAULT_SEAT_COUNT

        if not qs.price_per_seat or qs.price_per_seat is None:
            qs.price_per_seat = DEFAULT_PRICE_PER_SEAT

        if qs.redeemed_on:
            qs.is_redeemed = True

        if qs.promo_code[:7] == "GMOVTRY":
            qs.is_invalid = True

    ndb.put_multi(queryset_batch)
    number_updated += len(queryset_batch)

    if more:
        log.info('Put %d entities to Datastore for a total of %d' % (len(queryset_batch), number_updated))
        deferred.defer(update_schema, cursor=next_cursor.urlsafe(), number_updated=number_updated)
    else:
        log.info('UpdateSchema complete with %d updates' % number_updated)


def update_schema_post(filter_by, cursor='', number_updated=0,
            seat_count=None, price_per_seat=None, expired_date=None,
            status='available'):
    try:
        if filter_by[0] == 'promo_name':
            if status == 'available':
                log.info("Available queryset...")
                queryset = PromoCode.query(ndb.AND(PromoCode.promo_name == filter_by[1],
                        PromoCode.redeemed_on == None, PromoCode.is_expired == False,
                        ndb.OR(PromoCode.expired_date > asia_manila_timezone(datetime.datetime.now()),
                                PromoCode.expired_date <= None))).order(-PromoCode.expired_date, PromoCode.key)
            elif status == 'redeemed':
                log.info("Redemmed queryset...")
                queryset = PromoCode.query(PromoCode.promo_name == filter_by[1],
                        PromoCode.redeemed_on > None).order(-PromoCode.redeemed_on, PromoCode.key)
            elif status == 'expired':
                log.info("Expired queryset...")
                queryset = PromoCode.query(ndb.AND(PromoCode.promo_name == filter_by[1],
                        ndb.OR(PromoCode.is_expired == True,
                                ndb.AND(PromoCode.expired_date <= asia_manila_timezone(datetime.datetime.now()),
                                        PromoCode.expired_date > None,
                                        PromoCode.redeemed_on == None)))).order(-PromoCode.expired_date, PromoCode.key)
            else:
                log.info("All queryset...")
                queryset = PromoCode.query(PromoCode.promo_name == filter_by[1])
        else:
            raise BadRequestError("Failed to run update_schema_post", 400)

        cursor = ndb.Cursor(urlsafe=cursor)
        queryset_batch, next_cursor, more = queryset.fetch_page(100, start_cursor=cursor)

        for qs in queryset_batch:
            if seat_count:
                qs.seat_count = int(seat_count)

            if price_per_seat:
                qs.price_per_seat = Decimal(price_per_seat)

            if expired_date:
                qs.expired_date = expired_date

        ndb.put_multi(queryset_batch)
        number_updated += len(queryset_batch)

        if more:
            log.info('Put %d entities to Datastore for a total of %d' % (len(queryset_batch), number_updated))
            deferred.defer(update_schema_post, filter_by,
                    cursor=next_cursor.urlsafe(), number_updated=number_updated,
                    seat_count=seat_count, price_per_seat=price_per_seat,
                    expired_date=expired_date, status=status)
        else:
            log.info('UpdateSchema complete with %d updates' % number_updated)
    except BadRequestError, (instance):
        log.error("error_code: %s, error_message: %s", instance.code, instance.msg)
    except Exception, e:
        log.error("Error, failed to run update_schema_post...")
        log.error(e)


def update_schema_with_theaterorg(cursor='', number_updated=0):
    cursor = ndb.Cursor(urlsafe=cursor)
    queryset = PromoCode.query(PromoCode.redeemed_on > None).order(PromoCode.redeemed_on, PromoCode.key)
    queryset_batch, next_cursor, more = queryset.fetch_page(100, start_cursor=cursor)

    for qs in queryset_batch:
        if qs.redeemed_on is not None:
            if not qs.theaterorg_id or qs.theaterorg_id is None:
                qs.theaterorg_id = DEFAULT_THEATERORG

            if not qs.booked_seat_count or qs.booked_seat_count is None:
                if qs.seat_count:
                    qs.booked_seat_count = qs.seat_count
                else:
                    qs.booked_seat_count = DEFAULT_SEAT_COUNT

            if not qs.booked_price_per_seat or qs.booked_price_per_seat is None:
                if qs.price_per_seat:
                    qs.booked_price_per_seat = qs.price_per_seat
                else:
                    qs.booked_price_per_seat = Decimal(DEFAULT_PRICE_PER_SEAT)
        else:
            qs.theaterorg_id = None
            qs.booked_seat_count = None
            qs.booked_price_per_seat = None

    ndb.put_multi(queryset_batch)
    number_updated += len(queryset_batch)

    if more:
        log.info('Integrate Theater Organization: Put %d entities to Datastore for a total of %d' % (len(queryset_batch), number_updated))
        deferred.defer(update_schema_with_theaterorg,
                    cursor=next_cursor.urlsafe(),
                    number_updated=number_updated)
    else:
        log.info('Integrate Theater Organization: UpdateSchema complete with %d updates' % number_updated)

def parse_date(date):
    try:
        date = datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
    except ValueError:
        try:
            date = datetime.datetime.strptime(date, '%Y-%m-%d %H:%M')
        except ValueError:
            date = None

    return date


def get_theaterorg_name(theaterorg_id):
    theaterorg = TheaterOrganization.query(TheaterOrganization.gmovies_theaterorg_id == theaterorg_id).get()

    if theaterorg is not None:
        return theaterorg.name

    return None


def get_theater_name_and_code(theater_id, theaterorg_id):
    theaterorg = TheaterOrganization.query(TheaterOrganization.gmovies_theaterorg_id == theaterorg_id).get()

    if theaterorg:
        theater = Theater.query(Theater.gmovies_theater_id == theater_id).get()

        if theater:
            return theater.name, theater.theater_code

    return None, None


class MpassAccountIndexHandler(webapp2.RequestHandler):
    def get(self):
        try:
            wallets = MpassAccounts.query().fetch()

            template_values = {'wallets': wallets}
            template = JINJA_ENVIRONMENT.get_template('templates/mpass_accounts/mpass_index.html')
            self.response.write(template.render(template_values))
        except BadRequestError, instance:
            log.warn("ERROR!, MpassAccountIndexHandler, GET, BadRequestError...")

            self.error(instance.code)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)
        except Exception, e:
            log.warn("ERROR!, MpassAccountIndexHandler, GET...")
            log.error(e)

            self.error(500)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)

    def post(self):
        try:
            mpass_id = self.request.POST['wallet_id']

            mpass_key = ndb.Key(MpassAccounts, int(mpass_id))
            mpass = mpass_key.get()
            mpass.status = 0
            mpass.deleted_at = datetime.datetime.now()
            mpass.deleted_by = users.get_current_user().email()
            mpass.updated_by = users.get_current_user().email()
            mpass.put()
            return webapp2.redirect('/admin/0/mpass_accounts')
        except BadRequestError, instance:
            log.warn("ERROR!, MpassAccountIndexHandler, POST, BadRequestError...")

            self.error(instance.code)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)
        except Exception, e:
            log.warn("ERROR!, MpassAccountIndexHandler, POST...")
            log.error(e)

            self.error(500)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)

class MpassAccountCreateHandler(webapp2.RequestHandler):
    def get(self):
        try:
            template_values = {}
            template = JINJA_ENVIRONMENT.get_template('templates/mpass_accounts/mpass_create.html')
            self.response.write(template.render(template_values))
        except BadRequestError, instance:
            log.warn("ERROR!, MpassAccountCreateHandler, GET, BadRequestError...")

            self.error(instance.code)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)
        except Exception, e:
            log.warn("ERROR!, MpassAccountCreateHandler, GET...")
            log.error(e)

            self.error(500)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)

    def post(self):
        try:
            name = self.request.POST['name']
            username = self.request.POST['username']
            password = self.request.POST['password']

            if not name:
                raise BadRequestError('Name Required!', 404)

            if not username:
                raise BadRequestError('Username Required!', 404)
            
            if not password:
                raise BadRequestError('Password Required!', 404)

            mpass = MpassAccounts()
            mpass.name = name
            mpass.username = username
            mpass.password = password

            mpass_key = mpass.put()

            return webapp2.redirect('/admin/0/mpass_accounts/%s' % mpass_key.id())
        except BadRequestError, instance:
            log.warn("ERROR!, MpassAccountCreateHandler, POST, BadRequestError...")

            self.error(instance.code)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)
        except Exception, e:
            log.warn("ERROR!, MpassAccountCreateHandler, POST...")
            log.error(e)

            self.error(500)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)


class MpassAccountShowUpdateHandler(webapp2.RequestHandler):
    def get(self, mpass_id):
        try:
            mpass_key = ndb.Key(MpassAccounts, int(mpass_id))
            mpass = mpass_key.get()
            if not mpass:
                raise BadRequestError('MPass Account Not Found!', 404)

            template_values = {'mpass': mpass}
            template = JINJA_ENVIRONMENT.get_template('templates/mpass_accounts/mpass_show.html')
            self.response.write(template.render(template_values))
        except BadRequestError, instance:
            log.warn("ERROR!, MpassAccountShowUpdateHandler, GET, BadRequestError...")

            self.error(instance.code)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)
        except Exception, e:
            log.warn("ERROR!, MpassAccountShowUpdateHandler, GET...")
            log.error(e)

            self.error(500)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)

    def post(self, mpass_id):
        try:
            name = self.request.POST['name']
            username = self.request.POST['username']
            password = self.request.POST['password']
                    
            mpass_key = ndb.Key(MpassAccounts, int(mpass_id))
            mpass = mpass_key.get()

            if not name:
                raise BadRequestError('Name Required!', 404)

            if not username:
                raise BadRequestError('Username Required!', 404)
            
            if not password:
                raise BadRequestError('Password Required!', 404)
            
            mpass.name = name
            mpass.username = username
            mpass.password = password
            mpass.updated_by = users.get_current_user().email()
            mpass.put()

            return webapp2.redirect('/admin/0/mpass_accounts/%s' % mpass_id)
        except BadRequestError, instance:
            log.warn("ERROR!, MpassAccountShowUpdateHandler, POST, BadRequestError...")

            self.error(instance.code)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)
        except Exception, e:
            log.warn("ERROR!, MpassAccountShowUpdateHandler, POST...")
            log.error(e)

            self.error(500)
            self.response.out.write(DEFAULT_ERROR_MESSAGE_ADMIN)


app = webapp2.WSGIApplication([webapp2.Route('/update_schema', UpdateSchemaHandler),
            webapp2.Route('/admin/0/gmovies-promos', PromoCategoryIndexHandler),
            webapp2.Route('/admin/0/bank_deposit', BankDepositPromoCodesHandler),
            webapp2.Route('/admin/0/gmovies-promos/<promo_category_id>', PromoCategoryShowHandler),
            webapp2.Route('/admin/0/promo_codes', PromoCodesHandler),
            webapp2.Route('/admin/0/promo_codes/count', CountAvailableRedeemedExpiredHandler),
            webapp2.Route('/admin/0/movies', MovieIndexHandler),
            webapp2.Route('/admin/0/movies/create', MovieCreateHandler),
            webapp2.Route('/admin/0/movies/<movie_id>', MovieShowUpdateHandler),
            webapp2.Route('/admin/0/theaterorgs', TheaterOrganizationIndexHandler),
            webapp2.Route('/admin/0/theaterorgs/create', TheaterOrganizationCreateHandler),
            webapp2.Route('/admin/0/theaterorgs/<theaterorg_id>', TheaterOrganizationShowUpdateHandler),
            webapp2.Route('/admin/0/theaterorgs/<theaterorg_id>/theaters/create', TheaterCreateHandler),
            webapp2.Route('/admin/0/theaterorgs/<theaterorg_id>/theaters/<theater_id>', TheaterShowUpdateHandler),
            webapp2.Route('/admin/0/promo_codes/batch_update', PromoCodesBatchUpdateHandler),
            webapp2.Route('/admin/0/mpass_accounts', MpassAccountIndexHandler),
            webapp2.Route('/admin/0/mpass_accounts/create', MpassAccountCreateHandler),
            webapp2.Route('/admin/0/mpass_accounts/<mpass_id>', MpassAccountShowUpdateHandler),
            webapp2.Route('/admin/1', MainHandler),
            webapp2.Route('/admin/1/promo_codes/redeemed/<theaterorg_url_parameter>', AdminPanelHandler),
            webapp2.Route('/admin/1/promo_codes/redeemed/<theaterorg_url_parameter>/compute', PromoCodesComputationHandler),
            webapp2.Route('/admin/1/promo_codes/bank_deposit', BankDepositPromoCodesHandler)],
        debug=True)
