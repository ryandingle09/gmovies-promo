#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Python25
# import simplejson as json
# from google.appengine.ext import webapp
# from google.appengine.ext.webapp import util

# Python27
import webapp2
import json

# import base64
import re
import urllib
import string
import random
import datetime
# from Crypto.Cipher import AES

from google.appengine.api import mail
from google.appengine.api import urlfetch

from models import *


class BadRequestError(Exception):
    def __init__(self, param, code):
        super(Exception, self).__init__()
        self.msg = param
        self.code = code

class DumpHandler(webapp2.RequestHandler):
    def get(self):

        try:
            # q = PromoCode.all() # This line of code is using google.appengine.ext.db library
            # q.order("-email")
            q = PromoCode.query().order(-PromoCode.email)
            # for p in q.run():
            #     print "%s, %s, %s, %s" % (p.email, p.msisdn, p.promo_code, p.redeemed_on)

        except BadRequestError, (instance):
            self.error(instance.code)
            self.response.out.write(instance.msg)


"""" Old Code: Python25
def main():
    application = webapp.WSGIApplication([('/dump', DumpHandler)],
                                         debug=True)
    util.run_wsgi_app(application)


if __name__ == '__main__':
    main()"""


app = webapp2.WSGIApplication([('/dump', DumpHandler)], debug=True)
