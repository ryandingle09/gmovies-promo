import json
import logging
import webapp2
import traceback
from google.appengine.ext import deferred, ndb
from models import (PromoCode, PromoCategory, MpassAccounts)

log = logging.getLogger(__name__)

class BadRequestError(Exception):
    def __init__(self, param, code):
        super(Exception, self).__init__()
        self.msg = param
        self.code = code


class MpassHandler(webapp2.RequestHandler):
    def get(self):

        try:
            claim_code = self.request.headers["CLAIMCODE-MPASS"] if self.request.headers["CLAIMCODE-MPASS"] else None

            if claim_code is not None:
                p = PromoCode.query(PromoCode.promo_code==claim_code).get()
            
                if not p:
                    data = {'status': 0, 'message': 'Invalid claim code'}
                
                pr = PromoCategory.query(PromoCategory.name==p.promo_name).get()

                if pr.mpass_wallet or pr.mpass_wallet is not None:
                    wallet = MpassAccounts.get_by_id(int(pr.mpass_wallet))

                    data = {
                        'status': 1,
                        'data': {
                            'username': wallet.username,
                            'password': wallet.password
                        }
                    }

                else:
                    data = {'status': 0, 'message': 'No mpass wallet found'}

            else:
                data = {'status': 0, 'message': 'Invalid claim code'}

            log.debug("wallet: %s" % (data))
            self.response.out.write(str(data))

        except Exception as e:
            log.error(traceback.format_exc(e)) 
            print traceback.format_exc(e)
            self.response.out.write({'status': 0, 'message': 'Invalid authentication'})

app = webapp2.WSGIApplication([('/mpass', MpassHandler)], debug=True)