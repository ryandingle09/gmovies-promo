import calendar
import datetime
import jinja2
import json
import logging
import os
import re
import urllib
import webapp2
import StringIO
import sys
import traceback

sys.path.insert(0, 'libs')
import xlwt

from decimal import Decimal
from operator import itemgetter
from uuid import uuid4 as uuid_gen

from google.appengine.api import users
from google.appengine.ext import deferred, ndb
from google.appengine.runtime import DeadlineExceededError

from models import (asia_manila_timezone, PromoCategory, PromoCode, Theater, TheaterOrganization, CliamCodeApiSettings)
from util import DEFAULT_ERROR_MESSAGE_ADMIN, parse_string_to_datetime, parse_string_to_date, convert_timezone

log = logging.getLogger(__name__)

class BadRequestError(Exception):
    def __init__(self, param, code):
        super(Exception, self).__init__()
        self.msg = param
        self.code = code

class GetClaimCodes(webapp2.RequestHandler):
    def get(self):
        error = 'Unauthorized: Access is denied due to invalid credentials.'
        
        try:
            res = ''
            token = self.request.headers["CLIENT-TOKEN"] if self.request.headers["CLIENT-TOKEN"] else None
            slug = self.request.headers["CLIENT-SLUG"] if self.request.headers["CLIENT-SLUG"] else None
            code = self.request.get('code') if self.request.get('code') else None
            start = self.request.get('start_date') if self.request.get('start_date') else None
            end = self.request.get('end_date') if self.request.get('end_date') else None

            log.debug("header_token: %s" % (token))
            log.debug("header_slug: %s" % (slug))
            log.debug("request_code: %s" % (code))
            log.debug("request_start_date: %s" % (start))
            log.debug("request_end_date: %s" % (end))

            if token is None or slug is None:
                res = 'Unauthorized: Access is denied due to invalid credentials.'
            else:
                codes = CliamCodeApiSettings.query().order(-CliamCodeApiSettings.created_at, CliamCodeApiSettings.key)
                data = codes.filter(CliamCodeApiSettings.slug==slug, CliamCodeApiSettings.token==token)

                if data.get() is None:
                    log.debug("No results found for: token: %s and slug: %s" % (token, slug))
                    res = error
                else:
                    status = data.get().status
                    promos = data.get().promo_name.split(",")

                    claim_codes = []
                    redeemed = []
                    not_redeemed = []
                    
                    if status == 'active':

                        if code is not None:
                            p = PromoCode.query(PromoCode.promo_code==str(code)).get()

                            res = [{"status": "Redeemed" if p.is_redeemed else "Available", "code": str(p.promo_code) }]

                        else:
                            for names in promos:
                                p = PromoCategory.query()
                                p = p.filter(PromoCategory.name==str(names))

                                if p.get() is not None:
                                    c = PromoCode.query()
                                    c = c.filter(PromoCode.promo_name==names)

                                    if start is not None and end is not None:
                                        start = '%s 00:00:00' % (start)
                                        end = '%s 23:59:59' % (end)
                                        
                                        start = convert_timezone(parse_string_to_datetime(start), 8, '-')
                                        end = convert_timezone(parse_string_to_datetime(end), 8, '-')

                                        c = c.filter(PromoCode.redeemed_on >= start).filter(PromoCode.redeemed_on <= end)
                                        c = c.filter(PromoCode.is_redeemed==True)

                                    c = c.fetch()

                                    c_child = []

                                    # for all claim codes
                                    for c2 in c:
                                        childs = {
                                            "voucher_code": str(c2.promo_code),
                                            "status": 'Redeemed' if c2.is_redeemed else 'Available'
                                        }
                                        c_child.append(childs)

                                    claim_codes.append({str(names): c_child})

                                else:
                                    claim_codes.append({str(names): "Promo Name Not Found."})
                            
                            res = claim_codes

            return self.response.out.write(json.dumps(res))
        
        except Exception as e:
            log.error(traceback.format_exc(e))
            print traceback.format_exc(e)

            return self.response.out.write(error)

app = webapp2.WSGIApplication([
    webapp2.Route('/api/v1/get-voucher', GetClaimCodes)
],debug=True)