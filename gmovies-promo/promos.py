import datetime
import json
import logging
import random
import re
import string
import urllib
import webapp2
import jinja2
import os

from decimal import Decimal
from operator import itemgetter

from google.appengine.api import mail, urlfetch
from google.appengine.ext import ndb
from google.appengine.runtime import DeadlineExceededError
from google.appengine.api.app_identity import get_application_id

from models import (asia_manila_timezone, BookingDetails, PromoCode, Promos, Theater,
        TheaterOrganization, DEFAULT_PRICE_PER_SEAT, DEFAULT_SEAT_COUNT)


log = logging.getLogger(__name__)

JINJA_ENVIRONMENT = jinja2.Environment(
            loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
            extensions=['jinja2.ext.autoescape'], autoescape=True)

DEFAULT_NUMBER_OF_PROMO_CODES = 1
SCHEDULE_DATETIME_FORMAT = '%Y-%m-%dT%H:%M:%S'

MAIL_DEFAULT_FROM_TEMPLATE = "GMovies Claim Code <gmovies-noreply@%s.appspotmail.com>" % get_application_id()
BCC_DEFAULT_EMAIL = "Mary Antoniette Real <mreal@yondu.com>, Josh Lemuel Torio <jtorio@yondu.com>, Ryan Dingle <rdingle@yondu.com>"

class BadRequestError(Exception):
    def __init__(self, param, code):
        super(Exception, self).__init__()
        self.msg = param
        self.code = code

class PromosIndexHandler(webapp2.RequestHandler):
    def multikeysort(self, items, columns, functions={}, getter=itemgetter):
        """Sort a list of dictionary objects or objects by multiple keys bidirectionally.

        Keyword Arguments:
        items -- A list of dictionary objects or objects
        columns -- A list of column names to sort by. Use -column to sort in descending order
        functions -- A Dictionary of Column Name -> Functions to normalize or process each column value
        getter -- Default "getter" if column function does not exist
                  operator.itemgetter for Dictionaries
                  operator.attrgetter for Objects
        """
        comparers = []
        for col in columns:
            column = col[1:] if col.startswith('-') else col
            if not column in functions:
                functions[column] = getter(column)
            comparers.append((functions[column], 1 if column == col else -1))
     
        def comparer(left, right):
            for func, polarity in comparers:
                result = cmp(func(left), func(right))
                if result:
                    return polarity * result
            else:
                return 0
        return sorted(items, cmp=comparer)

    def compose(self, inner_func, *outer_funcs):
         """Compose multiple unary functions together into a single unary function"""
         if not outer_funcs:
             return inner_func
         outer_func = compose(*outer_funcs)
         return lambda *args, **kwargs: outer_func(inner_func(*args, **kwargs))


    def get(self):
        # template_values = {'query_set_page': query_set_page, 'next_cursor': next_cursor, 'more': more, 'search_value': search_value, 'promo_code_filter': promo_code_filter, 'return_query_set': return_query_set['pages'][page_num], 'filtered': filtered}
        log.info("Generate Promos Get.")
        search_value = self.request.get("search")
        promo_code_filter = self.request.get("search_by")
        export = self.request.get("export")
        cursor = ndb.Cursor(urlsafe=self.request.get('cursor'))
        page_num = 0

        filtered = True

        sort2 = promo_code_filter
        if self.request.get("n"):
            page_num = int(self.request.get("n"))

        try:
            log.info(promo_code_filter)
            
            if promo_code_filter == "promo_code":
                log.info('Fetching with Claim Code...')
                if search_value:
                    query_set = Promos.query(ndb.AND(Promos.promo_code >= search_value.upper(),
                                    Promos.promo_code < unicode(search_value.upper()) + u'\ufffd')).order(Promos.promo_code, Promos.key)

                else:
                    query_set = Promos.query().order(-Promos.redeemed_on, Promos.key)
            elif promo_code_filter == "promo_name":
                log.info('Fetching with Claim Name...')
                if search_value:
                    query_set = Promos.query(ndb.AND(Promos.promo_name >= search_value.lower(),
                                    Promos.promo_name < unicode(search_value.lower()) + u'\ufffd')).order(Promos.promo_name, Promos.key)
                else:
                    query_set = Promos.query().order(-Promos.redeemed_on, Promos.key)
            elif promo_code_filter == "created_on" or promo_code_filter == "redeemed_on" or promo_code_filter == "expired_date":
                log.info('Fetching with Dates...')
                if search_value:
                    date_from = parse_date(search_value + ' 00:00:00')
                    date_to = parse_date(search_value + ' 23:59:59')
                    if promo_code_filter == "created_on":
                        log.info('Fetching with Date Created...')
                        query_set = Promos.query(Promos.created_on >= date_from,
                                Promos.created_on <= date_to).order(-Promos.created_on, Promos.key)
                    elif promo_code_filter == "redeemed_on":
                        log.info('Fetching with Date Redeemed...')
                        query_set = Promos.query(Promos.redeemed_on >= date_from,
                                Promos.redeemed_on <= date_to).order(-Promos.redeemed_on, Promos.key)
                    else:
                        sort2 = "expired_date"
                        log.info('Fetching with Date Expired...')
                        query_set = Promos.query(Promos.expired_date >= date_from,
                                Promos.expired_date <= date_to).order(-Promos.expired_date, Promos.key)
                else:
                    query_set = Promos.query().order(-Promos.redeemed_on, Promos.key)
            else:
                if promo_code_filter == "available":
                    sort2 = "promo_code"
                    log.info('Fetching Available Claim Codes...')
                    query_set = Promos.query(ndb.AND(Promos.redeemed_on == None, Promos.is_expired == False,
                            ndb.OR(Promos.expired_date > asia_manila_timezone(datetime.datetime.now()),
                                    Promos.expired_date <= None))).order(-Promos.expired_date, Promos.key)
                elif promo_code_filter == "redeemed":
                    sort2 = "promo_code"
                    log.info('Fetching Redeemed Claim Codes...')
                    query_set = Promos.query(Promos.redeemed_on > None).order(-Promos.redeemed_on, Promos.key)
                elif promo_code_filter == "expired":
                    sort2 = "expired_date"
                    log.info('Fetching Expired Claim Codes...')
                    query_set = Promos.query(ndb.OR(Promos.is_expired == True,
                            ndb.AND(Promos.expired_date <= asia_manila_timezone(datetime.datetime.now()),
                                    Promos.expired_date > None, Promos.redeemed_on == None))).order(-Promos.expired_date, Promos.key)
                elif promo_code_filter == "bank_deposit":
                    sort2 = "promo_code"
                    query_set = Promos.query(Promos.is_bank_deposit == True).order(-Promos.redeemed_on, Promos.key)
                elif promo_code_filter == "all":
                    log.info('Fetching All Claim Codes...')
                    sort2 = "promo_code"
                    filtered = False
                    query_set = Promos.query().order(-Promos.redeemed_on,Promos.promo_code)
                else:
                    log.info('Fetching All Claim Codes...')
                    sort2 = "promo_code"
                    filtered = False
                    query_set = Promos.query(Promos.created_on >= datetime.datetime.today().replace().replace(hour=0, minute=0, second=0),
                                Promos.created_on <= datetime.datetime.today().replace().replace(hour=23, minute=59, second=59)).order(Promos.created_on, -Promos.redeemed_on)

            query_set_page, next_cursor, more = query_set.fetch_page(100, start_cursor=cursor)

            data_set = query_set_page
            if filtered:
                data_set = query_set

            query_set_list = []
            for query_set_entity in data_set:
                log.info("{} {} {} {}".format(query_set_entity.key, query_set_entity.promo_code, query_set_entity.created_on, query_set_entity.redeemed_on))
                query_set_to_dict = {}

                query_set_to_dict["promo_code"] = str(query_set_entity.promo_code)

                if query_set_entity.created_on:
                    query_set_to_dict["created_on"] = query_set_entity.created_on.strftime("%m/%d/%Y %I:%M %p")
                else:
                    query_set_to_dict["created_on"] = datetime.datetime(1999, 1, 1).strftime("%m/%d/%Y %I:%M %p")

                if query_set_entity.promo_name:
                    query_set_to_dict["promo_name"] = str(query_set_entity.promo_name.title())
                else:
                    query_set_to_dict["promo_name"] = "N/A"

                if query_set_entity.redeemed_on:
                    query_set_to_dict["redeemed_on"] = query_set_entity.redeemed_on.strftime("%m/%d/%Y %I:%M %p")
                else:
                    query_set_to_dict["redeemed_on"] = datetime.datetime(1999, 1, 1).strftime("%m/%d/%Y %I:%M %p")

                if query_set_entity.seat_count:
                    query_set_to_dict["seat_count"] = str(query_set_entity.seat_count)
                else:
                    query_set_to_dict["seat_count"] = 'N/A'

                if query_set_entity.get_total_amount():
                    query_set_to_dict["total_amount"] = str(query_set_entity.get_total_amount())
                else:
                    query_set_to_dict["total_amount"] = "N/A"

                if query_set_entity.expired_date:
                    query_set_to_dict["expired_date"] = query_set_entity.expired_date.strftime("%m/%d/%Y %I:%M %p")
                else:
                    query_set_to_dict["expired_date"] = datetime.datetime(1999, 1, 1).strftime("%m/%d/%Y %I:%M %p")
                
                query_set_to_dict["status"] = query_set_entity.get_status()

                if query_set_entity.promo_id:
                    log.info("PROMO ID: {} {}".format(query_set_entity.promo_id, type(query_set_entity.promo_id)))
                    query_set_to_dict["promo_id"] = []
                    for promo_id in query_set_entity.promo_id:
                        if str(promo_id):
                            query_set_to_dict["promo_id"].append(str(promo_id))
                    query_set_to_dict["promo_id"] = query_set_to_dict["promo_id"] if query_set_to_dict["promo_id"] else "N/A"
                else:
                    query_set_to_dict["promo_id"] = "N/A"

                query_set_list.append(query_set_to_dict)

            if sort2:
                if sort2 not in ['redeemed_on']:
                    if sort2 == "expired_date":
                        query_set_list = self.multikeysort(query_set_list, ['-expired_date', "promo_code"])
                    else:
                        query_set_list = self.multikeysort(query_set_list, ['-redeemed_on', sort2])

            return_query_set = {}
            return_query_set['data'] = query_set_list
            return_query_set['pages'] = []

            promo_per_page = []
            limit_per_page = 100
            counter = 0
            for promo in query_set_list:
                counter += 1

                promo_per_page.append(promo)

                if counter == limit_per_page:
                    return_query_set['pages'].append(promo_per_page)

                    promo_per_page = []
                    counter = 0
            return_query_set['pages'].append(promo_per_page)

            template_values = {'query_set_page': query_set_page, 
                'next_cursor': next_cursor, 
                'more': more, 
                'search_value': search_value, 
                'promo_code_filter': promo_code_filter, 
                'return_query_set': return_query_set['pages'][page_num], 
                'filtered': filtered}
            
            if page_num + 1 > len((return_query_set['pages'])):
                template_values["n"] = page_num + 1
            
            template = JINJA_ENVIRONMENT.get_template('templates/promos/promos.html')
            self.response.write(template.render(template_values))
        except BadRequestError, (instance):
            response_object = {'return': {'code': instance.code, 'message': instance.msg}}
            self.response.out.write(json.dumps(response_object))

class GeneratePromoCodeHandler(webapp2.RequestHandler):
    def get(self):
        template_values =  {}
        template = JINJA_ENVIRONMENT.get_template('templates/promos/generate.html')
        self.response.write(template.render(template_values))

    def post(self):
        log.info("Generate Promos Post.")

        expiration_date = None
        generated_promo_codes = []
        number_of_promo_codes = DEFAULT_NUMBER_OF_PROMO_CODES
        email = self.request.get("email")
        batch_code = self.request.get("batch_code")
        seat_count = self.request.get("seat_count")
        price_per_seat = self.request.get("price_per_seat")
        expired_date = self.request.get("expired_date")
        expired_time = self.request.get("expired_time")
        promo_name = self.request.get("promo_name").lower() if self.request.get("promo_name") else None
        issued_on = asia_manila_timezone(datetime.datetime.now())
        promo_id = self.request.get("promo_id").split(";") if self.request.get("promo_id") else []

        try:
            number_of_promo_codes = self.request.get("number_of_promo_per_batch")

            if batch_code:
                if re.match('^[A-Z0-9]{7}$', batch_code) is None:
                    raise BadRequestError("Error in Batch Code format.", 400)

            if number_of_promo_codes == "" or not number_of_promo_codes or number_of_promo_codes is None or re.match('^[0-9]+$', number_of_promo_codes) is None:
                raise BadRequestError("Error in No. of Claim Codes per Batch. Invalid format.", 400)

            if int(number_of_promo_codes) <= 0 or int(number_of_promo_codes) > 500:
                raise BadRequestError("Error in No. of Claim Codes per Batch. Value must be greater than 0 and less than or equal to 500.", 400)
            
            if email == "" or not email or email is None:
                raise BadRequestError("Error in email format.", 400)

            if seat_count == "" or not seat_count or seat_count is None:
                seat_count = DEFAULT_SEAT_COUNT

            if price_per_seat == "" or not price_per_seat or price_per_seat is None:
                price_per_seat = DEFAULT_PRICE_PER_SEAT

            if expired_date:
                if expired_time == "" or not expired_time or expired_time is None:
                    expired_time = '23:59'

                expiration_date = parse_expiration_date(expired_date, expired_time)

            try:
                log.info("BATCH CODE: {}".format(batch_code))
                for batch in range(0, int(number_of_promo_codes)):
                    log.info("PROMO ID: {}".format(promo_id))
                    promo_code = create_promo_code(batch_code)
                    PromoModel = Promos()
                    PromoModel.promo_code = promo_code
                    PromoModel.issued_on = issued_on
                    PromoModel.batch_code = batch_code
                    PromoModel.seat_count = int(seat_count)
                    PromoModel.price_per_seat = Decimal(price_per_seat)
                    PromoModel.expired_date = expiration_date
                    PromoModel.promo_name = promo_name
                    PromoModel.promo_id = promo_id
                    PromoModel.put()
                    generated_promo_codes.append(promo_code)

                generation_msg = "Successfully generated claim codes."
            except DeadlineExceededError, e:
                log.info(e)
                generation_msg = "Unable to finish claim code generation."
                pass
            except Exception, e:
                log.info(e)
                generation_msg = "Unable to finish claim code generation."
                pass

            log.info("Claim Code Generation: %s" % generation_msg)

            mail_body = create_mail_body(generated_promo_codes, expiration_date, promo_name)
            mail.send_mail(sender=MAIL_DEFAULT_FROM_TEMPLATE, to=email,
                    bcc=BCC_DEFAULT_EMAIL,
                    subject="GMovies Claim Code Generation", body=mail_body)

            response_object = {"return": {"code": 200, "message": generation_msg, "promo_codes": generated_promo_codes}}
            self.response.out.write(json.dumps(response_object))
        except BadRequestError, (instance):
            response_object = {"return": {"code": instance.code, "message": instance.msg}}
            self.response.out.write(json.dumps(response_object))

class RedeemHandler(webapp2.RequestHandler):
    def get(self):
        promo_code = self.request.get("promo_code")
        promo_id = self.request.get("promo_id")
        seat_count = int(self.request.get("seat_count")) if self.request.get("seat_count") else 0
        total_amount = Decimal(self.request.get("total_amount")) if self.request.get("total_amount") else Decimal("0.0")

        try:
            self.response.headers["Content-Type"] = "text/plain"

            if promo_code == "" or not promo_code or promo_code is None or re.match('\w{14}$', promo_code) is None:
                raise BadRequestError("Error in parameters.", 400)

            if promo_code[:7] == "GMOVTRY":
                raise BadRequestError("Thank you for using GMovies, but unfortunately this claim code is not entitled to win free movie tickets. Please visit us again to book tickets for your next movie adventure!", 403)

            queryset = Promos.query(Promos.promo_code == promo_code)
            promos_record = queryset.get()

            # Check if the claim code is actually valid
            status_code, message = verify_promos(promos_record, seat_count, total_amount, promo_id)

            if status_code != 200:
                raise BadRequestError(message, status_code)

            promos_record.redeemed_on = asia_manila_timezone(datetime.datetime.now())
            promos_record.is_redeemed = True
            promos_record.put()
        except BadRequestError, (instance):
            self.error(instance.code)
            self.response.out.write(instance.msg)

class VerifyHandler(webapp2.RequestHandler):
    def get(self):
        promo_code = self.request.get("promo_code")
        promo_id = self.request.get("promo_id")
        seat_count = int(self.request.get("seat_count")) if self.request.get("seat_count") else 0
        total_amount = Decimal(self.request.get("total_amount")) if self.request.get("total_amount") else Decimal("0.0")

        try:
            self.response.headers["Content-Type"] = "text/plain"

            if promo_code == "" or not promo_code or promo_code is None or re.match('\w{14}$', promo_code) is None:
                raise BadRequestError("Error in parameters.", 400)

            if promo_code[:7] == "GMOVTRY":
                raise BadRequestError("Thank you for using GMovies, but unfortunately this claim code is not entitled to win free movie tickets. Please visit us again to book tickets for your next movie adventure!", 403)

            queryset = Promos.query(Promos.promo_code == promo_code)
            promo_code_record = queryset.get()

            # Check if the claim code is actually valid
            status_code, message = verify_promos(promo_code_record, seat_count, total_amount, promo_id)

            if status_code != 200:
                raise BadRequestError(message, status_code)
        except BadRequestError, (instance):
            self.error(instance.code)
            self.response.out.write(instance.msg)

class ResetHandler(webapp2.RequestHandler):
    def get(self):
        promo_code = self.request.get("promo_code")
        # promo_id = self.request.get("promo_id")

        try:
            if promo_code == "" or not promo_code or promo_code is None or re.match('\w{14}$', promo_code) is None:
                raise BadRequestError("Error in parameters.", 400)

            # check if the claim code exists
            q = Promos.query(Promos.promo_code == promo_code)
            Record = q.get()

            if Record is None:
                raise BadRequestError("Invalid code.", 401)

            if Record.expired_date:
                if asia_manila_timezone(datetime.datetime.now()) <= Record.expired_date and Record.redeemed_on is None and not Record.is_expired:
                    raise BadRequestError("Pending redemption\n", 401)

                if asia_manila_timezone(datetime.datetime.now()) > Record.expired_date:
                    raise BadRequestError("Unable to reset this claim code, because the expiration date is less than the current date\n", 403)

            if Record.redeemed_on is None and not Record.is_expired:
                raise BadRequestError("Pending redemption\n", 401)

            # log.info(promo_id)
            # if promo_id:
            #     if promo_id not in Record.promo_id:
            #         raise BadRequestError("Unmatched Promo ID.", 401)

            Record.redeemed_on = None
            Record.is_expired = False
            Record.is_redeemed = False
            Record.put()
        except BadRequestError, (instance):
            self.error(instance.code)
            self.response.out.write(instance.msg)

class ExpireHandler(webapp2.RequestHandler):
    def get(self):
        promo_code = self.request.get("promo_code")
        # promo_id = self.request.get("promo_id")
        
        try:
            if promo_code == "" or not promo_code or promo_code is None or re.match('\w{14}$', promo_code) is None:
                raise BadRequestError("Error in parameters.", 400)

            # check if the claim code exists
            q = Promos.query(Promos.promo_code == promo_code)
            Record = q.get()

            if Record is None:
                raise BadRequestError("Invalid code.", 401)

            if Record.is_expired:
                raise BadRequestError("This claim code is expired\n", 401)

            if Record.expired_date:
                if asia_manila_timezone(datetime.datetime.now()) > Record.expired_date:
                    raise BadRequestError("This claim code is expired\n", 401)

            if Record.redeemed_on is not None:
                raise BadRequestError("This claim code has already been redeemed\n", 401)

            # log.info(promo_id)
            # if promo_id:
            #     if promo_id not in Record.promo_id:
            #         raise BadRequestError("Unmatched Promo ID.", 401)

            Record.is_expired = True
            Record.put()
        except BadRequestError, (instance):
            self.error(instance.code)
            self.response.out.write(instance.msg)

def create_promo_code(batch_code=None):
    while(True):
        if batch_code:
            promo_code_suffix = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(7))
            promo_code = batch_code + promo_code_suffix
        else:
            promo_code = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(14))

        # check if this value exists
        z = Promos.query(Promos.promo_code == promo_code)
        Collided = z.get()

        if Collided is None:
            break

    return promo_code

def create_mail_body(promo_codes, expiration_date=None, promo_name=None):
    mail_body = ""

    if expiration_date:
        mail_body = mail_body + 'Expiration Date: %s\n' % str(expiration_date)
    else:
        mail_body = mail_body + 'Expiration Date: N/A\n'

    if promo_name:
        mail_body = mail_body + 'Promo Name: %s\n\n' % promo_name.title()
    else:
        mail_body = mail_body + 'Promo Name: N/A\n\n'

    if len(promo_codes) > 1:
        mail_body = mail_body + 'Generated %s Claim Codes:\n\n' % str(len(promo_codes))
        mail_body = mail_body + '\n'.join(promo_codes)
    elif len(promo_codes) == 1:
        mail_body = mail_body + 'Generated Claim Code: %s' % promo_codes[0]

    return mail_body

def parse_expiration_date(expired_date, expired_time):
    try:
        expiration_date_str = expired_date + " " + expired_time
        expiration_date = datetime.datetime.strptime(expiration_date_str, '%m/%d/%Y %H:%M')
    except ValueError:
        raise BadRequestError("Error in expiration date/time format.", 400)

    return expiration_date

def parse_date(date):
    try:
        date = datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
    except ValueError:
        try:
            date = datetime.datetime.strptime(date, '%Y-%m-%d %H:%M')
        except ValueError:
            date = None

    return date

def verify_promos(promos, seat_count, total_amount, promo_id=None):
    max_seat_count = DEFAULT_SEAT_COUNT
    max_total_amount = Decimal(DEFAULT_PRICE_PER_SEAT) * DEFAULT_SEAT_COUNT

    if promos is None:
        log.info("Invalid...")
        return 404, "Thank you for using GMovies, but this claim code seems to be invalid. Please check it and try again."

    if promos.redeemed_on is not None:
        log.info("Already been redeemed...")
        return 401, "Thank you for using GMovies, but this claim code has already been redeemed. Please select another payment option."

    if promos.is_expired:
        log.info("Expired...")
        return 401, "Thank you for using GMovies, but this claim code is expired. Please select another payment option."

    if promos.expired_date:
        if asia_manila_timezone(datetime.datetime.now()) > promos.expired_date:
            log.info("Expired...")
            return 401, "Thank you for using GMovies, but this claim code is expired. Please select another payment option."

    if promos.get_total_amount() is not None:
        max_seat_count = promos.seat_count
        max_total_amount = promos.get_total_amount()

    if seat_count > max_seat_count:
        log.info("Claim code exceeded maximum seats...")
        return 401, "You are only allowed a maximum of %s" % max_tickets_message(max_seat_count)

    if total_amount > max_total_amount:
        log.info("Claim code exceeded maximum amount...")
        return 401, "Invalid ticket amount."

    if promos.promo_id and promo_id not in promos.promo_id:
        log.info("Different promo_id")
        return 404, "Thank you for using GMovies, but this claim code seems to be invalid for this promo. Please check it and try again."

    log.info("Successfully Verified Claim Code...")

    return 200, "Successfully verified claim code."

def max_tickets_message(max_seat_count):
    max_seat_count_str = ['zero (%s) ticket.', 'one (%s) ticket.',
            'two (%s) tickets.', 'three (%s) tickets.', 'four (%s) tickets.',
            'five (%s) tickets.', 'six (%s) tickets.', 'seven (%s) tickets.',
            'eight (%s) tickets.', 'nine (%s) tickets.', 'ten (%s) tickets.']

    return max_seat_count_str[max_seat_count] % max_seat_count


app = webapp2.WSGIApplication([webapp2.Route('/admin/0/promos', PromosIndexHandler),
                webapp2.Route('/admin/0/promos/generate_promo_code', GeneratePromoCodeHandler),
                webapp2.Route('/admin/0/promos/redeem', RedeemHandler),
                webapp2.Route('/admin/0/promos/verify', VerifyHandler),
                webapp2.Route('/admin/0/promos/reset', ResetHandler),
                webapp2.Route('/admin/0/promos/expire', ExpireHandler)],
                debug=True)