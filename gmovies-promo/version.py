#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Python25
# from google.appengine.ext import webapp
# from google.appengine.ext.webapp import util

# Python27
import webapp2


class GenericVersionHandler(webapp2.RequestHandler):
    def get(self):
        self.response.out.write("1.3")


class AndroidVersionHandler(webapp2.RequestHandler):
    def get(self):
        self.response.out.write("1.2")


class IOSVersionHandler(webapp2.RequestHandler):
    def get(self):
        self.response.out.write("1.3")


class GMessageAndroidVersionHandler(webapp2.RequestHandler):
    def get(self):
        self.response.out.write("1.0")


class GMessageIosVersionHandler(webapp2.RequestHandler):
    def get(self):
        self.response.out.write("1.0")


""" Old Code: Python25
def main():
    application = webapp.WSGIApplication([('/version', GenericVersionHandler),
                        ('/version/android/', AndroidVersionHandler),
                        ('/version/ios/', GenericVersionHandler),
                        ('/gmessage/version/android/', GMessageAndroidVersionHandler),
                        ('/gmessage/version/ios/', GMessageIosVersionHandler)],
                        debug=True)
    util.run_wsgi_app(application)


if __name__ == '__main__':
    main()"""


app = webapp2.WSGIApplication([('/version', GenericVersionHandler),
                    ('/version/android/', AndroidVersionHandler),
                    ('/version/ios/', GenericVersionHandler),
                    ('/gmessage/version/android/', GMessageAndroidVersionHandler),
                    ('/gmessage/version/ios/', GMessageIosVersionHandler)],
                    debug=True)
